%Part B Matlab
close all
clc
clearvars
addpath(genpath(pwd))
w = warning ('off','all');

load('Part_A_Values.mat');
fprintf('Q10\n')

C = C(3,:); % model as SISO system
D = D(3,:);


s = tf('s');

tau1 = 0.5;
tau2 = 5.5;
tau3 = 0.1;
Kc = .02;


%Making the system G
Gs = ss(A,B,C,D);
GGs = tf(Gs);

%Making filters and weights
M = 4;
A_W = .0005;  % to ensure integral action
wb = 7;  % bandwidth
W_1 = ((s/sqrt(M) + wb)/(s + wb*sqrt(A_W)))^2;
W_2 = ss(0.9*(s/(s+150)));


%Making the controller K
[K,CL,gamma] = mixsyn(Gs,W_1,W_2,[]);

S = minreal(inv(1+GGs*tf(K)));
KS = minreal(tf(K)*S);
L = minreal(GGs*tf(K));
T = minreal(GGs*tf(K)*inv(1+GGs*tf(K)));

figure('Renderer', 'painters', 'Position', [10 10 900 400])
bodemag(S,1/W_1)
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
[mag,~,w] = bode(S);
legend('S','1/W_1');
bandwidth = w(min(find(mag >= db2mag(-3))));
title('Specification check: S and 1/W_1');
grid on
saveas(gcf,'Figs/Q10_S_W1.png')

figure('Renderer', 'painters', 'Position', [10 10 900 400])
hold on
bodemag(S*K,1/W_2)
[mag,~,w] = bode(W_1*S);
gamma = max(mag);
[mag,~,w] = bode(W_2*K*S);
gamma = max(max(mag),gamma);
grid on
legend('KS','1/W2')
title('');
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
hold off
title('Mixed sensitivity specification plot')
saveas(gcf,'Figs/Q10_KS_W2.png')

figure('Renderer', 'painters', 'Position', [10 10 900 600])
step(K*S)
title('Step of K*S');
set(findall(gcf,'type','line'),'linewidth',2)
grid on
saveas(gcf,'Figs/Q10_step.png')

disptext = sprintf('Gamma_max: %f bandwidth: %f.',gamma,bandwidth);
disp(disptext);

%% Q11 
fprintf('\n\nQ11\n')
K_p = pole(K);
K_z = zero(K);
disptext = sprintf('Controller has %d poles and %d zeros.',size(K_p,1),size(K_z,1));
disp(disptext);

CL_p = pole(CL);
CL_z = zero(CL);
disptext = sprintf('Closed loop has %d poles and %d zeros.',size(CL_p,1),size(CL_z,1));
disp(disptext);


%% Q12

simout = sim('threeDiskOscillatorModelB.slx','SimulationMode','normal');
sim_y = simout.y;
sim_r = simout.r;
figure('Renderer', 'painters', 'Position', [10 10 900 600])
plot(sim_y,'LineWidth',2)
hold on
plot(sim_r,'LineWidth',2)
grid on
yticks([0, pi/4, pi/2])
yticks([0, 2*pi/8, 4*pi/8])
yticklabels({0, '\pi/4', '\pi/2'})
ylim([-pi/2,pi])
ylabel('$\theta$ [rad]','fontsize',14,'interpreter','latex')
xlabel('Time[s]','fontsize',14,'interpreter','latex')
legend('y','r')
title('Simulated reference and output')
hold off
saveas(gcf,'Figs/Q12_y.png')


%% Q13
fprintf('\n\nQ13\n')

%ALL THIS OUT COMMENTED STUFF IS SHITE - DON'T USE IT
% figure('Renderer', 'painters', 'Position', [10 10 900 400])
% bodemag(T)
% hold on
% bodemag(1/W_2)
% set(findall(gcf,'type','line'),'linewidth',2)
% xlim([1e-3 1e3])
% legend('T','1/W2')
% title('Robust stability check')
% grid on
% saveas(gcf,'Figs/Q13_RS.png')
% 
% figure('Renderer', 'painters', 'Position', [10 10 900 400])
% bodemag(S)
% hold on
% bodemag(1/W_1)
% set(findall(gcf,'type','line'),'linewidth',2)
% xlim([1e-3 1e3])
% legend('S','1/W1')
% title('Robust performance check')
% grid on
% saveas(gcf,'Figs/Q13_RP.png')
% 
% fprintf('||W_1*S||: %f \n',hinfnorm(W_1*S)) %||Wp*S|| < 1, this is required for RP
% fprintf('||W_2*T||: %f \n',max(bode(T*W_2))) %RS is satisfied

%Equation 8.54, p303
M_0 = GGs*tf(K)*(1+GGs*tf(K))^-1;
W_O = 1/(M_0);
fprintf('Maximum output uncertainty for RS: %f \n',min(bode(W_O))) %This works! RS

%Page 322 eq. 8.129 and following text (T=T_I for SISO) and Page 295
%paragraph 1, G_P is the same for input and output uncertainties
fprintf('Maximum output uncertainty for RP: %f \n',(1-norm(W_1*S,inf))/norm(T,inf)) %This works! RP
% 1-||Wp*S||
% ----------  = W_O
%    |T|

%% Q14

% Change the inertia of the bottom disk J_1 
J_1 = 0.0325;

A = [[        0,        1,                0,        0,        0,        0]; % build new A matrix
[ -k_1/J_1, -b_1/J_1,          k_1/J_1,        0,        0,        0];
[        0,        0,                0,        1,        0,        0];
[  k_1/J_2,        0, -(k_1 + k_2)/J_2, -b_2/J_2,  k_2/J_2,        0];
[        0,        0,                0,        0,        0,        1];
[        0,        0,          k_2/J_3,        0, -k_2/J_3, -b_3/J_3]];

B = [  0
     1/J_1
       0
       0
       0
       0];
   
G_p = ss(A,B,C,D);

Gs_p  = tf(G_p);

%Comparing G and G_P
figure('Renderer', 'painters', 'Position', [10 10 900 400])
bodemag(GGs)
hold on
bodemag(Gs_p)
grid on
title('G and G_P')
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
legend('G','G_P')
saveas(gcf,'Figs/Q14_G_GP.png')

l_I = (Gs_p - GGs)/GGs;

figure('Renderer', 'painters', 'Position', [10 10 900 400])
bodemag(l_I,W_O)
title('Lower bound $l_I$ compared with $W_O$','interpreter','latex')
grid on
legend('$l_I$','$W_O$','interpreter','latex')
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
ylim([-50 100])
saveas(gcf,'Figs/Q14_l_I.png')

%% Q15
fprintf('\n\nQ15\n')

W_I = (0.833*s)/(s + 0.089);
figure('Renderer', 'painters', 'Position', [10 10 900 400])
bodemag(l_I,'b',W_I,'r--')
xlim([1e-6 1e3])
title('Approximation of W_I compared with l_I')
grid on
legend('$W_I$','$l_I$','interpreter','latex')
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
saveas(gcf,'Figs/Q15_comparison.png')

%Making filters and weights
M = 4;
A_W = .0005;  % to ensure integral action
wb = 2;  % bandwidth
W_1 = 0.5*((s/sqrt(M) + wb)/(s + wb*sqrt(A_W)))^2;
W_2 = ss(4*(s/(s+20)));

[K,CL,gamma] = mixsyn(Gs,W_1,W_2,W_I);

S = minreal(inv(1+GGs*tf(K)));
KS = minreal(tf(K)*S);
L = minreal(GGs*tf(K));
T = minreal(GGs*tf(K)*inv(1+GGs*tf(K)));

figure('Renderer', 'painters', 'Position', [10 10 900 400])
hold on
bodemag(W_1*S)
[mag,~,w] = bode(W_1*S);
gamma = max(mag);
bodemag(W_2*K*S)
[mag,~,w] = bode(W_2*K*S);
gamma = max(max(mag),gamma);
bodemag(W_I*T)
[mag,~,w] = bode(W_I*T);
gamma = max(max(mag),gamma);
set(findall(gcf,'type','line'),'linewidth',2)
xlim([1e-3 1e3])
ylim([-100 0])
grid on
legend('W1 S','W2 K S','W_I T')
title('Mixed sensitivity specification plot');
hold off
movegui('southwest');
saveas(gcf,'Figs/Q15_gamma.png')

figure('Renderer', 'painters', 'Position', [10 10 900 600])
opt = stepDataOptions('InputOffset',-1,'StepAmplitude',2);
step(K*S)
title('Step of K*S');
movegui('southeast');
set(findall(gcf,'type','line'),'linewidth',2)
grid on
saveas(gcf,'Figs/Q15_step.png')

disptext = sprintf('Gamma_max: %f bandwidth: %f.',gamma,bandwidth);
disp(disptext);

%% Q16 
J_1 = 0.005;
simTime = 30;
simout = sim('threeDiskOscillatorModelB.slx','SimulationMode','normal');
sim_y = simout.y;
sim_r = simout.r;
figure('Renderer', 'painters', 'Position', [10 10 900 600])
plot(sim_y,'LineWidth',2)
hold on
plot(sim_r,'LineWidth',2)
grid on
yticks([0, pi/4, pi/2])
yticks([0, 2*pi/8, 4*pi/8])
yticklabels({0, '\pi/4', '\pi/2'})
ylim([-pi/4,3*pi/4])
ylabel('$\theta$ [rad]','fontsize',14,'interpreter','latex')
xlabel('Time[s]','fontsize',14,'interpreter','latex')
legend('y','r')
title('Simulated reference and output with plant disturbance')
saveas(gcf,'Figs/Q16_ref.png')
hold off


simout = sim('threeDiskOscillatorModelB.slx','SimulationMode','normal');
sim_u = simout.u;
figure('Renderer', 'painters', 'Position', [10 10 900 600])
plot(sim_u,'LineWidth',2)
grid on
ylabel('$\theta$ [rad]','fontsize',14,'interpreter','latex')
xlabel('Time[s]','fontsize',14,'interpreter','latex')
legend('y','r')
title('Input u to G')
hold off
%saveas(gcf,'Figs/Q16_u_to_g.png')





