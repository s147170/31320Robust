% 31320 Mandatory assignment 2020 starting script
% Dimitrios Papageorgiou (dimpa@elektro.dtu.dk)

clear all;
close all;
clc;
load('ECP_values.mat');
% Physical system parameters
J_1 = ECP_values(1);            % Disk 1 inertia kgm^2
J_2 = ECP_values(2);            % Disk 2 inertia kgm^2
J_3 = ECP_values(3);            % Disk 3 inertia kgm^2
k_1 = ECP_values(4);            % Shaft 1-2 stiffness Nm/rad
k_2 = ECP_values(5);            % Shaft 2-3 stiffness Nm/rad
b_1 = mean(ECP_values([6 7]));  % Disk 1 damping and friction Nms/rad
b_2 = mean(ECP_values([8 9]));  % Disk 2 damping and friction Nms/rad
b_3 = mean(ECP_values([10 11]));% Disk 3 damping and friction Nms/rad
T_Cp = ECP_values(12);          % Disk 1 Coulomb friction in positive direction
T_Cm = ECP_values(13);          % Disk 1 Coulomb friction in negative direction
atan_scale = 100;               % Sign approximation factor
w_th = 0.75;                    % Threshold angular velocity rad/s

% The system states are [theta_1;omega_1;theta_2;omega_2;theta_3;omega_3]
x_0 = [0;0;0;0;0;0];            % Initial conditions

T_s = 0.004;                    % Sampling period
sigma_meas = deg2rad(0.0056)*[1;1;1]; % Measurements noise
load('test1.mat');
TimeVector = [0:T_s:T_s*(length(r)-1)]';

%% State space representation
A = [[        0,        1,                0,        0,        0,        0];
[ -k_1/J_1, -b_1/J_1,          k_1/J_1,        0,        0,        0];
[        0,        0,                0,        1,        0,        0];
[  k_1/J_2,        0, -(k_1 + k_2)/J_2, -b_2/J_2,  k_2/J_2,        0];
[        0,        0,                0,        0,        0,        1];
[        0,        0,          k_2/J_3,        0, -k_2/J_3, -b_3/J_3]];
B = [ 0;
 1/J_1;
     0;
     0;
     0;
     0];
C = [[ 1, 0, 0, 0, 0, 0];
[ 0, 0, 1, 0, 0, 0];
[ 0, 0, 0, 0, 1, 0]];
D = [0;
    0;
    0];
E_x = [0;
 -1/J_1;
      0;
      0;
      0;
      0];
E_y = [ 0;
     0;
     0;
     0;
     0;
     0];

% Discrete time
sys_d = c2d(ss(A,B,C,D),T_s);
F_d = sys_d.A;
G_d = sys_d.B;

% Luenberger observer with integral action design
F_aug = [F_d G_d;zeros(1,6) 1];
G_aug = [G_d;0];
C_aug = [C zeros(3,1)];
L_aug = dlqe(F_aug,eye(7),C_aug,1e-3*eye(7),deg2rad(0.0056)*eye(3));
L_o = L_aug(1:6,:);
L_d = L_aug(7,:);

% State-feedback LQR design
Q_c = diag([2 0 2 0 2.5 0.0024]);
R_c = 10;
K_c = [];

% Scaling of reference
C_ref = [];
theta_ref = pi/2;

%% Residuals and filters design

%% Design of the nullspace-based residuals

%% CUSUM

%% GLR
f_m = [0;-0.125;0]; % Sensor fault vector (added to [y1;y2;y3])

%% Virtual sensor
C_f = [];
vs_eig = [];
L_f = [];
A_v = [];
B_v = [];
P_v = [];
C_v = [];

%% Simulation
simTime = 25;       % Simulation duration in seconds
f_m_time = 8.5;     % Sensor fault occurence time

tau_1 = 1;
tau_2 = 1;
tau_3 = 1;
tau_4 = 1;

K_c = [0 0 0 0 0 0];
C_ref = [0 0 0 0 0 0]';

% sim('threeDiskOscillatorModel');

%% Plots
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize',20);
set(0,'DefaultLineLineWidth', 2);





