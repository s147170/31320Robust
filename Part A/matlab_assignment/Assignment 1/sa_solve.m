function [res,analytic_sym] = sa_solve(sys,parIndex)
% Similar to the solve in sa_backtrack just for one parity equation only
    numInputs = size(sys.inputs,2);
    numMeas = size(sys.meas,2);
    numUnknowns = size(sys.unknowns,2);
    numKnowns = numInputs+numMeas;
    analytic_sym = [];

    
    usedCons = find(sys.parityEqs{parIndex}>0);
    unmatchedCons = find(sys.matchings(parIndex,:) ==0);
    usedCons = usedCons(usedCons~=unmatchedCons);
    
    usedUnknowns = zeros(1, size(sys.incMatrix, 2) - numKnowns);   
    for i=usedCons
        usedUnknowns = usedUnknowns | sys.incMatrix(i, numKnowns+1:end);
    end
    
    s = warning('off', 'symbolic:solve:warnmsg3');
    res = solve(sys.cons{3, usedCons}, sys.unknowns{1, usedUnknowns});
    
    analytic = cell(1, length(unmatchedCons));
    if ~isempty(res)
        for i = 1:length(unmatchedCons)
                analytic{i} = sys.cons{3, unmatchedCons(i)};
                % Retrieve indices of unknown variables that are used in this
                % parity equation.
                usedUnknowns = ...
                    find(sys.incMatrix(unmatchedCons(i), numKnowns+1:end));
                % Replace all unknown variables by known variables obtain before by
                % solivng the set of equations.
                for j = usedUnknowns
                %for j = 1:length(sys.unknowns) % Run through all unknowns to make sure everything is replaced (less efficient but should always work)
                    unknown = sys.unknowns{1, j};
                    %sprintf('replace %s with %s in  %s ',char(sys.unknowns{1, j}),char(res.(unknown)),char(analytic{i}))
                    if length(fieldnames(res)) < 1
                        analytic{i} = subs(analytic{i}, unknown, res);
                    else
                        analytic{i} = subs(analytic{i}, unknown, res.(unknown)(1));
                    end
                    %analytic{i} = subs(analytic{i}, unknown, res.(unknown),0); % added flag to only replace left to right
                end
                % Convert symbol into a character string.
                if(strcmp(class(analytic{i}),'sym'))
                analytic_sym{i} = analytic{i};
                end
                analytic{i} = char(analytic{i});
        end
    else
        return
    end
    disp(analytic_sym{1})

end