function sys = sa_disable(sys, cons, vars, enable)
%SA_DISABLE Disable variables and constraints.
%   SYS=SA_DISABLE(SYS,CONS) returns a new system in which the
%   constraints CONS are disabled. CONS is either a vector with indices of
%   the constraints, a string 'all' for all constraints or a cell string
%   {'CON1', 'CON2', ..., 'CONN'} with the constraint names to disable.
%
%   SYS=SA_DISABLE(SYS,CONS,VARS) further returns a new system in which the
%   variables VARS are also disabled. VARS is either a vector with indices
%   of the variables within the incidence matric, a string 'all' for all
%   variables or a cell string {'VAR1', 'VAR2', ..., 'VARN'} with the
%   variables names to disable.
%
%   SYS=SA_DISABLE(SYS,CONS,VARS,1) further reverses the effect and enables
%   the constraints CONS and variables VARS instead of disabling them.
%
%   Examples:
%       % Disable constraints 1 and 2:
%       sys = sa_rand();
%       sys = sa_disable(sys, {'c1', 'c2'});
%       % or:
%       sys = sa_disable(sys, [1, 2]);
%
%       % Disable all input variables:
%       sys = sa_disable(sys, [], 1:size(sys.meas, 2));
%
%       % Enable all variables and constraints:
%       sys = sa_disable(sys, 'all', 'all', 1);
%
%   See also: sa_disp.

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 21/10-2012
%   Last modified - 25/01-2012

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Get number of constraints and variables.
[numCons, numVars] = size(sys.incMatrix);
allCons = 1:numCons;
allVars = 1:numVars;

% Check constraints.
if (ischar(cons) && strcmpi(cons, 'all'))
    cons = allCons;
elseif (iscellstr(cons))
    cons = strcmp2(cons, sys.cons(1, :));
elseif (~isnumeric(cons) || any(~ismember(cons, allCons)))
    error('Invalid constraints argument.');
end

% Check variables.
if (nargin >= 3)
    if (ischar(vars) && strcmpi(vars, 'all'))
        vars = allVars;
    elseif (iscellstr(vars))
        [~, vars2] = sa_names(sys, 1);
        vars = strcmp2(vars, vars2);
    elseif (~isnumeric(vars) || any(~ismember(vars, allVars)))
        error('Invalid variables argument.');
    end
else
    vars = [];
end

% Check enabled flag.
if ((nargin > 3) && ~isnumeric(enable) && ~islogical(enable))
    error('Invalid type of enable argument.');
elseif (nargin <= 3)
    enable = 0;
end

% Clear matchings and parity equations because they are not valid anymore
% since the system changes.
sys.matchings = [];
sys.parityEqs = [];

if (enable)
    % Increase amount of enabled constraints/variables by taking the union.
    sys.enabledCons = union(sys.enabledCons, cons);
else
    % Decrease amount of enabled constraints/variables by removing (xor)
    % all those elements, that are specified and still present (intersect).
    sys.enabledCons = setxor(sys.enabledCons, ...
        intersect(sys.enabledCons, cons));
end

if (~enable)
    % Disable further all constraints that contain disabled variables.
    enabledCons = ~any(sys.incMatrix(sys.enabledCons, vars), 2);
    sys.enabledCons = sys.enabledCons(enabledCons);
end

end

function ind = strcmp2(search, cells)
% STRCMP2 Return the indices of each element of a search cell string within
% a second cell string.
    numCells = size(cells, 2);
    l = zeros(1, numCells);
    for i=1:length(search)
        l = strcmp(search{i}, cells) | l;
    end
    ind = find(l);
end