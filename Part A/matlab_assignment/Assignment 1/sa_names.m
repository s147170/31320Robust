function [cons, varargout] = sa_names(sys, combine, type)
%SA_NAMES Returns the names of the constraints and variables of a system.
%   [CONS,INPUTS,MEAS,UNKNOWN,PARAMETERS] = SA_NAMES(SYS) returns the names of the
%   constraints and inputs, measurement and unknown variables, and the parameters of a system
%   SYS in four cell string arrays CONS, INPUTS, MEAS and UNKNOWNS,
%   respectively.
%
%   [CONS,VARS] = SA_NAMES(SYS, 1) further combines the inputs, measurement
%   and unknown variables and parameters into one string cell array VARS in the given
%   order.
%
%   [CONS,VARS] = SA_NAMES(SYS, 1, type) further returns the information
%   defined by TYPE:
%       'name':     Returns the names of the constraints and variables,
%                   respectively. (default)
%       'latex':    Returns the latex expression of the constraints and
%                   variables, respectively.
%       'analytic': Returns the analytic expressions of the constraints.
%                   The return variables for the variables is an empty set.
%
%   TYPE can either be a char array or a cell string array holding a
%   combination of the former described. In the latter case the returned
%   names will be of the first specfied type, except the first name is
%   empty, then the name of the second type is chosen and so on. This can
%   be helpful to avoid empty names when checking for latex or analytic
%   expressions that are not mandatory.
%
%   Examples:
%       sys = sa_rand([1, 1], 1, 1);
%       [cons, inputs, meas, unknowns] = sa_names(sys);
%       % cons is {'c1'}
%       % inputs is {'u1'}, meas is {'y1'}, unknowns is {}
%
%       [cons, vars] = sa_names(sys, 1);
%       % cons is {'c1'}
%       % vars is {'u1, 'y1'}

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 12/01-2013
%   Last modified - 15/01-2013

if (nargin < 2)
    combine = 0;
end

if (nargin < 3)
    type = 'name';
end

if (ischar(type))
    type = {type};
end

numTypes = length(type);
cons = cell(numTypes, size(sys.cons, 2));
inputs = cell(numTypes, size(sys.inputs, 2));
meas = cell(numTypes, size(sys.meas, 2));
unknowns = cell(numTypes, size(sys.unknowns, 2));
parameters = cell(numTypes, size(sys.parameters, 2));

for i = 1:numTypes

    % Retrieve row index dependent on specified type.
    switch lower(type{i})
        case 'name'
            index = 1;
        case 'latex'
            index = 2;
        case 'analytic'
            index = 3;
        otherwise
            index = 1;
    end

    % Return constraints.
    [numRows, numCols] = size(sys.cons);
    if (numCols > 0)
        if (numRows >= index)
            cons(i, :) = sys.cons(index, :);
        else
            cons(i, :) = repmat({''}, 1, numCols);
        end
    end

    % Build inputs.
    [numRows, numCols] = size(sys.inputs);
    if (numCols > 0)
        if (numRows >= index)
            inputs(i, :) = sys.inputs(index, :);
        else
            inputs(i, :) = repmat({''}, 1, numCols);
        end
    end

    % Build measurements.
    [numRows, numCols] = size(sys.meas);
    if (numCols > 0)
        if (numRows >= index)
            meas(i, :) = sys.meas(index, :);
        else
            meas(i, :) = repmat({''}, 1, numCols);
        end
    end

    % Build unknown variables.
    [numRows, numCols] = size(sys.unknowns);
    if (numCols > 0)
        if (numRows >= index)
            unknowns(i, :) = sys.unknowns(index, :);
        else
            unknowns(i, :) = repmat({''}, 1, numCols);
        end
    end
    % Build parameters.
    [numRows, numCols] = size(sys.parameters);
    if (numCols > 0)
        if (numRows >= index)
            parameters(i, :) = sys.parameters(index, :);
        else
            parameters(i, :) = repmat({''}, 1, numCols);
        end
    end
end

% Replace empty fields with fields from the next category.
cons = replace_empty_fields(cons);
inputs = replace_empty_fields(inputs);
meas = replace_empty_fields(meas);
unknowns = replace_empty_fields(unknowns);
parameters = replace_empty_fields(parameters);

cons = cons(1, :);
if (combine)
    varargout = {[inputs(1, :), meas(1, :), unknowns(1, :),parameters(1,:)]};
else
    varargout = {inputs(1, :), meas(1, :), unknowns(1, :),parameters(1,:)};
end

end

function cell = replace_empty_fields(cell)
for i = 1:size(cell, 2)
    for j = 2:size(cell, 1)
        if isempty(cell{1, i})
            cell{1, i} = cell{j, i};
        end
    end
end
end