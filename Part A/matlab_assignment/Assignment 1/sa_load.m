function sys = sa_load(fileName, format)
%SA_LOAD Load structural analysis system.
%   SYS = SA_LOAD(FILENAME) loads a structural analysis system from the
%   file specified in FILENAME. The file format will be determined
%   automatically by checking the file extension. Currently the following
%   file formats are supported:
%       'xml' (recommend):
%           Loads the system from a XML system description file. This file
%           format is recommend since it is backwards compatible.
%
%       'mat':
%           Loads the system from a binary MATLAB workspace file. Since the
%           binary format might change over time it is not recommend to
%           store a system in this file format.
%
%   SYS = SA_LOAD(FILENAME, FORMAT) further enforces the file format to be
%   FORMAT. This can be helpful if the file extension of a file is not set
%   accordingly. FORMAT can be any of the above described file formats. 
%
%   Example:
%       % Load a system from a XML file.
%       sys = ('test.xml');
%
%       % Load a system from a XML file without ending.
%       sys = ('test', 'xml');
%
%   See also: sa_create sa_save   

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 23/10-2012
%   Based on xmlOpenSavedfile
%   Last modified - 02/12-2012

% Check if file exists.
if (exist(fileName, 'file') ~= 2)
    error('File %s not found.', fileName);
end

% Check arguments.
if nargin < 2
    % Default method is determined by file extension
    [~, ~, ext] = fileparts(fileName);
    format = lower(ext);
else
    format = ['.', lower(format)];
end

% Check format and call sub-functions.
switch(format)
    case '.xml'
        sys = load_xml(fileName);
        
    case '.mat'
        sys = load_mat(fileName);
        
    otherwise
        error('Unsupported file extension "%s".', format);
end

end

%% Load XML file
function sys = load_xml(fileName)
% Reads the SaTool XML save data file using the Xerces XML parser already
% implemented in matlab.
currentVersion = 1.1;
xmldoc = xmlread(fileName);

% PARSE VERSION
version = xmldoc.getElementsByTagName('version');
if (version.getLength == 0)
    version = 1;
else
    strVersion = version.item(0).getTextContent;
    version = str2double(strVersion);
    if (isempty(version))
        error('Invalid version identifier %s', strVersion);
    elseif ceil(version) > ceil(currentVersion)
        error('Major file version differs. File is incompatible.');
    elseif currentVersion < version
        warning('satools:newerFileVersion', ...
            'Minor file version differs. Possible loss of information');
    end
end

% PARSE VARIABLES

% Get number of variables
varTypeElement = xmldoc.getElementsByTagName('variables');
varsElement = varTypeElement.item(0).getElementsByTagName('variable');
numVars = varsElement.getLength;
% Allocate and initialize variables
vars = cell(2, numVars);
meas = cell(2, numVars);
unknowns = cell(2, numVars);
numInputs = 0;
numMeas = 0;
numUnknowns = 0;
% Loop through all variables
for i = 0:numVars-1
    % Name
    nameElement = varsElement.item(i).getElementsByTagName('name');
    name = char(nameElement.item(0).getTextContent);
    if (version >= 1.1)
        % Latex expression
        latexElement = varsElement.item(i).getElementsByTagName('latex');
        latex = char(latexElement.item(0).getTextContent);
    else
        latex = '';
    end
    % Type
    typeElement = varsElement.item(i).getElementsByTagName('type');
    type = char(typeElement.item(0).getTextContent);
    switch type
        case 'input'
            numInputs = numInputs + 1;
            vars{1, numInputs} = name;
            vars{2, numInputs} = latex;
        case 'measured'
            numMeas = numMeas + 1;
            meas{1, numMeas} = name;
            meas{2, numMeas} = latex;
        case 'unknown'
            numUnknowns = numUnknowns + 1;
            unknowns{1, numUnknowns} = name;
            unknowns{2, numUnknowns} = latex;
        otherwise
            error('Variable type %s is unknown.', type); 
    end
end
% Combine inputs, measurements and unknown variables in one cell string.
vars(:, numInputs+1:numInputs+numMeas) = meas(:, 1:numMeas);
vars(:, numInputs+numMeas+1:numInputs+numMeas+numUnknowns) = ...
    unknowns(:, 1:numUnknowns);

% PARSE VARIABLES

% Get number of parameters
skipParameters = 0;
pars = []; 
try
    parTypeElement = xmldoc.getElementsByTagName('parameters');
    parsElement = parTypeElement.item(0).getElementsByTagName('parameter');
catch
    warning('Parameters field does not exist in %s',fileName);
    skipParameters = 1;
end
if ~skipParameters
    numPars = parsElement.getLength;
    % Allocate and initialize parameters
    pars = cell(2, numPars);
    numParm = 0;
    % Loop through all parameters
    for i = 0:(numPars-1)
        % Name
        nameElement = parsElement.item(i).getElementsByTagName('name');
        name = char(nameElement.item(0).getTextContent);
        if (version >= 1.1)
            % Latex expression
            latexElement = parsElement.item(i).getElementsByTagName('latex');
            latex = char(latexElement.item(0).getTextContent);
        else
            latex = '';
        end
        % Type
        typeElement = parsElement.item(i).getElementsByTagName('type');
        type = char(typeElement.item(0).getTextContent);
        numParm = numParm + 1;
        pars{1, numParm} = name;
        pars{2, numParm} = latex;
    end
end

% PARSE CONSTRAINTS

% Get number of constraints
consElements = xmldoc.getElementsByTagName('constraint');
numCons = consElements.getLength;
% Allocate memory for constraints
cons = cell(3, numCons);
domains = ones(1, numCons);
canFail = zeros(1, numCons);
incM = zeros(numCons, numVars);
% Loop through constraints
for i = 0:numCons-1
    % Name
    nameElement = consElements.item(i).getElementsByTagName('name');
    cons{1, i+1} = char(nameElement.item(0).getTextContent);
    if (version >= 1.1)
        % Latex expression
        latexElement = consElements.item(i).getElementsByTagName('latex');
        latex = char(latexElement.item(0).getTextContent);
        % Analytic expression
        analyticElement = ...
            consElements.item(i).getElementsByTagName('analytic');
        analytic = char(analyticElement.item(0).getTextContent);
    else
        latex = '';
        analytic = '';
    end
    cons{2, i+1} = latex;
    cons{3, i+1} = analytic;
    % Domain
    domainElement = consElements.item(i).getElementsByTagName('domain');
    domains(i+1) = ...
        double(java.lang.Integer.valueOf(domainElement.item(0).getTextContent));
    % CanFail option
    canFailElement = consElements.item(i).getElementsByTagName('canFail');
    if strcmp(char(canFailElement.item(0).getTextContent),'Yes');
        canFail(i+1) = 1;
    end
    % Variables in the constraint
    varElements = consElements.item(i).getElementsByTagName('variable');
    for j = 0:varElements.getLength-1
        % Variable name
        nameElement = varElements.item(j).getElementsByTagName('name');
        name = char(nameElement.item(0).getTextContent);
        % Is variable directed
        directedElement = varElements.item(j).getElementsByTagName('directed');
        directed = ...
            double(java.lang.Integer.valueOf(directedElement.item(0).getTextContent));
        % Get index of variable in cell string
        cmp = strcmp(name, vars(1, :));
        ind = find(cmp);
        % Check if only one index was found
        if (length(ind) ~= 1)
            error('Variable "%s" not found or duplicated.', name);
        end
        % Set field in incidence matrix accordingly
        if (directed)
            incM(i+1, ind) = -1;
        else
            incM(i+1, ind) = 1;
        end
    end
end
% Convert canFail vector from logical to indices
canFail = find(canFail);

% Create system
sys = sa_create(incM, cons, vars(:, 1:numInputs), ...
    vars(:, numInputs+1:numInputs+numMeas), ...
    vars(:, numInputs+numMeas+1:end), domains, canFail,pars);

end

%% Load MATLAB binary file
function sys = load_mat(fileName)
    sys = load(fileName);
    [valid, msg] = sa_check(sys);
    if (~valid)
        error('Invalid binary file: %s', msg);
    end
end