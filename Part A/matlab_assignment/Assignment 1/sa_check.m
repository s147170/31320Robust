function [valid,msg] = sa_check(sys)
%SA_CHECK Check validity of structural analysis system.
%   VALID = SA_CHECK(SYS) validates the structural analysis system SYS by
%   checking whether the system has all necessary properties and whether
%   they are within valid ranges. Returns non-zero if the system is valid.
%
%   [VALID,MSG] = SA_CHECK(SYS) further returns a human readable error
%   message MSG describing the validation error in case an error occured
%   (hence VALID = 0).
%
%   Example:
%       % Create random system and check for validity.
%       sys = sa_rand();
%       [valid,msg] = sa_check(sys);
%       if (~valid)
%           error(msg);
%       end         
%
%   See also: sa_create

%   Copyright 2002-2014 Automation and Control Group, 
%   Technical University of Denmark
%   Created by Hans-Peter Wolf - 12/10-2012
%   Last modified - 13/10-2012

global VALID MSG;
MSG = '';

% Check if system is a struct
VALID = isstruct(sys);
if (~VALID)
    MSG = 'System is not a structure.';
end

% Check if properties exist
check_field(sys, 'incMatrix');
check_field(sys, 'cons');
check_field(sys, 'inputs');
check_field(sys, 'meas');
check_field(sys, 'unknowns');
check_field(sys, 'parameters');
check_field(sys, 'domains');
check_field(sys, 'canFail');
check_field(sys, 'enabledCons');
check_field(sys, 'matchings');
check_field(sys, 'parityEqs');

% Exit here if the structure is not correct to prevent further errors when
% accessing the subelements.
if (~VALID)
    % Return result
    valid = VALID;
    msg = MSG;
    return;
end

% Check if properties have reasonable values

% Check incidence matrix
check_numeric(sys.incMatrix, 'Incidence matrix');
check_value(sys.incMatrix, [-1, 0, 1], 'Incidence matrix');
check_dim(sys.incMatrix, [1,1], [inf, inf], 'incidence matrix');
[numRows, numCols] = size(sys.incMatrix);


% Check constraints
numCons = size(sys.cons, 2);
check_names(sys.cons(1,:), 'Constraints vector');
check_dim(sys.cons, [1,numRows], [3,numRows], 'contraint names');

% Check inputs
if (VALID)
    numInputs = size(sys.inputs, 2);
    VALID = (numInputs >= 0 && numInputs <= numCols);
    if (~VALID)
        MSG = 'Invalid number of inputs.';
    end
    check_dim(sys.inputs, [0,numInputs], [2,numInputs], 'input names');     
end

% Check measurements
if (VALID)
    numMeas = size(sys.meas, 2);
    VALID = (numMeas > 0 && numMeas <= (numCols - numInputs));
    if (~VALID)
        MSG = 'Invalid number of measurements.';
    end
    check_dim(sys.meas, [1,numMeas], [2,numMeas], 'measurement names');     
end

% Check unknown variables
if (VALID)
    numUnknowns = size(sys.unknowns, 2);
    VALID = (numUnknowns >= 0 && numUnknowns <= (numCols - numInputs - numMeas));
    if (~VALID)
        MSG = 'Invalid number of unknown variables.';
    end
    check_dim(sys.unknowns, [0,numUnknowns], [2,numUnknowns], ...
        'unknown variable names');     
end

% Check variables
if (VALID)
    numUnknowns =size(sys.unknowns, 2);
    numInputs =size(sys.inputs, 2);
    numMeas = size(sys.meas, 2);
    [~, vars] = sa_names(sys, 1);
    check_names(vars(1:(numInputs+numMeas+numUnknowns)), 'Variables');       
    %check_names(vars(1:(numInputs+numMeas+numUnknowns)), 'Variables vector');       
end

% Check for duplicates
if (VALID)
    names = [sys.cons(1,:), vars(1,:)];
    unique_names = unique(names);
    VALID = length(unique_names) == length(names);
    if (~VALID)
        MSG = 'Duplicate names.';
    end
end        

% Check domains
check_numeric(sys.domains, 'Domains vector');
check_dim(sys.domains, [1,numCons], [1,numCons], 'domain vector');

% Check can fail vector
check_numeric(sys.canFail, 'canFail vector');
check_dim(sys.canFail, [0,0], [1,numCons], 'canFail vector');
check_value(sys.canFail, 1:numCons, 'canFail vector');

% Check enable constraints vector
check_numeric(sys.enabledCons, 'enabled constraints vector');
check_dim(sys.enabledCons, [0,0], [1,numCons], ...
    'enabled contraints vector');
check_value(sys.enabledCons, 1:numCons, 'Enabled constraints vector');

% Check matchings matrix
if (~isempty(sys.matchings))
    check_numeric(sys.matchings, 'Matching matrix');
    numUnknowns = size(sys.unknowns, 2);
    check_dim(sys.matchings, [1, numCons], [inf, numCons], ...
        'matching matrix');
    check_value(sys.matchings, [-1, 0, 1:numUnknowns], 'Matching matrix');
end

% Return result
valid = VALID;
msg = MSG;

end

function check_field(sys, name)
% Check whether the system has a certain field
    global VALID MSG;
    if VALID
        VALID = isfield(sys, name);
        if (~VALID)
            MSG = sprintf('Field "%s" is missing in system.', name);
        end
    end
end

function check_names(A, name)
% Check whether string cell only contains allowed characters.
    global VALID MSG;
    if VALID
       VALID = iscellstr(A);
       if (~VALID)
           MSG = sprintf('%s is not a string cell.', name);
       else
           % Check for a valid name.
           B = ~cellfun(@(x) isvarname(x), A);
           VALID = ~any(B);
           if ~VALID
               ind = find(B, 1, 'first');
               MSG = sprintf('%s contains an invalid name "%s".', ...
                   name, A{ind});
           end
       end
    end
end

function check_numeric(A, name)
% Check whether a matrix is numeric
    global VALID MSG;
    if VALID
        VALID = ~any(isnan(A(:))) && ~any(isinf(A(:))) && isnumeric(A);
        if (~VALID)
            MSG = sprintf('%s is not numeric.', name);
        end
    end
end

function check_dim(A, mi, ma, name)
% Checker whether a matrix has the correct dimension
    global VALID MSG;
    if VALID
        [m, n, o] = size(A);
        VALID = (m >= mi(1)) && (m <= ma(1));
        if (length(mi) >= 2 && length(ma) >= 2)
           VALID = VALID && (n >= mi(2)) && (n <= ma(2));
        end
        if (length(mi) >= 3 && length(ma) >= 3)
           VALID = VALID && (o >= mi(3)) && (o <= ma(3));
        end
        if (~VALID)
            MSG = sprintf('Invalid dimension of %s.', name);
        end
    end
end

function check_value(A, values, name)
% Check whether the matrix values are within range
    global VALID MSG;
    if VALID
        if (~isempty(A))
            VALID = isempty(setdiff(A(:), values));
            if (~VALID)
                MSG = sprintf('%s contains invalid values.', name);
            end
        end
    end
end