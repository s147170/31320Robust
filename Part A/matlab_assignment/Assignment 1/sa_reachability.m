function [reachable,notReachable] = sa_reachability(sys, onlyInputs)
%SA_REACHABILITY Examine reachability of a structural analysis system.
%	[REACHABLE,NOTREACHABLE]=SA_REACHABILITY(SYS) examines if the system
%   SYS is structurally reachable. REACHABLE and NOTREACHABLE return a
%   vector of indices of the unknown variables that are reachable and not
%   reachable, respectively. Disabled constraints and variables are not
%   included in this analysis.
%
%   [REACHABLE,NOTREACHABLE]=SA_REACHABILITY(SYS,ONLYINPUTS) further
%   allows to only include the inputs (ONLYINPUTS = 1) as known variables
%   in order to examine the system for structurally controllability.
%
%   The function investigates the equal powers of the adjacency matrix 
%   ADJ = [0      INC]
%         [INCS'  0  ]
%   to find walks from the known variables to the unknowns. If
%   ADJ^k(i,j) = 1 then there is a walk of length k between the i'th row
%   and the j'th column by investigating the elements where i's are the row
%   indices corresponding to known variables and j's are the column indices
%   corresponding to unknown variables (marked with x's on the drawing
%   below).
%
%                      k1 k2 x1 x2
%   ADJ = [           |           ] c1
%         |           |           | c2
%         |     0     |           | c3
%         |           |           | c4
%         |-----------------------|
%      k1 |           |           | 
%      k2 |           |           |
%      x1 |           |x x        |
%      x2 [           |x x        ]
%          c1 c2 c3 c4
%
%   Example:
%       % Examine controllability of random system.
%       sys = sa_rand();
%       [reach,nonReach] = sa_reachability(sys);
%       [~,~,~,unknowns] = sa_names(sys);
%       disp('Controllable reachable variables:');
%       disp(unknowns(reach));
%
%       % More elegant is to use sa_disp().
%       sa_disp(sys, 'r');
%
%   See also: sa_adj sa_controllability sa_disp.

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 13/10-2012
%   Based on reachability from Torsten Lorentzen
%   Last modified - 14/01-2013

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

if nargin == 1
  onlyInputs = 0;
end

% Get enabled part of the incidence matrix
incM = sys.incMatrix(sys.enabledCons, :);

% Retrieve length of constraints and variables
[numCons, numVars] = size(incM);
% Calculate number of knowns
numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);

% Preallocate memory
reachable = false(1, numVars - numKnowns);
adj = cell(1, 2 + floor((numCons+numVars) / 2));

% Get adjacency matrix
adj{1} = sa_adj(sys, onlyInputs);
adj{2} = adj{1}*adj{1};
adj{2} = double(adj{2}&adj{2});

j = 3;
for i = 2:2:numCons+numVars
  adj{j} = (adj{j-1}*adj{2});
  adj{j} = double(adj{j}&adj{j});
  mapKnownToUnknown = adj{j}(numCons+numKnowns+1:numCons+numVars, ...
      numCons+1:numCons+numKnowns);
  % OR the reachable vector with itself so it only contains ones and zeros
  reachable = sum(mapKnownToUnknown,2)' | reachable;
  j = j+1;
end

% Return indices of the unknowns from the enabled subpart
notReachable = find(~reachable);
reachable = find(reachable);

end