function [detectable,notDetectable,notFail] = sa_detectability(sys, ...
    index, overall)
%SA_DETECTABILITY Examine detectability of faults in the constraints.
%   [DETECTABLE,NOTDETECTABLE,NOTFAIL]=SA_DETECTABILITY(SYS) determines for
%   all parity equations found through matching of the system SYS the
%   detectable, non-detectable and not-failing constraints and returns
%   their indices in DETECTABLE, NOTDETECTABLE and NOTFAIL, respectively.
%   Disabled constraints are not considered.
%
%   Since a matching can find N different sets of parity equations and each
%   set may have different detectability properties, this algorithm
%   determines the detectability of each set separately. The return
%   variables DETECTABLE and NOTDETECTABLE are thus cell row vectors of
%   size N holding the detectability indices of each set in one cell. The
%   non-failable contraints are identical for each set and hence returned
%   once in vector.
%
%   [DETECTABLE,NOTDETECTABLE,NOTFAIL]=SA_DETECTABILITY(SYS, INDEX) returns
%   the detectability properties only for the parity equations specified in
%   the vector INDEX. Use an empty vector to analyze all matchings.
%
%   [DETECTABLE,NOTDETECTABLE,NOTFAIL]=SA_DETECTABILITY(SYS, INDEX, 1)
%   returns the detectability over all specified parity equations. The
%   variables DETECTABLE and NOTDETECTABLE return in this case two vectors
%   (instead of cell vectors).
%   
%   Detectable constraints are all those that have a non-zero signature in
%   all parity equations.
%
%   Example:
%       % Examine detectability of random system.
%       sys = sa_rand();
%       sys = sa_match(sys);
%       [detect,nonDetect] = sa_detectability(sys);
%       for i = 1:size(detect, 1)
%           fprintf('Detectable faults for matching %d:\n', i);
%           disp(detect{i});
%       end
%
%       % More elegant is to use sa_disp().
%       sa_disp(sys, 'd');
%
%   See also: sa_disp sa_isolability sa_match.

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 24/10-2012
%   Based on detectable.m by Torsten Lorentzen - 02/12-2003
%   Last modified - 14/01-2013

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Check arguments
if (nargin == 1 || ~isvector(index) || ~isnumeric(index))
    index = 1:size(sys.parityEqs, 1);
else
    % Filter valid indices from index vector.
    index = intersect(index, 1:size(sys.parityEqs, 1));
end

if (nargin < 3)
    overall = false;
end

% The notFail vector is independent of the sets of parity equations and
% thus always the same.
notFail = setdiff(sys.enabledCons, sys.canFail);
% Get vector of enabled constraints that can also fail.
enabledAndCanFailCons = intersect(sys.enabledCons, sys.canFail);

% Check if parity equations exist
if isempty(sys.parityEqs)
    % If no parity equations exist, the detectable set is empty, the
    % non-isolable set consists of all contraint that are enabled and can
    % fail and the notFail part is the part of the enable constraints that
    % are not non-isolable.
    detectable = {[]};
    notDetectable = {enabledAndCanFailCons};
else
    if (~overall)
        % Preallocate memory for detectable and non-detectable part. Since
        % the parts can differ for each set of parity equations, cell
        % arrays are necessary.
        detectable = cell(length(index), 1);
        notDetectable = cell(length(index), 1);
        % For each set of parity equations return the isolable and
        % non-isolable part.        
        for i=1:length(index)
            [detectable{i},notDetectable{i}] = ...
                get_detectability(sys.parityEqs{i}, enabledAndCanFailCons);
        end
    else
        % Check the complete set of parity equations for isolability.
        [detectable,notDetectable] = ...
                get_detectability(cell2mat(sys.parityEqs(index)), ...
                enabledAndCanFailCons);
    end
end

end

function [detectable,notDetectable] = get_detectability(parityEqs, ...
    enabledAndCanFailCons)
    % Retrieve indices of non-zero columns.
    if size(parityEqs, 1) > 1
        detectable = enabledAndCanFailCons(...
            any(parityEqs(:, enabledAndCanFailCons)));
    else
        detectable = enabledAndCanFailCons(logical(...
            parityEqs(:, enabledAndCanFailCons)));
    end
    % The non-detectable part consists out of all enabled and failable
    % constraints minus the constraints in the detectable part.
    notDetectable = setdiff(enabledAndCanFailCons, detectable);
end

