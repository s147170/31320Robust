function incMatrix = sa_incidencematrix(cons,inputs,measurements,unknowns,consoneway)

% function incMatrix = sa_incidencematrix(cons,inputs,measurements,unknowns,cons_oneway)
% 
% Function for automatic generation of an incidence matrix for a given system. 
% 
% Example: see exampleSingleTank.m in the "examples" folder
% 
% Method: 
% Search for the occurrence of variable names in constraints. Set a +1 in
% in the incidence matric position (i,j) if constraint "i" includes variable
% "j". If variable "j" is marked as "oneway" for constraint "i", this variable 
% cannot be calculated from the constraint "i" hencethe incidence matrix entry (i,j) is set to -1
%
%
% Input:
% cons(3,:)         Character array that contains expressions for constraintws in symbolic form 
% inputs(1,:)       Character array containing names of input variables
% measurements(1,:) Character array containing names of measured variables
% unknowns(1.:)     Character array containing names of unknown variables
% consoneway        Cell array that for each constraint specifies variable(s) that cannot be
%                   calculated by the constraint. Used for differential or nonlinear constraints 
%                   for which certain variablea can not be calculated
%
% example: cons_oneway = {[],[],[],[],{'h','g'}} means that, in constraint #5
% the variables 'h' and 'g' cannot be determined resulting in a -1 in the
% incidence matrix. NOTE that consoneway is a CELL array, hence is
% different from the data structure of cons, inputs, etc. 
% This wille be % CHANGED in a future release of SaTool
%
%   Copyright 2002-2014 Automation and Control Group, 
%   Technical University of Denmark
%   Created by Thomas Falkenberg - Nov 2013
%   Last modified
%

% NOTE: Please do be aware that the
% incidence matrix must have colums ordered as: 
% input, measurements, unknowns

vars = [inputs,measurements,unknowns];
incMatrix = zeros(size(cons,2),size(vars,2));

for i = 1:size(incMatrix,1) % for every constraint                            
    for j = 1:size(incMatrix,2) % for every variable
        match = regexp(cons{3,i}, ['(?<![A-Za-z0-9])',vars{1,j},'(?![A-Za-z0-9])'],'ONCE');
        if ~isempty(match);
            incMatrix(i,j) = 1;
            if ~isempty(consoneway{i})
                for k = 1:length(consoneway{i})
                    if strcmp(consoneway{i}{k},vars{1,j})
                        incMatrix(i,j) = -1;
                    end
                end
            end
        end
    end
end