function [ sys ] = sa_rand( m, n, ni, nm )
%SA_RAND Create random structural analysis system.
%   SYS=SA_RAND(M,N,NI,NM) creates a structural analysis system with an
%   incidence matrix of size MxN and NI and NM number of inputs and
%   measurements, respectively.
%
%   None of the four parameters is mandatory. The missing parameters are
%   calculated according to the following formulas:
%       M = 10
%       N = M   
%       NI = floor(N / 4)
%       NM = NI
%
%   By default thus as system of size 10x10 with 10 constraints, 2 inputs,
%   2 measurement and 6 unknown variables is created. Note that no
%   guarantee is given whether a matching for the found system exists.
%
%   Example:
%       % Create random system and display it.
%       sys = sa_rand();
%       sa_disp(sys, 'm');
%
%   See also: sa_create

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 16/10-2012
%   Last modified - 01/12-2012

% Set default parameters
if (nargin < 1)
    m = 10;
end
if (nargin < 2)
    n = m;
end
if (nargin < 3)
    ni = floor(n / 4);
end
if (nargin < 4)
    nm = ni;
end

% Create random uniformly-distributed incidence matrix.
incM = rand(m, n);
incM(incM > 0.666) = -1;
incM(incM > 0.333) = 0;
incM(incM > 0) = 1;

% Create system with default names
sys = sa_create(incM, ni, nm);
    
end

