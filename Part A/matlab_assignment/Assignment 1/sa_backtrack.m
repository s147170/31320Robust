function [sys, symbolic,solveOrder, analytic,analytic_sym] = sa_backtrack(sys, index)
%SA_BACKTRACK Backtrack matchings to obtain parity equations.
%   SYS = SA_BACKTRACK(SYS) performs a backtracking of all obtained
%   matchings in SYS and returns a cell array in SYS.parityEqs that holds
%   in each cell c{i} a two-dimensional matrix c{i} = P. The element
%   cN = P(j, k) is one (1) if the k-th constraint is included in the j-th
%   parity equation of the i-th matching. The length of the cell array
%   SYS.parityEqs is equal to the number of rows in SYS.matchings.
%
%   Typically this method is implicitly called by sa_match() to obtain
%   parity equations for the matchings that were found.
%
%   [SYS,SYMBOLIC] = SA_BACKTRACK(SYS) further returns a cell array of the
%   same length as sys.parityEqs where each cell s{i} contains a second
%   cell array s{i}{j} with the symbolic expression for the j-th parity
%   equation of the i-th matching.
%
%   [SYS,SYMBOLIC,ANALYTIC] = SA_BACKTRACK(SYS) further returns a cell
%   same length as sys.parityEqs where each cell a{i} contains a second
%   cell array a{i}{j} with the analytic expression for the j-th parity
%   equation of the i-th matching. For this to work the analytic expression
%   of each constraint must be specified in the third row of SYS.cons in
%   laplacian form. Note that this method relies on solve() and is thus
%   time-consuming and restricted by the same limitations.
%
%   [...] = SA_BACKTRACK(SYS, INDEX) further only obtains
%   the parity equations for the matchings defined in INDEX. Use an empty
%   vector to obtain all party equations.
%
%   The back-tracking of one matching is recursive and has a complexity of
%   O(n) where n is the number of constraints of the system.
%
%   Example: See sa_match().
%
%   See also: sa_match solve.

%   TODO: Improve speed and remove recursions!

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 23/10-2012
%   Last modified - 15/02-2014

% Check system
if exist('analytic_sym','var') == 0
    analytic_sym= [];
end
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

numMatchings = size(sys.matchings, 1);

% Check input variables
if nargin < 2 || isempty(index) || ~isvector(index)
    index = 1:numMatchings;
end
% Filter valid indices from index vector.
index = intersect(index, 1:numMatchings);
    
% Preallocate memory.
numCons = size(sys.incMatrix, 1);
sys.parityEqs = cell(numMatchings, 1);
if nargout > 1
    symbolic = cell(numMatchings, 1);
    solveOrder = cell(numMatchings,1);
end
if nargout > 3
    if size(sys.cons, 1) >= 3 && ~isempty(strjoin(sys.cons(3, :), ''))
        analytic = cell(numMatchings, 1);
        analytic_sym = cell(numMatchings, 1);
    else
        analytic = {};
        analytic_sym = {};
    end
end
numUnknowns = size(sys.unknowns, 2);


% TEMPORARY BUG FIX: RELY ON DEFAULT RECURSION LIMIT OF 500
% Problem: The symbolic solver also relies on the global recursion limit.
% Therefore it is not valid to set the global recursion limit to the
% necessary number for backtracking since the symbolic solver might need
% more recursion. Normally 500 recursions should be more then enough when
% working with normal systems. However since the number of recursions is
% proportional to the square of contraints, systems with more than 22
% constraints might fail under this setting. Future work shall completely
% replace the recursions in order to improve performance and avoid these
% drawbacks.

% TEMPORARY BUG FIX2: Set temporary recursion limit of max(500,numCons^2)

% Store current recursion limit and set temporarily a new recursion limit.
recursionLimit = get(0, 'RecursionLimit');
set(0, 'RecursionLimit', max(numCons*numCons,recursionLimit));

% Preallocate memory.
consBuffer = false(numCons, numCons);

% Loop through all matchings
for i = index
    % Find unmatched constraints that are for use (=0).
    unmatchedCons = find(sys.matchings(i, :) == 0);
    % Reshape matchings matrix to have the unknown variables as the indices
    % of a vector with the matched constraints as its elements.
    matching = zeros(1, numUnknowns);
    for j = 1:size(sys.matchings(i, :), 2)
        if sys.matchings(i, j) > 0
            matching(sys.matchings(i, j)) = j;
        end
    end
    % Assign memory for constraint and symbolic buffer.
    consBuffer(:) = false;
    if nargout > 1
        symbBuffer = cell(1, numCons);
    end
    %consOrder = cell(length(unmatchedCons)-1,numCons); %TODO: Bugreport
    consOrder = cell(numCons,numCons);
    orderNum = 1;
    % Loop through all unmatched constraints.
    for cons=unmatchedCons
        if nargout <= 1
            consBuffer = recursive_backtrack(sys, matching, cons, ...
                consBuffer);
        else
            consOrder{orderNum,cons} = cons;
            [consBuffer, symbBuffer,consOrder] = recursive_backtrack(sys, ...
                matching, cons, consBuffer, symbBuffer,consOrder,orderNum+1);
        end
    end
    % Return current set of parity equations
    sys.parityEqs{i} = consBuffer(unmatchedCons, :);
    % Return current set of symbolic expressions.
    if nargout >= 2
        symbolic{i} = symbBuffer(unmatchedCons);
        solveOrder{i} = consOrder;
    end
    if nargout >= 4 && ~isempty(analytic) && ~isempty(unmatchedCons)
        [analytic{i},analytic_sym{i}] = get_analytic(sys, i, consBuffer, unmatchedCons);
    end
end

% Restore old recursion limit.
set(0, 'RecursionLimit', recursionLimit);

end

function [consBuffer,symbBuffer,consOrder] = recursive_backtrack(sys, ...
    matching, consIndex, consBuffer, symbBuffer,varargin)
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    % Mark current constraint in constraints buffer.
    consBuffer(consIndex, consIndex) = true;
    % Create temporary symbolic expression for use in equations  that use
    % constraints recursively.
    if (nargout > 1)
        symbBuffer{consIndex} = [sys.cons{1, consIndex}, '(...)'];
    end

    % Array with indices refering to all constraints used in this
    % constraint. The current constraint is excluded from this set.
    usedCons = matching(...
        logical(sys.incMatrix(consIndex, numKnowns+1:end)));
    usedCons = usedCons(usedCons~=consIndex);
    % Loop through all unknown variables
    if (nargout > 1)
        consOrder = varargin{1};
        orderNum = varargin{2};
        if size(consOrder,consIndex) < orderNum
            consOrder{orderNum,consIndex} = usedCons;
        else
            consOrder{orderNum,consIndex} = [consOrder{orderNum,consIndex},usedCons];
        end
    end
    for i=1:length(usedCons)
        % Backtrack current unknown variable, if it has not been
        % backtracked before.
        if (~consBuffer(usedCons(i), usedCons(i)))
            % Mark used constraint in current constraint.
            consBuffer(consIndex, usedCons(i)) = true;
            if nargout <= 1
                consBuffer = recursive_backtrack(sys, matching, ...
                    usedCons(i), consBuffer);
            else
                [consBuffer, symbBuffer,consOrder] = recursive_backtrack(sys, ...
                    matching, usedCons(i), consBuffer, ...
                    symbBuffer,consOrder,orderNum+1);
            end
        end
        % Add matched constraint in current constraint.
        consBuffer(consIndex, :) = consBuffer(consIndex, :) | ...
            consBuffer(usedCons(i), :);
    end
    % Build symbolic expression
     if nargout > 1
         % Get all known variables, the current constraint and all used
         % variables and build the symbolic expression.
         [~, vars] = sa_names(sys, 1);
         knownsStr = strjoin(vars(1, ...
             logical(sys.incMatrix(consIndex, 1:numKnowns)), 1), ',');
         consStr = sys.cons{1, consIndex};
         % Check whether there are constraints in use.
         if ~isempty(usedCons)
             symbStr = strjoin(symbBuffer(usedCons), ',');
         else
             symbStr = [];
         end
         if (~isempty(knownsStr) && ~isempty(symbStr))
             symbBuffer{consIndex} = ...
                 [consStr, '(', knownsStr, ',', symbStr, ')'];
         else
             symbBuffer{consIndex} = ...
                 [consStr, '(', knownsStr, symbStr, ')'];
         end
     end
end

function [analytic,analytic_sym] = get_analytic(sys, matchingIndex, consBuffer, ...
    unmatchedCons)
    % Check whether equations for constraints exist.
    analytic_sym = [];
    if (size(sys.cons, 1) < 3) || isempty(unmatchedCons)
        analytic = {};
        return;
    end
    % Retrieve number of constraints and known variables.
    numCons = size(sys.incMatrix, 1);
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    % Get all constraints that are necessary to calculate a unique
    % solution.
    usedCons = zeros(1, numCons);
    for cons = unmatchedCons
        usedCons = usedCons | consBuffer(cons, :);       
    end
    % Remove unmatched constraints since they are not necessary to obtain a
    % unique solution.
    usedCons(unmatchedCons) = 0;
    usedCons = find(usedCons);
    % Gat all unknowns variables that are necessary to calculate a unique
    % solution.
    usedUnknowns = zeros(1, size(sys.incMatrix, 2) - numKnowns);
    for cons = usedCons
        usedUnknowns = usedUnknowns | sys.incMatrix(cons, numKnowns+1:end);
    end
    % Solve constraints for the unknown variables. Hide warning when an
    % implicit solution cannot be found.
    s = warning('off', 'symbolic:solve:warnmsg3');
    res = solve(sys.cons{3, usedCons}, sys.unknowns{1, usedUnknowns});
    % Restore old warning settings.
    warning(s);
    % Preallocate memory for analytic expressions.
    analytic = cell(1, length(unmatchedCons));
    if ~isempty(res)
        % Loop through all unmatched constraints (= parity equations).
        for i = 1:length(unmatchedCons)
            analytic{i} = sys.cons{3, unmatchedCons(i)};
            % Retrieve indices of unknown variables that are used in this
            % parity equation.
            usedUnknowns = ...
                find(sys.incMatrix(unmatchedCons(i), numKnowns+1:end));
            % Replace all unknown variables by known variables obtain before by
            % solivng the set of equations.
            for j = usedUnknowns
            %for j = 1:length(sys.unknowns) % Run through all unknowns to make sure everything is replaced (less efficient but should always work)
                unknown = sys.unknowns{1, j};
                %sprintf('replace %s with %s in  %s ',char(sys.unknowns{1, j}),char(res.(unknown)),char(analytic{i}))
                if length(fieldnames(res)) < 1
                    analytic{i} = subs(analytic{i}, unknown, res);
                else
                    analytic{i} = subs(analytic{i}, unknown, res.(unknown));
                end
                %analytic{i} = subs(analytic{i}, unknown, res.(unknown),0); % added flag to only replace left to right
            end
            % Convert symbol into a character string.
            if(strcmp(class(analytic{i}),'sym'))
                analytic_sym{i} = analytic{i};
            elseif (strcmp(class(analytic{i}),'char'))
                analytic_sym{i} = sym(analytic{i});
            end
            analytic{i} = char(analytic{i});
        end
    else
        warning('satools:noAnalyticSolutionForMatching', ...
            'No analytic solution for matching %d found.', matchingIndex);
    end
end