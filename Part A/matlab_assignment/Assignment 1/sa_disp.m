function sa_disp(sys, format, varargin)
%SA_DISP Display information about a structural analysis system.
%   plot(SYS) displays the content of the structural analysis system SYS in
%   a human readable matter. By default only the number of constraints and
%   variables are plotted.
%
%   plot(SYS, format) displays only the content specified in FMT of the
%   system. FORMAT is a string containing a combination of the following
%   formatting options:
%       'm': Display incidence matrix of the system
%       'c': Display controllability of the system
%       'r': Display reachability of the system
%       'd': Display detectability of parity equations (in selection).
%       'D': Display detectability over all parity equations (in selection).
%       'i': Display isolability of parity equations (in selection).
%       'I': Display isolability over all parity equations (in selection).
%       't': Display matchings (in selection).
%       'p': Display parity equations (in selection).
%       's': Display symbolic parity equations (in selection).
%       'a': Display analytic parity equations (in selection).
%       'o': Display canonical decomposition.
%
%   plot(SYS, format, ...) is as above but limits the analysis to a
%   selection of certain elements. The selection is either a scalar or a
%   vector of indices and not supported by every formatting element. See
%   the list above for the support of selections.
%
%   Example:
%       % Display general information about a system.
%       sa_disp(sys);
%
%       % Display matchings.
%       sa_disp(sys, 't');
%
%       % Display only the first two matchings.
%       sa_disp(sys, 't', [1:2]);
%
%       % Display on the first matching and the parity equations for the
%       % second matching.
%       sa_disp(sys, 'tp', 1, 2);
%
%   See also: sa_create.

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 12/10-2012
%   Last modified - 15/02-2014
%
%   TODO: Display domain and can fail?
%   TODO: Implement line break after MAX_WIDTH characters

% Define max width of display
global MAX_WIDTH;
MAX_WIDTH = get(0, 'CommandWindowSize');

% Check input arguments
[valid,msg] = sa_check(sys);
if (~valid)
    error(msg);
end
if (nargin == 2 && (~ischar(format)))
    error('Invalid format specifier.');
end

% Set default parameters.
if (nargin < 2)
    format = '';
end

% Concate all variables in one string cell for easier handling.
[cons, inputs, meas, unknowns] = sa_names(sys);
vars = [inputs, meas, unknowns];

% Obtain unused and enabled variables.
allVars = 1:size(vars, 2);
usedVars = allVars(any(sys.incMatrix(:, :)));
unusedVars = setdiff(allVars, usedVars);
% List variables that are not used within the enabled constraints.
enabledVars = 1:size(sys.incMatrix, 2);
enabledVars = enabledVars(any(sys.incMatrix(sys.enabledCons, :)));
% Obtain subset of enabled unknown variables.
numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
enabledUnknowns = intersect(enabledVars, numKnowns+1:size(vars, 2)) - ...
    numKnowns;

% Display general information
if isempty(format)
    disp_general(sys, unusedVars, enabledVars, vars);
end

% Parse format strings
index = 1;
for i = 1:length(format)
    % Add newline inbetween
    switch format(i)
        case 'm'
            disp_inc_matrix(sys, enabledVars, vars);
            fprintf('\n');
            
        case 'c'
            disp_constrollability(sys, enabledUnknowns, unknowns);
            fprintf('\n');
        case 'r'
            disp_reachability(sys, enabledUnknowns, unknowns);
            fprintf('\n');
            
        case 'd'
            if (size(varargin, 2) >= index)
                disp_detectability(sys, varargin{index});
            else
                disp_detectability(sys, []);
            end
            index = index + 1;
            fprintf('\n');
            
        case 'D'
            if (size(varargin, 2) >= index)
                disp_detectability(sys, varargin{index}, 1);
            else
                disp_detectability(sys, [], 1);
            end
            index = index + 1;
            fprintf('\n');
            
        case 'i'
            if (size(varargin, 2) >= index)
                disp_isolability(sys, varargin{index});
            else
                disp_isolability(sys, []);
            end
            index = index + 1;
            fprintf('\n');
            
        case 'I'
            if (size(varargin, 2) >= index)
                disp_isolability(sys, varargin{index}, 1);
            else
                disp_isolability(sys, [], 1);
            end
            index = index + 1;
            fprintf('\n');
            
        case 't'
            if (size(varargin, 2) >= index)
                disp_matchings(sys, varargin{index});
            else
                disp_matchings(sys, []);
            end
            index = index + 1;
            fprintf('\n');
            
        case 'p'
            if (size(varargin, 2) >= index)
                disp_parity_eqs(sys, varargin{index});
            else
                disp_parity_eqs(sys, []);
            end
            index = index + 1;
            fprintf('\n');
            
        case 's'
            if (size(varargin, 2) >= index)
                disp_symbolic_eqs(sys, varargin{index});
            else
                disp_symbolic_eqs(sys, []);
            end
            index = index + 1;
            fprintf('\n'); 
            
        case 'a'
            if (size(varargin, 2) >= index)
                disp_analytic_eqs(sys, varargin{index});
            else
                disp_analytic_eqs(sys, []);
            end
            index = index + 1;
            fprintf('\n');             
         
        case 'o'
            disp_decomp(sys, enabledUnknowns, cons, unknowns);
            fprintf('\n');
            
        otherwise
            % Ignore unknown characters.
    end
end

end

%% Display general information
function disp_general(sys, unusedVars, enabledVars, vars)
%   Displays general information for a system SYS
    [numCons, numVars] = size(sys.incMatrix);
    numInputs = size(sys.inputs, 2);
    numMeas = size(sys.meas, 2);
    numUnknowns = numVars - numInputs - numMeas;
    
    % Print out general informations
    str = 'System with ';
    space = repmat(' ', 1, length(str));
    fprintf(['%s%d constraint(s), %d input(s),', ...
        ' %d measurement(s) and\n%s%d unknown variable(s).\n'], ...
        str, numCons, numInputs, numMeas, space, ...
        numUnknowns);
    
    % Print disabled constraints.
    if (length(sys.enabledCons) ~= numCons)
        disabledCons = setdiff(1:numCons, sys.enabledCons);
        fprintf('\nDisabled constraint(s): %s.\n', ...
            strjoin(sys.cons(1, disabledCons), ', '));
    end
    
    % Print unused variables.
    if (~isempty(unusedVars))
        fprintf('Unused variable(s)    : %s.\n', ...
            strjoin(vars(1, unusedVars), ', '));
    end
    % Print disabled variables.
    allVars = 1:numVars;
    disabledVars = setdiff(allVars, union(enabledVars, unusedVars));
    if (~isempty(disabledVars))
        fprintf('Disabled variable(s)  : %s.\n', ...
            strjoin(vars(1, disabledVars), ', '));
    end
    
    % Print number of matchings.
    if (~isempty(sys.matchings))
        fprintf('Number of matchings: %d.\n', ...
            size(sys.matchings, 1));
    end
        
end

%% Diplay incidence matrix
function disp_inc_matrix(sys, enabledVars, vars)
%   Displays the incidence matrix of SYS in a human readable matter.
    % Create formatted incidence matrix
    mat = format_matrix(sys.incMatrix(sys.enabledCons, enabledVars), ...
        '1', '', 'X');
    % Set other display properties.
    rowNames = sys.cons(1, sys.enabledCons);
    columnNames = vars(1, enabledVars);
    rowIdent = ' | ';
    columnIdent = ' | ';
    % Display matrix.
    disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent);
end

%% Display matchings
function disp_matchings(sys, index)
    function inc = format_matrix_func(inc)
        if (inc) > 0
            inc = sys.unknowns(1, inc);
        elseif (inc == 0)
            inc = {'0'};
        else
            inc = {'-'};
        end        
    end

    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.matchings, 1);
    else
        % Filter valid indices from index vector.
        index = intersect(index, 1:size(sys.matchings, 1));        
        if ~isrow(index)
            % Transform to row vector for display purposes.
            index = index';
        end
    end
    if (~isempty(sys.matchings))
        % Format each cell of the matching matrix.
        mat = cellfun(@(x) format_matrix_func(x), num2cell(...
            sys.matchings(index, sys.enabledCons)));
        % Define other display properties.
        rowNames = cellstr(num2str(index'));
        columnNames = sys.cons(1, sys.enabledCons);
        rowIdent = ' | ';
        columnIdent = ' | ';
        disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent);
    else
        disp('No matchings found, maybe try sa_match()');
    end
end

%% Display parity equations
function disp_parity_eqs(sys, index)
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.parityEqs, 1);
    else
        % Filter valid indices from index vector.
        index = intersect(index, 1:size(sys.parityEqs, 1));
    end
    if (~isempty(sys.parityEqs))
        numCons = size(sys.incMatrix, 1);
        % Create names for parity equations.
        rowNames = strcat('r', cellstr(num2str((1:numCons)', '%-1.0f'))');
        % Define other display properties.
        columnNames = sys.cons(1, sys.enabledCons);
        rowIdent = ' | ';
        columnIdent = ' | ';
        % Loop through all sets of parity equations
        for i=1:length(index)
            if ~isempty(sys.parityEqs{index(i)})
                fprintf('Matching %d:\n', index(i));
                % Create formatted matrix and display it.
                mat = format_matrix(...
                    sys.parityEqs{index(i)}(:, sys.enabledCons), 'X', '', '');
                disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent);
                % Add a new line expect for the last set.
                if (i ~= length(index))
                    fprintf('\n');
                end
            end
        end
    else
        disp('No parity equations found, maybe try sa_backtrack().');
    end
end

%% Display symbolic equations
function disp_symbolic_eqs(sys, index)
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.parityEqs, 1);
    else
        % Filter valid indices from index vector.
        index = intersect(index, 1:size(sys.matchings, 1));
    end
    if (~isempty(sys.matchings))
        [~, symbolic, ~] = sa_backtrack(sys, index);
        % Loop through all sets of parity equations
        for i=1:length(index)
            if ~isempty(symbolic{index(i)})
                fprintf('Matching %d:\n', index(i));
                % Create formatted matrix and display it.
                for j = 1:length(symbolic{index(i)})  % MB 19 jan 2014
                    fprintf('  %s\n', symbolic{index(i)}{j});
                end
                % Add a new line expect for the last set.
                if (i ~= length(index))
                    fprintf('\n');
                end
            end
        end
    else
        disp('No matchings found, maybe try sa_match().');
    end
end

%% Display analytic equations
function disp_analytic_eqs(sys, index)
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.parityEqs, 1);
    else
        % Filter valid indices from index vector.
        index = intersect(index, 1:size(sys.matchings, 1));
    end
    if (~isempty(sys.matchings))
        [~, ~, ~, analytic] = sa_backtrack(sys, index);
        % Loop through all sets of parity equations
        for i=1:length(index)
            if ~isempty(analytic{index(i)})
                fprintf('Matching %d:\n', index(i));
                % Create formatted matrix and display it.
                for j = 1:length(analytic{index(i)})  % MB 19 jan 2014
                    fprintf('  %s\n', analytic{index(i)}{j});
                end
                % Add a new line expect for the last set.
                if (i ~= length(index))
                    fprintf('\n');
                end
            end
        end
    else
        disp('No matchings found, maybe try sa_match().');
    end
end

%% Display controllability
function disp_constrollability(sys, enabledUnknowns, unknowns)
%   Displays constrollability of system SYS
    [controllable, notControllable] = sa_controllability(sys);
    % Remove disabled variables.
    controllable = intersect(enabledUnknowns, controllable);
    notControllable = intersect(enabledUnknowns, notControllable);
    % Display controllability.
    if (isempty(notControllable))
        fprintf('The system is fully controllable.\n');
    else
        fprintf('Controllable variable(s)    : %s.\n', ...
            strjoin(unknowns(1, controllable), ', '));
        fprintf('Non-controllable variable(s): %s.\n', ...
            strjoin(unknowns(1, notControllable), ', '));
    end
end

%% Display reachability
function disp_reachability(sys, enabledUnknowns, unknowns)
%   Displays reachability of system SYS
    [reachable, notReachable] = sa_reachability(sys);
    % Remove disabled variables.
    reachable = intersect(enabledUnknowns, reachable);
    notReachable = intersect(enabledUnknowns, notReachable);
    % Display reachability.
    if (isempty(notReachable))
        fprintf('The system is fully reachable.\n');
    else
        fprintf('Reachable variable(s)    : %s.\n', ...
            strjoin(unknowns(1, reachable), ', '));
        fprintf('Non-reachable variable(s): %s.\n', ...
            strjoin(unknowns(1, notReachable), ', '));
    end
end

%% Display detectability
function disp_detectability(sys, index, overall)
%   Display detectability of system SYS
    if (nargin < 3)
        overall = false;
    end
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.parityEqs, 1);
    else
        index = intersect(index, 1:size(sys.parityEqs, 1));
        if (~isrow(index))
            index = index';
        end
    end
    if (~isempty(sys.parityEqs))
        [detectable, ~, notFail] = sa_detectability(sys, index, overall);
        if (~iscell(detectable))
            detectable = {detectable};
        end
        mat = zeros(size(detectable, 1), size(sys.cons, 2));
        mat(:, notFail) = -1;
        % Set all isolable constraints to one (1).
        for i = 1:size(detectable, 1)
            mat(i, detectable{i}) = 1;
        end
        mat = format_matrix(mat(:, sys.enabledCons), 'D', '', 'N');
        % Define other display properties.
        rowNames = cellstr(num2str(index'));
        columnNames = sys.cons(1, sys.enabledCons);
        rowIdent = ' | ';
        columnIdent = ' | ';
        disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent);
    else
        disp('No parity equations found, maybe try sa_backtrack().');
    end
end

%% Display isolability
function disp_isolability(sys, index, overall)
%   Display isolability of system SYS
    if nargin < 3
        overall = false;
    end
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.parityEqs, 1);
    else
        index = intersect(index, 1:size(sys.parityEqs, 1));
        if (~isrow(index))
            index = index';
        end
    end
    if (~isempty(sys.parityEqs))
        [isolable, ~, notFail] = sa_isolability(sys, index, overall);
        if (~iscell(isolable))
            isolable = {isolable};
        end
        mat = zeros(size(isolable, 1), size(sys.cons, 2));
        % Set all isolable constraints to one (1).
        for i = 1:size(isolable, 1)
            mat(i, isolable{i}) = 1;
        end
        % Set all constraints that cannot fail to minus one (-1).
        mat(:, notFail) = -1;
        mat = format_matrix(mat(:, sys.enabledCons), 'I', '', 'N');
        % Define other display properties.
        rowNames = cellstr(num2str(index'));
        columnNames = sys.cons(1, sys.enabledCons);
        rowIdent = ' | ';
        columnIdent = ' | ';
        disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent);
    else
        disp('No parity equations found, maybe try sa_backtrack().');
    end
end

%% Display canonical decomposition
function disp_decomp(sys, enabledUnknowns, cons, unknowns)
%   Displays reachability of system SYS
    [uc,uu,jc,ju,oc,ou] = sa_decomp(sys);
    % Display reachability.
    if (length(oc) == length(sys.enabledCons))
        fprintf('The system is fully over-determined.\n');
    else
        % Obtain enabled unknown variables.
        uu = intersect(enabledUnknowns, uu);
        ju = intersect(enabledUnknowns, ju);
        ou = intersect(enabledUnknowns, ou);
        % Print results.        
        fprintf('Over-determined subsystem:\n');
        if isempty(oc) && isempty(ou)
            fprintf('  [ ]\n\n');
        else
            fprintf('  Constraint(s): %s.\n', strjoin(cons(1, oc), ', '));
            fprintf('  Variable(s)  : %s.\n\n', ...
                strjoin(unknowns(1, ou), ', '));
        end
        fprintf('Just-determined subsystem:\n');        
        if isempty(jc) && isempty(ju)
            fprintf('  [ ]\n\n');
        else
            fprintf('  Constraint(s): %s.\n', strjoin(cons(1, jc), ', '));
            fprintf('  Variable(s)  : %s.\n\n', ...
                strjoin(unknowns(1, ju), ', '));
        end
        fprintf('Under-determined subsystem:\n');        
        if isempty(uc) && isempty(uu)
            fprintf('  [ ]\n\n');
        else
            fprintf('  Constraint(s): %s.\n', strjoin(cons(1, uc), ','));
            fprintf('  Variable(s)  : %s.\n', ...
                strjoin(unknowns(1, uu), ', '));
        end
    end
end

%% Helper functions
function disp_matrix(mat, rowNames, columnNames, rowIdent, columnIdent)
%   Displays the cell string matrix MAT 
    global MAX_WIDTH;

    if isempty(mat)
        return;
    end
    
    % Get size of cell matrix to display
    [numRows, numCols] = size(mat);
    
    % Get length of largest element of the row names and the column names
    % and matrix elements, respectively.
    lRow = length_of_largest_element(rowNames);
    m = mat(:)';
    lColumn = length_of_largest_element([columnNames, m]);

    % Calculate length of headers.
    lLeft = lRow + length(rowIdent);
    lTop = lColumn + length(columnIdent);
       
    % Calculate the number of columns that can be displayed
    numColsToDisp = min(floor((MAX_WIDTH - lLeft) / lTop), numCols);
    if (numColsToDisp <= 0)
        error('Columns names are too wide to print.');
    end

    % Loop though columns
    for curCols=1:numColsToDisp:numCols
        % Indices of columns to display
        colsToDisp = curCols:min(curCols+numColsToDisp-1,numCols);
        % Display column numbers if necessary
        if (curCols > 1)
           fprintf('\nColumns %d through %d\n\n', colsToDisp(1), ...
               colsToDisp(end));
        end
        % Print column header
        fprintf('%s%s%s\n', repeat_char(lRow, ' '), rowIdent, ...
            make_column_str(columnNames(colsToDisp), lColumn, ...
            columnIdent));
       
        % Calculate the length of a complete print out line
        lLine = lLeft + length(colsToDisp) * lTop - length(columnIdent);
        % Print line to separate header from data
        fprintf('%s\n', repeat_char(lLine, '-'));
        % Loop through all constraints
        for i=1:numRows
            % Print out data
            fprintf('%s%s%s\n', right_align(rowNames{i}, lRow), ...
                rowIdent, make_column_str(mat(i,colsToDisp), lColumn, ...
                columnIdent));
        end
    end    
end

function formattedInc = format_matrix(incM, posSign, zeroSign, negSign)
%   Convert system's incident matrix to a human readable string cell array
    
    function inc = format_matrix_func(inc, posSign, zeroSign, negSign)
        if (inc) > 0
            inc = {posSign};
        elseif (inc == 0)
            inc = {zeroSign};
        else
            inc = {negSign};
        end        
    end

    % Format each cell of the incidence matrix
    formattedInc = ...
        cellfun(@(x) format_matrix_func(x, posSign, zeroSign, negSign), ...
        num2cell(incM));
end

function str = make_column_str(cstr, columnWidth, delimiter)
%   Return string holding strings in cell array cstr separated by ident and
%   right aligned by spaces.
    str = strjoin(right_align(cstr, columnWidth), delimiter);
end

function str = right_align(str, len)
%   Fills the left size of a string with spaces to match length l
    function sout = right_align_func(sin, length)
        m = size(sin, 2);
        if (m < length)
            sout = {[repeat_char(length-m, ' '), sin]};
        else
            sout = {sin};
        end        
    end
    isArray = 0;
    if (~iscell(str))
        str = {str};
        isArray = 1;
    end
    % Call cell function to improve performance
    str = cellfun(@(x) right_align_func(x, len), str);
    if (isArray)
        str = cell2mat(str);
    end
end

function str = repeat_char(l, chr)
%   Returns a string with l number of spaces
    str = repmat(chr, 1, l);
end

function l = length_of_largest_element(cstr)
%   Return size of longest string in a cell array of strings
    l = max(cellfun('size', reshape(cstr, 1, []), 2));
end
