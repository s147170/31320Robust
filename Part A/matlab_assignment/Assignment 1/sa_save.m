function sa_save(sys, fileName, format)
%SA_SAVE Save structural analysis system.
%   SA_SAVE(SYS,FILENAME) saves a structural analysis system SYS to the
%   file specified in FILENAME. The file will be overwritten if it already
%   exists. The file format will determined automatically by checking the
%   file extension. Currently the following file formats are supported:
%       'xml' (recommended):
%           Saves the system to a XML system description file. This file
%           format is recommend since it will also be supported by upcoming
%           versions.
%
%       'mat':
%           Saves the system to a binary MATLAB workspace file. Since the
%           binary format might change over time it is not recommend to
%           store a system in this file format.
%
%   SA_SAVE(SYS,FILENAME, FORMAT) further enforces the file format to be
%   FORMAT. This can be helpful if the file extension of a file is not set
%   accordingly. FORMAT can be any of the above described file formats. 
%
%   Example:
%       % Store system to a XML file.
%       sys = sa_rand();
%       sa_save(sys, 'test.xml');
%
%       % Store system to a XML file without file ending.
%       sa_save(sys, 'test', 'xml');
%
%   See also: sa_load

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 23/10-2012
%   Based on xmlCreateSaveFile
%   Last modified - 02/12-2012

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Check arguments.
if nargin < 3
    % Default method is determined by file extension
    [~, ~, ext] = fileparts(fileName);
    format = lower(ext);
else
    format = ['.', lower(format)];
end

% Check format and call sub-functions.
switch(format)
    case '.xml'
        save_xml(sys, fileName);
        
    case '.mat'
        save_mat(sys, fileName);
        
    otherwise
        error('Unsupported file extension "%s".', format);
end

end

%% Save XML file
function save_xml(sys, fileName)
% Saves the system SYS to FILENAME using the Xerces XML parser already
% implemented in matlab.
currentVersion = '1.1';
docNode = com.mathworks.xml.XMLUtils.createDocument...
  ('','satool_savefile');
docRootNode = docNode.getDocumentElement;

[numCons, numVars] = size(sys.incMatrix);
numInputs = size(sys.inputs, 2);
numMeas = size(sys.meas, 2);
numUnknowns = size(sys.unknowns,2);
[cons, vars] = sa_names(sys, 1);
[consLatex, varsLatex] = sa_names(sys, 1, 'latex');
[consAnalytic, ~] = sa_names(sys, 1, 'analytic');

numPars = size(vars,2)-numInputs-numMeas-numUnknowns;

% Save version.
versionElement = docNode.createElement('version');
versionElement.appendChild(docNode.createTextNode(currentVersion));
docRootNode.appendChild(versionElement);
% SAVE VARIABLES
% Create variables element
variablesElement = docNode.createElement('variables');
for i=1:numVars
    % Add subelement variable with properties...
    varElement = docNode.createElement('variable');
    % Name
    nameElement = docNode.createElement('name');
    nameElement.appendChild(docNode.createTextNode(vars{i}));
    varElement.appendChild(nameElement);
    % Latex expression
    latexElement = docNode.createElement('latex');
    latexElement.appendChild(docNode.createTextNode(varsLatex{i}));
    varElement.appendChild(latexElement);
    % Type
    typeElement = docNode.createElement('type');
    if (i <= numInputs)
        varType = 'input';
    elseif (i <= (numInputs + numMeas))
        varType = 'measured';
    else
        varType = 'unknown';
    end
    typeElement.appendChild(docNode.createTextNode(varType));
    varElement.appendChild(typeElement);
    % Add variable to variables element
    variablesElement.appendChild(varElement);
end
% Add variables element to root node
docRootNode.appendChild(variablesElement);


% SAVE PARAMETERS
% Create parameters element
parsElement = docNode.createElement('parameters');
for i=(size(vars,2)-numPars+1):size(vars,2)
    % Add subelement variable with properties...
    parElement = docNode.createElement('parameter');
    % Name
    nameElement = docNode.createElement('name');
    nameElement.appendChild(docNode.createTextNode(vars{i}));
    parElement.appendChild(nameElement);
    % Latex expression
    latexElement = docNode.createElement('latex');
    latexElement.appendChild(docNode.createTextNode(varsLatex{i}));
    parElement.appendChild(latexElement);
    % Type
    typeElement = docNode.createElement('type');
    typeElement.appendChild(docNode.createTextNode('parameter'));
    parElement.appendChild(typeElement);
    % Add variable to variables element
    parsElement.appendChild(parElement);
end
% Add variables element to root node
docRootNode.appendChild(parsElement);




% SAVE CONSTRAINTS

% Convert index to logical vector
canFail = zeros(1, numCons);
canFail(sys.canFail) = 1;
% Create 'constraints' element
constraintsElement = docNode.createElement('constraints');
for i=1:numCons
    % Add subelement constraint with properties...
    conElement = docNode.createElement('constraint');
    % Name
    nameElement = docNode.createElement('name');
    nameElement.appendChild(docNode.createTextNode(cons{i}));
    conElement.appendChild(nameElement);
    % Latex expression
    latexElement = docNode.createElement('latex');
    latexElement.appendChild(docNode.createTextNode(consLatex{i}));
    conElement.appendChild(latexElement);
    % Analytic expression
    latexElement = docNode.createElement('analytic');
    latexElement.appendChild(docNode.createTextNode(consAnalytic{i}));
    conElement.appendChild(latexElement);
    % Domain
    domainElement = docNode.createElement('domain');
    domainElement.appendChild(docNode.createTextNode(int2str(...
        sys.domains(i))));
    conElement.appendChild(domainElement);
    % canFail 
    failElement = docNode.createElement('canFail');
    if (canFail(i))
        canFailText = 'Yes';
    else
        canFailText = 'No';
    end
    failElement.appendChild(docNode.createTextNode(canFailText));
    conElement.appendChild(failElement);
    % Variables
    variablesElement = docNode.createElement('variables');
    for j=1:numVars
        % Check whether entry in incidence matrix is non-zero
        if (sys.incMatrix(i,j))
            % Add subelement variable with properties...
            varElement = docNode.createElement('variable');
            % Name
            nameElement = docNode.createElement('name');
            nameElement.appendChild(docNode.createTextNode(vars{j}));
            varElement.appendChild(nameElement);
            % Direction
            if (sys.incMatrix(i,j) == -1)
                varDirText = '1';
            else
                varDirText = '0';
            end
            directedElement = docNode.createElement('directed');
            directedElement.appendChild(docNode.createTextNode(...
                varDirText));
            varElement.appendChild(directedElement);
            % Add variable to variables element
            variablesElement.appendChild(varElement);
        end
    end
    % Add variables to constraint element
    conElement.appendChild(variablesElement);
    % Add constraint to constraints element
    constraintsElement.appendChild(conElement);
end
% Add 'constraints' element to root node
docRootNode.appendChild(constraintsElement);

% Save structure to XML file
xmlwrite(fileName,docNode);

end

%% Save MATLAB binary file
function save_mat(sys, fileName) %#ok sys is used for saving.
    save(fileName, '-struct', 'sys');
end