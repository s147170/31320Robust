function adj = sa_adj(sys, onlyInputs)
%SA_ADJ Retrieve adjacency matrix of a structural analysis system.
%   ADJ=SA_ADJ(SYS) returns the adjacency matrix of the structural
%   analysis system SYS. Disabled constraints and variables are not
%   included.
%
%   ADJ=SA_ADJ(SYS,ONLYINPUTS) further allows to limit the known variables
%   to inputs only so that the adjacency matrix can be used for a
%   controllability analysis.
%
%   The adjacency matrix is of the form
%   ADJ = [0      INC]
%         [INCS'  0  ]
%   where INC is the incidence matrix of the system and INCS' is the
%   transposed incidence matrix with the directed connection replaced by
%   zeros.
%
%   Example:
%       % Create random system, retrieve adjacency matrix and display it.
%       sys = sa_rand();
%       adj = sa_adj(sys);
%       disp(adj);
%
%   See also: sa_create

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 13/10-2012
%   Based on an2adj
%   Last modified - 01/12-2012

% Check system
[valid,msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Set default input parameter
if (nargin == 1)
    onlyInputs = 0;
end

% Use only incidence matrix with enabled constraints
inc = sys.incMatrix(sys.enabledCons, :);

% Remove measurements if requested
if (onlyInputs)
    % Select measurements and set them to zero.
    numInputs = size(sys.inputs, 2);
    numMeas = size(sys.meas, 2);
    inc(:, numInputs+1:numInputs+numMeas) = 0;
end

% Copy incidence matrix
incstar = inc;
% Include directed variables in first part
inc(inc<0) = 1;
% Exclude directed variables from second part
incstar(incstar<0) = 0;

[numRows,numCols] = size(inc);
% Return adjacency matrix
adj = [zeros(numRows, numRows), inc; incstar', zeros(numCols, numCols)];

end

