%Part A
%Q1

close all; clc; clear all;
warning ('off','all');
% Define the constraints

cons(1,:) = {'c1','c2','c3','c4','c5','c6','d7','d8','d9','d10','d11','d12','m13','m14','m15'}; % names to be used in the equations
cons(2,:) = {'c_1','c_2','c_3','c_4','c_5','c_6','d_7','d_8','d_9','d_10','d_11','d_12','m_13','m_14','m_15'};     % LaTeX notation  
cons(3,:) = {...
    '0 = dtheta_1 -omega_1', ...
    '0 = J_1 * domega_1 - u + b_1 * omega_1 + k_1 * (theta_1 - theta_2)+d', ...
    '0 = dtheta_2 - omega_2', ...
    '0 = J_2 * domega_2 + b_2 * omega_2 + k_1 * (theta_2 - theta_1) + k_2 (theta_2 - theta_3)', ...
    '0 = dtheta_3 - omega_3',...
    '0 = J_3 * domega_3 + b_3 * omega_3 + k_2 * (theta_3 - theta_2)'...
    '0 = dtheta_1 - D(theta_1)',...
    '0 = domega_1 - D(omega_1)',...
    '0 = dtheta_2 - D(theta_2)',...
    '0 = domega_2 - D(omega_2)',...
    '0 = dtheta_3 - D(theta_3)',...
    '0 = domega_3 - D(omega_3)',...
    '0 = y_1 - theta_1',...
    '0 = y_2 - theta_2',...
    '0 = y_3 - theta_3'
    };

% Define the input
input(1,:)    = {'u'};
input(2,:)    = {'u'};                           % LaTeX

% Define the outputs
meas(1,:)    = {'y_1', 'y_2','y_3'};
meas(2,:)    = {'y_1', 'y_2','y_3'};             % LaTeX

% Define any parameters
par(1,:)    = {'J_1','J_2','J_3','k_1','k_2','b_1','b_2','b_3'};
par(2,:)    = {'J_1','J_2','J_3','k_1','k_2','b_1','b_2','b_3'};

% Define the unknown variables
unknowns(1,:) = {'omega_1','domega_1','omega_2','domega_2','omega_3','domega_3','theta_1','dtheta_1','theta_2','dtheta_2','theta_3','dtheta_3','d'};
unknowns(2,:) = {'\\omega_1','\\dot{\\omega_1}','\\omega_2','\\dot{\\omega_2}','\\omega_3','\\dot{\\omega_3}','\\theta_1','\\dot{\\theta_1}','\\theta_2','\\dot{\\theta_2}','\\theta_3','\\dot{\\theta_3}','d'};    % LaTeX

canfail  = [1 2 3 4 5 6 13 14 15]; % constraints d7 to d12 can't fail
domains  = ones(1,15); % Number of constraints

my_ST_IncidenceMatrix = ...
    [0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0;
    1 0 0 0 1 1 0 0 0 0 1 0 1 0 0 0 1;
    0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0;
    0 0 0 0 0 0 1 1 0 0 1 0 1 0 1 0 0;
    0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 1 1 0 0 0 1 0 1 0;
    0 0 0 0 0 0 0 0 0 0 -1 1 0 0 0 0 0;
    0 0 0 0 -1 1 0 0 0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0 0 0 0 -1 1 0 0 0;
    0 0 0 0 0 0 -1 1 0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 -1 1 0;
    0 0 0 0 0 0 0 0 -1 1 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0;
    0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0;
    0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0];

% Constraints that are non-invertible (write the corresponding "known" variable)
cons_oneway = {[],[],[],[],[],[],{'theta_1'},{'omega_1'},{'theta_2'},{'omega_2'},{'theta_3'},{'omega_3'},[],[],[]};

% Compute incidence matrix
ST_IncidenceMatrix = sa_incidencematrix(cons,input,meas,unknowns,cons_oneway);

% Create the system object
Q1_sys = sa_create(ST_IncidenceMatrix,cons,input, meas,unknowns, domains,...
           canfail, par);
% Display the system
sa_disp(Q1_sys);

% Perform a matching
Q1_sys=sa_match(Q1_sys,'rank');

% Autogenerate pdf report - requires LaTeX to be installed
sa_report(Q1_sys,'Q1_sys','pdf',...
            'analytic_expressions',true);
disp('A report has been generated with the following results:')

% Display the results
disp('Obtained matching:');
sa_disp(Q1_sys, 't');

disp('Parity relation symbolic form:')
sa_disp(Q1_sys, 's');
disp('Parity relation analytic form:')
sa_disp(Q1_sys, 'a');

