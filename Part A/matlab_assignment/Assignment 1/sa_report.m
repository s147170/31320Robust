function sa_report(sys, fileName, format, varargin)
%SA_REPORT Generate a report of a structural system.
%   SA_REPORT(SYS,FILENAME) saves a structural analysis system SYS to the
%   file specified in FILENAME. The file will be overwritten if it already
%   exists. The file format will determined automatically by checking the
%   file extension. Currently the following file formats are supported:
%       'txt':
%           Saves the report in a text file with a similar structure as
%           known from the SAtool.
%
%       'pdf' (experimental):
%           Saves the report in a PDF file that is generated from a latex
%           text file. In order for the generation to work, latex must be
%           installed appropriately.
%
%   SA_REPORT(SYS,FILENAME,FORMAT,OPTIONS...) further allows to specifiy
%   the following options:
%
%       'analytic_expressions':
%           Determines whether analytic expression are include in the
%           report. Note that analytic expressions cannot obtained for all
%           kind of systems and that the analytic expressions of the
%           constraints must be present. Also note that the process of
%           obtaining analytic expressions is very slow (default: false).
%
%   Example:
%       % Generate a report and save it to a TXT file.
%       sys = sa_rand();
%       sa_report(sys, 'test.txt');
%
%   See also: sa_disp

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 10/01-2013
%   Based on xmlCreateSaveFile
%   Last modified - 03/02-2013

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Check arguments.
if nargin < 3
    % Default method is determined by file extension
    [~, ~, ext] = fileparts(fileName);
    format = lower(ext);
else
    format = ['.', lower(format)];
end

p = inputParser();
p.addOptional('analytic_expressions', false, @(x) islogical(x));
p.parse(varargin{:});

% Check format and call sub-functions.
switch(format)
    case '.txt'
        save_txt(sys, fileName, p, varargin);
        
    case '.pdf'
        save_pdf(sys, fileName, p, varargin);
        
    otherwise
        error('Unsupported file extension "%s".', format);
end

% Cleanup after compiling pdf with pdflatex - Leaves only .aux, .tex, and .pdf
    if exist([fileName,'.gz'],'file') == 2
        delete([fileName,'.gz'])
    end
%     if exist([fileName,'.aux'],'file') == 2
%         delete([fileName,'.aux'])
%     end
    if exist([fileName,'.dvi'],'file') == 2
        delete([fileName,'.dvi'])
    end    
    if exist([fileName,'.log'],'file') == 2
        delete([fileName,'.log'])
    end
    if exist([fileName,'.ps'],'file') == 2
        delete([fileName,'.ps'])
    end

end

%% Save report as a very simple TXT file.
function save_txt(sys, fileName, p, varargin)
% Define output parameters.
sep1 = '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%';
sep2 = '-------------------------------------';
dateFormat = 'dd-mmm-yyyy HH:MM:SS';
% Define function to build header string.
    function fheaderstr(f, headerName, short, tab)
        if nargin < 3
            short = false;
        end
        if nargin < 4
            tab = 0;
        end
        tab = repmat(sprintf('\t'), 1, tab);
        fprintf(f, '%s%s\n', tab, sep2);
        if (ischar(headerName))
            headerName = {headerName};
        end
        for ind = 1:length(headerName)
            fprintf(f, '%s%s\n', tab, headerName{ind});
        end
        fprintf(f, '%s%s\n', tab, sep2);
        if (~short)
            fprintf(f, '\n');
        end
    end
% Define function to build a vector string.
    function fcellstr(f, c, short, tab)
        if (nargin < 3)
            short = false;
        end
        if nargin < 4
            tab = 0;
        end
        tab = tab + 1;
        tab = repmat(sprintf('\t'), 1, tab);
        str = strjoin(c, sprintf('\n%s', tab));
        if ~isempty(str)
            fprintf(f, '%s%s\n', tab, str);
            if (~short)
                fprintf(f, '\n');
            end
        end
    end
% Get names of variables and constraints.
[cons, ~, ~, unknowns] = sa_names(sys);
% Open file and write content.
f = fopen(fileName, 'w');
% Write header.
fprintf(f, '%s\n', sep1);
fprintf(f, '%s\n', datestr(now, dateFormat));
fprintf(f, '%s\n\n', sep1);
% Write reachability
[reach, notReach] = sa_reachability(sys);
fheaderstr(f, 'Reachability');
fprintf(f, '%s\n\n', 'Reachable variables');
fcellstr(f, unknowns(reach));
fprintf(f, '%s\n\n', 'Not reachable variables');
fcellstr(f, unknowns(notReach));
% Write controlability
[control, notControl] = sa_controllability(sys);
fheaderstr(f, 'Controlability');
fprintf(f, '%s\n\n', 'Controlable variables');
fcellstr(f, unknowns(control));
fprintf(f, '%s\n\n', 'Non-controlable variables');
fcellstr(f, unknowns(notControl));
% Loop through all matchings.
if (~isempty(sys.matchings) && ~isempty(sys.parityEqs))
    analytic = {};
    if p.Results.analytic_expressions
        [sys, symbolic,~, analytic] = sa_backtrack(sys);
    else
        [sys, symbolic] = sa_backtrack(sys);
    end
    [detect, notDetect, notFail] = sa_detectability(sys);
    [isolable, notIsolable, ~] = sa_isolability(sys);
    for i = 1:size(sys.matchings, 1)
        fheaderstr(f, ['Matching ', int2str(i)]); 
        % Parity relations
        fheaderstr(f, 'Parity Relations', 0, 1);
        % Loop through all symbolic expressions.
        for j = 1:size(symbolic{i})
            if ~isempty(analytic)
                fheaderstr(f, {symbolic{i}{j}, analytic{i}{j}}, 0, 2);
            else
                fheaderstr(f, symbolic{i}{j}, 0, 2);
            end
            fcellstr(f, cons(sys.parityEqs{i}(j, :)), 0, 2);
        end
        % Write detectability.
        fheaderstr(f, 'Detectability', 0, 1);
        fprintf(f, '\t%s\n\n', 'Detectable subsystem');
        fcellstr(f, cons(detect{1}), 0, 1);
        fprintf(f, '\t%s\n\n', 'Undetectable subsystem');
        fcellstr(f, cons(notDetect{1}), 0, 1);
        fprintf(f, '\t%s\n\n', 'Constraints that cannot fail');
        fcellstr(f, cons(notFail), 0, 1);
        % Write isolability.
        fheaderstr(f, 'Isolability', 0, 1);
        fprintf(f, '\t%s\n\n', 'Isolable subsystem');
        fcellstr(f, cons(isolable{1}), 0, 1);
        fprintf(f, '\t%s\n\n', 'Non-isolable subsystem');
        fcellstr(f, cons(notIsolable{1}), 0, 1);
    end
    % Print isolability over all
    if size(sys.matchings, 1) > 1
        [detect, notDetect, notFail] = sa_detectability(sys, [], 1);
        [isolable, notIsolable, ~] = sa_isolability(sys, [], 1);
        % Write detectability over all.
        fheaderstr(f, 'Detectability over all');
        fprintf(f, '\t%s\n\n', 'Detectable subsystem');
        fcellstr(f, cons(detect));
        fprintf(f, '\t%s\n\n', 'Undetectable subsystem');
        fcellstr(f, cons(notDetect));
        fprintf(f, '\t%s\n\n', 'Constraints that cannot fail');
        fcellstr(f, cons(notFail));
        % Write isolability over all
        fheaderstr(f, 'Isolability over all matchings');
        fprintf(f, '\t%s\n\n', 'Isolable subsystem');
        fcellstr(f, cons(isolable));
        fprintf(f, '\t%s\n\n', 'Non-isolable subsystem');
        fcellstr(f, cons(notIsolable));
    end
end
% Close file.
fclose(f);
end

%% Save report as a PDF file.
function status = save_pdf(sys, fileName, p, varargin)
%% generateReport(data,content,papersize)
%
%
% This function generates a report from a satool analysis.
%
% The generator can produce the following content:
% - Status (Detectability/Isolability)
% - Incidence Matrix
% - Dependency Matrix
% - Parity Relations
% - Dulmage-Mendelsohn Decomposition plot
%
%
% The input arguments to the function are:
% Required:
% - data        : A cell array containing information about 
%	              the system and structural analysis results.
%	              The structure of the cell aray is defined by
%	              the satool specifications.
%
% Optional:
% - content     : 
%
%
% - papertype   : String defining the size of the paper.
%                 Can be either a1, a2, a3, a4 (default) or a5.
%
% - orientation : String defining the orientation of the paper.
%                 Can be either portrait (default) or landscape.
%
% - latexPkg    : Cell array of strings defining additional 
%                 latex packages needed. 
%
% The outputs from the function are:
% - status      : An interger indicating succes/faillure:
%                 - 0  : Everything was acomplished
%                 ....
%                 - 1  : Nothing was acomplished


%% Initialize status to 0 (will be change if failure occurs during execution)
status = 0;

%% Extract arguments
% Define input parser
p1 = inputParser;   % Create an instance of the inputParser class.
p1.addOptional('content', {'all'}, @iscell);
p1.addOptional('papertype', 'a4', ...
    @(x)any(strcmpi(x,{'a1','a2','a3','a4','a5'})));
p1.addOptional('orientation', 'portrait', ....
    @(x)any(strcmpi(x,{'portrait','landscape'})));
p1.addOptional('latexPkg', {}, @iscell);

% Parse arguments
p1.parse(varargin{:})

%content = p1.Results.content;
content = {'all'};
papertype = p1.Results.papertype;
orientation = p1.Results.orientation;
latexPkg = p1.Results.latexPkg;

%% Setup paper (all in mm!)
% a4
if strcmpi(papertype,'a4') 
    paper.type = 'a4';
    paper.width = 210;
    paper.height = 297;
    paper.oddsidemargin = -13;
    paper.evensidemargin = -13;
    paper.innermargin = 20; 
    paper.textwidth = paper.width - (paper.oddsidemargin + 25.4 + paper.innermargin);
    paper.textheight = paper.height - 50;
else
    warning('Unknown type of paper!')
    status = -1;
    return;
end

%% Determine content of report

% Incidence Matrix
includeMatrix = ...
	(any(strcmpi(content,'all')) && ~any(strcmpi(content,'-incidence'))) ...
        || any(strcmpi(content,'incidence'));

% Status (Detectability/Isolability)
includeStatus = ...
    (any(strcmpi(content,'all')) && ~any(strcmpi(content,'-status'))) ...
        || any(strcmpi(content,'status'));

% Parity Relations
includeParity = ...
	(any(strcmpi(content,'all')) && ~any(strcmpi(content,'-parity'))) ...
        || any(strcmpi(content,'parity'));

% Dulmage-Mendelsohn Decomposition plot
includeDulmage = ...
	(any(strcmp(content,'all')) && ~any(strcmp(content,'-dulmage'))) ...
        || any(strcmp(content,'dulmage'));

[cons, inputs, meas, unknowns] = sa_names(sys, 0, {'latex', 'name'});
[numCons, numVars] = size(sys.incMatrix);
    
%% Produce report

% Document Header
str = ['\\documentclass[', paper.type,',11pt]{article}\n'];
str = [str,'\\usepackage{rotating}\n'];
str = [str,'\\usepackage{graphicx,color,psfrag}\n'];
str = [str,'\\usepackage{longtable}\n']; 
str = [str,'\\usepackage{multirow}\n'];
str = [str,'\\usepackage{amsmath}\n'];
str = [str,'\\usepackage{breqn}\n']; % added breqn package for line wrapping of equation
for n = 1:1:size(latexPkg,2)
    str = [str,'\\usepackage{',latexPkg{n},'}\n'];
end
str = [str,'\\oddsidemargin ', num2str(paper.oddsidemargin),'mm\n'];
str = [str,'\\evensidemargin ', num2str(paper.evensidemargin),'mm\n'];
str = [str,'\\textwidth ', num2str(paper.textwidth),'mm\n'];
str = [str,'\\title{Structural Analysis Toolbox Report}\n'];
str = [str,'\\author{This document is autogenerated.}\n'];
str = [str,'\\begin{document}\n'];
str = [str,'\\maketitle\n'];

% Print overview
% Changed to individual math environments to allow line breaking in math.
str = [str, '\\section{Overview}\n'];
str = [str, 'The investigated system has\n\\begin{itemize}\n'];
str = [str, '\t\\item ', int2str(size(cons, 2)), ' constraints $', ...
    '\\mathcal{C} = [$ $', strjoin(cons, '$, $'), '$ $]$,\n'];
str = [str, '\t\\item ', int2str(size([inputs, meas], 2)), ' known variables $', ...
    '\\mathcal{K} = [$ $', strjoin([inputs, meas], '$, $'), '$ $]$ and\n'];
str = [str, '\t\\item ', int2str(size(unknowns, 2)), ' unknown variables $', ...
    '\\mathcal{X} = [$ $', strjoin(unknowns, '$, $'), '$ $]$.\n'];
str = [str, '\\end{itemize}\n'];
[analytic, ~] = sa_names(sys, 1, 'analytic');
if (~isempty(strjoin(analytic, '')))
    str = [str, 'The constraints of the system are as follows:\n'];
    str = [str, '\\begin{longtable}{ l | p{0.8\\textwidth} }\n'];
    str_cons_list = '';
    for i = 1:numCons      
        str_cons_list = [str_cons_list, '\t', '$', cons{i}, '$ & $ ', ...
                         strrep(latex(sym(analytic{i})),'\','\\'), ' $ \\\\\n'];
    end
    str_cons_list =regexprep(str_cons_list,'\\\\mathrm{(\w+)}','$1');
    % See comments for the parity relations. Some small changes in addition
    % to these
%     str_cons_list = regexprep(str_cons_list,'s(\^\d|)\\\\\, (\S+)','\\\\frac{d$1}{dt$1}$2'); % replace premultiplied s with d/dt
%     str_cons_list = regexprep(str_cons_list,'(\S+)\\\\\, s(\^\d|)','\\\\frac{d$2}{dt$2}$1'); % replace postmultiplied s with d/dt  in front
                
    str_cons_list = regexprep(str_cons_list,'(\w+)(''{2,})','\\\\frac{d^${int2str(length($2))}}{dt^${int2str(length($2))}}$1'); % replace multiple ' with d^n/dt^n  in front
    str_cons_list = regexprep(str_cons_list,'(\w+)''','\\\\frac{d}{dt}$1'); % replace single ' with d/dt  in front
    
    for j = 1:size(sys.inputs,2)
%         rep_str = ['(?<![a-z0-9])',['\\\\mathrm{',sys.inputs{1,j},'}'],'(?![a-z0-9])'];
        rep_str = ['(?<![a-z0-9])',sys.inputs{1,j},'(?![a-z0-9])'];
        latex_input = strrep(sys.inputs{2,j},'\','\\');
        str_cons_list =regexprep(str_cons_list,rep_str,latex_input);
    end 
    for j = 1:size(sys.meas,2)
%         rep_str = ['(?<![a-z0-9])',['\\\\mathrm{',sys.meas{1,j},'}'],'(?![a-z0-9])'];
        rep_str = ['(?<![a-z0-9])',sys.meas{1,j},'(?![a-z0-9])'];
        latex_meas = strrep(sys.meas{2,j},'\','\\');
        str_cons_list =regexprep(str_cons_list,rep_str,latex_meas);
    end 
    for j = 1:size(sys.unknowns,2)
%         rep_str = ['(?<![a-z0-9])',['\\\\mathrm{',sys.unknowns{1,j},'}'],'(?![a-z0-9])'];
        rep_str = ['(?<![a-z0-9])',sys.unknowns{1,j},'(?![a-z0-9])'];
        latex_unknowns = strrep(sys.unknowns{2,j},'\','\\');
        sprintf('%s is replaced with %s',sys.unknowns{1,j},sys.unknowns{2,j});
        str_cons_list =regexprep(str_cons_list,rep_str,latex_unknowns);
    end
    for j = 1:size(sys.parameters,2)
%         rep_str = ['(?<![a-z0-9])',['\\\\mathrm{',sys.parameters{1,j},'}'],'(?![a-z0-9])'];
        rep_str = ['(?<![a-z0-9])',sys.parameters{1,j},'(?![a-z0-9])'];
        latex_parameters = strrep(sys.parameters{2,j},'\','\\');
        str_cons_list =regexprep(str_cons_list,rep_str,latex_parameters);
    end   
    
    str_cons_list = [str_cons_list, '\\end{longtable}\n'];
    str = [str,str_cons_list];
    numMatchings = size(sys.matchings, 1);
    if numMatchings > 0 && isempty(sys.parityEqs)
        sys = sa_backtrack(sys);
    end
    numParityEqs = 0;
    for i = 1:numMatchings
        numParityEqs = numParityEqs + size(sys.parityEqs{i}, 1);
    end
    str = [str, 'The analysis obtained ', int2str(numMatchings), ...
        ' matchings that yield in total ', int2str(numParityEqs), ...
        ' parity equations.'];
    str = [str, '\\newpage\n'];
end

if (includeDulmage)
    % Write decomposition
    % Changed to individual math environments $ $ to enable wrapping
    [uc,uu,jc,ju,oc,ou] = sa_decomp(sys);
    str = [str, '\\section{Canonical Decomposition}'];
    str = [str, 'The system consists of\n\\begin{itemize}'];
    str = [str, '\t\\item the over-determined subsystem '];
    str = [str,'$\\mathcal{S}^+$ with $\\mathcal{C}^+ = [$ $', ...
        strjoin(cons(sort(oc)), '$, $'), ' $ $]$ ', 'and $\\mathcal{X}^+ = [$ $', ...
        strjoin(unknowns(sort(ou)), '$, $'), ' $ $]$,\n'];
    str = [str, '\t\\item the just-determined subsystem '];
    str = [str,'$\\mathcal{S}^0$ with $\\mathcal{C}^0 = [$ $', ...
        strjoin(cons(sort(jc)), '$, $'), ' $ $]$ ', 'and $\\mathcal{X}^+ = [$ $', ...
        strjoin(unknowns(sort(ju)), '$, $'), ' $ $]$ and\n'];
    str = [str, '\t\\item the under-determined subsystem '];
    str = [str,'$\\mathcal{S}^-$ with $\\mathcal{C}^- = [$ $', ...
        strjoin(cons(sort(uc)), '$, $'), ' $ $]$ ', 'and $\\mathcal{X}^+ = [$ $', ...
        strjoin(unknowns(sort(uu)), '$, $'), ' $ $]$.\n'];
    str = [str, '\\end{itemize}\n'];
end

% Incidence Matrix
if includeMatrix
    str = [str, '\\section{Incidence Matrix}\n'];
    str = [str, 'Table \\ref{tab:matrix} presents the incidence matrix', ...
        ' of the investigated system.'];
    str = [str, incidenceMatrix2latex(sys, paper)];
    str = [str, '\\newpage\n'];
end

if ~isempty(sys.matchings)
    str = [str, '\\section{Matchings}\n'];
    str = [str, 'Table \\ref{tab:matchings} lists the obtained matchings. ', ...
        'The fields either contain the matched unknown variables, ' ...
        'zeros to indicate an unmatched constraints or nothing if ', ...
        'constraints are not used in a matching.'];        
    [numRows, numCols] = size(sys.matchings);
    % Build cell matrix.
    cellMatrix = cell(numRows, numCols);
    for i = 1:numRows
        for j = 1:numCols
            m = sys.matchings(i, j); 
            if m > 0
                cellMatrix{i, j} = ['$', unknowns{m}, '$'];
            elseif m == 0
                cellMatrix{i, j} = '0';
            else
                cellMatrix{i, j} = '';
            end
        end
    end
    % Build other things.
    colNames = cons;
    for i = 1:length(colNames)
        colNames{i} = ['\\textbf{$', colNames{i}, '$}'];
    end
    rowNames = cellstr(num2str((1:numRows)', '%-1.0f'));
    for i = 1:length(rowNames)
        rowNames{i} = ['\\textbf{', rowNames{i}, '}'];
    end
    caption = 'Matchings of the investigated system.';
    layout = ''; % TODO
    label = 'tab:matchings';
    mat = matrix2latex(cellMatrix, rowNames, colNames, layout, caption, ...
        label,paper);
    str = [str, mat, '\n'];

    if (includeParity || includeStatus) && isempty(sys.parityEqs)
        sys = sa_backtrack(sys); % Obtain analytical parity equations as matlab symbolic equations
    end    
    
    if includeParity &&  p.Results.analytic_expressions
        % Added this section to typeset and include the parity equation in latex format
        [~, ~, ~,~,analytic_sym] = sa_backtrack(sys);
        str = [str, '\\section{Parity Equations}\n'];
        str = [str,'\\begin{dgroup*}', '\n']; % uses breqn to enable equation line wrapping
        %for k=1:size(analytic_sym,2) % For every matching
        % pnha 9/4 - 2015: changed size check 
        for k=1:size(analytic_sym,1) % For every matching
            for i=1:length(analytic_sym{1}) % For every equation
                sym_parity = latex(analytic_sym{k}{i}); % Use built in matlab function latex to convert symbolic expression to latex
                sym_parity =regexprep(sym_parity,'\\mathrm{(\w+)}','$1');
                
%                 sym_parity = regexprep(sym_parity,'([^\\])\,','$1, ');
%                 sym_parity = regexprep(sym_parity,'s(\^\d|)\\\, (\S+)','\\frac{d$1}{dt$1}$2'); % replace premultiplied s with d/dt
%                 sym_parity = regexprep(sym_parity,'(\S+)\\\, s(\^\d|)','\\frac{d$2}{dt$2}$1'); % replace postmultiplied s with d/dt  in front
                
                sym_parity = regexprep(sym_parity,'(\w+)(''{2,})','\\frac{d^${int2str(length($2))}}{dt^${int2str(length($2))}}$1'); % replace multiple ' with d^n/dt^n  in front
                sym_parity = regexprep(sym_parity,'(\w+)''','\\frac{d}{dt}$1'); % replace single ' with d/dt  in front

                for j = 1:size(sys.inputs,2) % replaces the input names with their corresponding latex expressions, also removes  \mathrm (from latex())
                    rep_str = ['(?<![a-z0-9])',sys.inputs{1,j},'(?![a-z0-9])'];
                    sym_parity =regexprep(sym_parity,rep_str,sys.inputs{2,j});
                end 
                for j = 1:size(sys.meas,2) % replaces the measurements names with their corresponding latex expressions, also removes  \mathrm (from latex())
                    rep_str = ['(?<![a-z0-9])',sys.meas{1,j},'(?![a-z0-9])'];
                    sym_parity =regexprep(sym_parity,rep_str,sys.meas{2,j});
                end 
                for j = 1:size(sys.unknowns,2) % replaces the unknowns names with their corresponding latex expressions, also removes  \mathrm (from latex())
                    rep_str = ['(?<![a-z0-9])',sys.unknowns{1,j},'(?![a-z0-9])'];
                    sym_parity =regexprep(sym_parity,rep_str,sys.unknowns{2,j});
                end
                for j = 1:size(sys.parameters,2)
                    rep_str = ['(?<![a-z0-9])',sys.parameters{1,j},'(?![a-z0-9])'];
                    sym_parity =regexprep(sym_parity,rep_str,sys.parameters{2,j});
                end  
                sym_parity =regexprep(sym_parity,',',',\\allowbreak '); % add \allowbreak to enable wrapping at commas
                sym_parity = strrep(sym_parity,'\','\\'); %escape charater \
                %sym_parity = strrep(sym_parity,'=','&=');
                str = [str,'\\begin{dmath*}', '\n'];
                str = [str,sym_parity, '\n'];
                str = [str,'\\end{dmath*}', '\n'];
            end
        end
        str = [str,'\\end{dgroup*}', '\n'];
        clear i;
        
       % str = [str, '\\newpage\n'];
        
    end
    
    if includeStatus
        str = [str, '\\section{Detectability and isolability analysis}\n'];
        str = [str, 'Table \\ref{tab:iso} lists the detectability and isolability', ...
            ' properties of the parity equations separately and over all', ...
            ' combined. Detectable (\\textbf{$d$}), isolable (\\textbf{$i$})', ...
            ' and non-failable constraints (\\textbf{$n$}) are marked', ...
            ' accordingly.'];
        [detectable, ~, notFail] = sa_detectability(sys);
        [detectableAll, ~, ~] = sa_detectability(sys, [], 1);
        [isolable, ~, ~] = sa_isolability(sys);
        [isolableAll, ~, ~] = sa_isolability(sys, [], 1);
        % Append over all items
        detectable = [detectable; {detectableAll}];
        isolable = [isolable; {isolableAll}];
        % Fill cell matrix.
        cellMatrix = cell(size(detectable, 1), numCols);
        [cellMatrix{:, :}] = deal('');
        [cellMatrix{:, notFail}] = deal('$n$');
        for i = 1:size(detectable, 1)
            [cellMatrix{i, detectable{i}}] = deal('$d$');
            [cellMatrix{i, isolable{i}}] = deal('$i$');
        end
        % Add last element to row headers.
        rowNames = [rowNames; {'\\textbf{ALL}'}];
        % Produce matrix.
        caption = 'Detectability and isolability of the investigated system.';
        layout = ''; % TODO
        label = 'tab:iso';
        mat = matrix2latex(cellMatrix, rowNames, colNames, layout, ...
            caption, label,paper);
        str = [str, mat, '\n'];
    end
        
end

% Footer
str = [str, '\n\\end{document}'];

%% Save latex file and produce DVI, PS and PDF reports
[pathstr, name, ~] = fileparts(fileName);
latexFile = [pathstr, name, '.tex'];
dviFile = [pathstr, name, '.dvi'];
psFile = [pathstr, name, '.ps'];
fid = fopen(latexFile, 'w');
fprintf(fid,str);
fclose(fid);

system(['pdflatex ', latexFile]);
% Old use of latex
%system(['latex ', latexFile]);
% system(sprintf('dvips %s -o -t%s -t%s -Ppdf -G0',...
%     dviFile, paper.type, orientation)); 
% system(['ps2pdf ', psFile]);
if exist(fileName, 'file') == 2
    system(fileName);
end
end

%% Additional functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% - %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function statusMatrix = statusMatrix2Latex(sys)

ncmax = 13; % Maximum number of columns
ncons = size(sys.incMatrix, 1); % Number of constraints


% Extract detectability/isolability properties
for n=1:ncons
    if ~isempty(residuals)
        if KMT(n)==-1
            detectisolate(n) = '-';  % can't fail
        elseif KMT(n)==0
            detectisolate(n) = '0';  % undetectable
        elseif KMT(n)==1
            detectisolate(n) = 'i';  % isolable
        else
            detectisolate(n) = 'd';  % detectable
        end
    else
        if constraint_canfail{n} == 1
            detectisolate(n) = '0';
        else
            detectisolate(n) = '-';
        end
    end
end

% get number of collumns
if ncons > ncmax
    nc = ncmax;
else
    nc = ncons;
end
    
% write table header
statusMatrix = '\n\\begin{table}[!h]\n\\centering';
statusMatrix = [statusMatrix, '\n\\begin{tabular}{'];
for i=0:nc
    if (i==0)
        statusMatrix = [statusMatrix, 'l|'];
    else
        statusMatrix = [statusMatrix, 'r'];
    end
end
statusMatrix = [statusMatrix, '}\n']; 

% write horizontal variables
n = 1;
while n< ncons-1
    statusMatrx = [statusMatrix, '\\\\'];
    if size(constraint_names(n+1:end),1) < nc
        nc = size(constraint_names(n:end),1);
    end
    statusMatrix = [statusMatrix, 'Constraint'];
    for i=0:nc-1
        statusMatrix = [statusMatrix, '& \\begin{sideways}$',constraint_names{n+i} , '$\\end{sideways}'];
    end
  
    statusMatrix = [statusMatrix, ' \\\\ \n\\hline\n'];


    statusMatrix = [statusMatrix,'Status']; % print row name
    if (strcmpi(class(detectisolate),'char'))
        for i=0:nc-1 % print char matrix
            statusMatrix = [statusMatrix, ' & ',detectisolate(n+i)];
        end
    else
        for i=0:nc-1 % print integer matrix
            statusMatrix = [statusMatrix, ' & ',int2str(detectisolate(n+i))];
        end
    end
    if nc<ncmax % no \\ on last row
        statusMatrix = [statusMatrix, ' \n'];
    else
        statusMatrix = [statusMatrix, ' \\\\ \\\\ \n'];
    end
    n = n+i+1;
end


statusMatrix = [statusMatrix, '\\end{tabular}\n\\caption{Summary of the',...
    'complete d/i properties of the investigated system.'...
    '(- : can''t fail, 0 : undetectable, i : isolable, d : detectable)}\n\\end{table}\n'];
end

%% - %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [incidenceMatrix]=incidenceMatrix2latex(sys, paper)
% incidenceMatrix2latex(data,graphdata)
% 
% This function generates a report from data and graphdata. 
% INPUT:
%  graphdata - Internal data representation of bipartite digraph 
%              from incidence matrix representation
%  data	     - Internal data representation of the system 
% OUTPUT:
%  incidenceMatrix - String containing the latex code to generate the
%  incidence matrx
% The function uses the following functions:
%  None


%% Extract information

[constraint_names, variable_names] = sa_names(sys, 1, {'latex', 'name'});
% constraint_names = strrep(constraint_names,'\','\\');
% variable_names = strrep(variable_names,'\','\\');

% get number of variables
[ncon, nvar] = size(sys.incMatrix);
nvarknown = nvar - size(sys.unknowns, 2);

cellMatrix = cell(ncon, nvar);

for n = 1:nvar
    for j = 1:ncon
        switch sys.incMatrix(j,n)
            case 1
                cellMatrix{j, n} = '1';
                
            case -1
                cellMatrix{j, n} = 'X';
                
            otherwise
                cellMatrix{j, n} = '0';
        end
    end
end

% Move constraints that can't fail to bottom of table
canFail = sys.canFail;
nconcanfail = length(canFail);
notCanFail = setdiff(1:ncon, canFail);
cellMatrix = [cellMatrix(canFail, :); cellMatrix(notCanFail, :)];
constraint_names = [constraint_names(canFail), ...
    constraint_names(notCanFail)];

%% Create LaTex tabel

col = nvar;
row = ncon;

% Choose Layout and setup layout chooser
textsizes = {'\\tiny','\\scriptsize','\\footnotesize','\\small','\\normalsize'};
%colwidth = {1.5,2.1,2.4,2.7,7}; changed to line below
colwidth = {5.5,6,6.25,6.75,7}; % More realistic values
layoutOK = 'Not checked';
layout = 'normal';
tabletextsize = 5; % Begin width normalsized text

% Calculate initial maximum values for specified paper
maxcol = floor(paper.textwidth/colwidth{tabletextsize})-1;
maxrow = floor(paper.textheight/colwidth{tabletextsize})-1;

% Iteration towards optimal layout
while  ~strcmp('Yes', layoutOK) && tabletextsize > 1; % changed from tabletextsize > 0 to avoid segmentation faults
    
    if col >= maxcol || row >= maxrow % Added || row >= maxrow to avoid infinite loop if there is no room for the sideways table
        tmp = col;
        col = row;
        row = tmp;
        if strcmp ('sideways',layout) % Added so that layout string always corresponds to row, col.
            layout = 'normal';
        else
            layout = 'sideways';
        end
        
        if row <= maxrow && col <= maxcol
            layoutOK = 'Yes';
        else
            tabletextsize = tabletextsize - 1;
            maxcol = floor(paper.textwidth/colwidth{tabletextsize})-1;
            maxrow = floor(paper.textheight/colwidth{tabletextsize})-1;
        end
    end
    if tabletextsize > 0 && row <= maxrow && col <= maxcol
        layoutOK = 'Yes';
    else
        layoutOK = 'Could not fit';
    end
end
% Produce the incidence matrix even if it does not fit on page
if tabletextsize == 0
    tabletextsize = 1;
end

% write table header
incidenceMatrix = '\\setlength\\tabcolsep{2mm}\n';
if strcmp('sideways', layout);
    incidenceMatrix = [incidenceMatrix,'\n\\begin{sidewaystable}\n\\centering\n',textsizes{tabletextsize}, '\n\\begin{tabular}{'];
elseif strcmp('Could not fit', layoutOK);
    incidenceMatrix = [incidenceMatrix,'\n\\begin{table}[!htb]\n\\centering\n','\\resizebox{\\textwidth}{!}{', '\n\\begin{tabular}{']; % replaced textsizes{tabletextsize} with resizebox
else
    incidenceMatrix = [incidenceMatrix,'\n\\begin{table}[!htb]\n\\centering\n',textsizes{tabletextsize}, '\n\\begin{tabular}{'];
end
for i=0:nvar
    if (i==0)
%         incidenceMatrix = [incidenceMatrix, '|l|l|'];
        incidenceMatrix = [incidenceMatrix, '|l|'];
    else
        incidenceMatrix = [incidenceMatrix, 'c'];
    end
    if i==nvarknown
        incidenceMatrix = [incidenceMatrix, '|'];
    end
end
incidenceMatrix = [incidenceMatrix, '|}\n\\hline\n']; 

% Distinguish between known and unknown variables
% incidenceMatrix = [incidenceMatrix, '&&', '\\multicolumn{', int2str(nvarknown), '}{c|}{$\\mathcal{K}$} & \\multicolumn{', int2str(nvar-nvarknown)];
incidenceMatrix = [incidenceMatrix, '&', '\\multicolumn{', int2str(nvarknown), '}{c|}{$\\mathcal{K}$} & \\multicolumn{', int2str(nvar-nvarknown)];
% incidenceMatrix = [incidenceMatrix, '}{|c|}{$\\mathcal{X}$}\\\\\n\\cline{3-', int2str(nvar+2),'}\n'];
incidenceMatrix = [incidenceMatrix, '}{|c|}{$\\mathcal{X}$}\\\\\n\\cline{2-', int2str(nvar+1),'}\n'];
% write horizontal variables
for j=1:ncon
    if (j==1) % first row has names of columns
%         incidenceMatrix = [incidenceMatrix, '&\\# '];
        incidenceMatrix = [incidenceMatrix, '\\# '];
        for i=1:nvar
            incidenceMatrix = [incidenceMatrix, '& \\begin{sideways}$',variable_names{i}, '$\\end{sideways}'];
        end
        incidenceMatrix = [incidenceMatrix, ' \\\\ \n\\hline\n'];
    end
    
    if (j==1)
        incidenceMatrix = [incidenceMatrix, ''];%\\multirow{', int2str(nconcanfail), '}{*}{$\\mathcal{C}_{can fail}$}']; % cannot use _\text with breqn
    end
    if (j==nconcanfail+1)
        incidenceMatrix = [incidenceMatrix, '\\hline\n'];%\\multirow{', int2str(ncon-nconcanfail), '}{*}{$\\mathcal{C}_{cannot fail}$}']; % cannot use _\text with breqn
    end
    
%     incidenceMatrix = [incidenceMatrix,'&$', constraint_names{j}, '$ ']; % print row name
    incidenceMatrix = [incidenceMatrix,'$', constraint_names{j}, '$ ']; % print row name

    for i = 1:nvar % print integer matrix
        incidenceMatrix = [incidenceMatrix, ' & ', cellMatrix{j,i}];
    end
    
    if (j~=ncon) % no \\ on last row
        incidenceMatrix = [incidenceMatrix, ' \\\\ \n'];
    else
        incidenceMatrix = [incidenceMatrix, ' \\\\ \\hline\n'];
    end
end
if strcmp('Could not fit',layoutOK)
    incidenceMatrix = [incidenceMatrix, '\\end{tabular}}\n\\caption{Incidence matrix of the investigated system.}\n'];
else
    incidenceMatrix = [incidenceMatrix, '\\end{tabular}\n\\caption{Incidence matrix of the investigated system.}\n'];   
end
incidenceMatrix = [incidenceMatrix, '\\label{tab:matrix}\n'];
if strcmp('sideways', layout);
    incidenceMatrix = [incidenceMatrix, '\\end{sidewaystable}'];
else
    incidenceMatrix = [incidenceMatrix, '\\end{table}\n'];
end
end

%% - %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [matrix] = matrix2latex(cellMatrix, rowNames, colNames, ...
    layout, caption, label,paper) % Added paper struct to be able to determine tables sizes
% incidenceMatrix2latex(data,graphdata)
% 
% This function generates a report from data and graphdata. 
% INPUT:
%  graphdata - Internal data representation of bipartite digraph 
%              from incidence matrix representation
%  data	     - Internal data representation of the system 
% OUTPUT:
%  incidenceMatrix - String containing the latex code to generate the
%  incidence matrx
% The function uses the following functions:
%  None

% Create LaTex tabel

[numRows, numCols] = size(cellMatrix);

col = numCols;
row = numRows;
% 
% % Choose Layout and setup layout chooser
textsizes = {'\\tiny','\\scriptsize','\\footnotesize','\\small','\\normalsize'};
% colwidth = {1.5,2.1,2.4,2.7,7};
colwidth = {7,7.5,7.75,8.25,8.5}; % Changed to more realistic values
layoutOK = 'Not checked';
layout = 'normal';
tabletextsize = 5; % Begin width normalsized text
% 
% Calculate initial maximum values for specified paper
maxcol = floor(paper.textwidth/colwidth{tabletextsize})-1;
maxrow = floor(paper.textheight/colwidth{tabletextsize})-1;

% Iteration towards optimal layout
while  ~strcmp('Yes', layoutOK) && tabletextsize > 1;
    
    if col >= maxcol || row >= maxrow % Added || row >= maxrow to avoid infinite loop if there is no room for the sideways table
        tmp = col;
        col = row;
        row = tmp;
        if strcmp ('sideways',layout) % Added so that layout string always corresponds to row, col.
            layout = 'normal';
        else
            layout = 'sideways';
        end
        
        if row <= maxrow && col <= maxcol
            layoutOK = 'Yes';
        else
            tabletextsize = tabletextsize - 1;
            maxcol = floor(paper.textwidth/colwidth{tabletextsize})-1;
            maxrow = floor(paper.textheight/colwidth{tabletextsize})-1;
        end
    end
    if tabletextsize > 0 && row <= maxrow && col <= maxcol
        layoutOK = 'Yes';
    else
        layoutOK = 'Could not fit';
    end
end

% % Produce the incidence matrix even if it does not fit on page
if tabletextsize == 0
    tabletextsize = 1;
end

% Write table header
matrix = '\\setlength\\tabcolsep{2mm}\n';
if strcmp('sideways', layout);
    matrix = [matrix, '\n\\begin{sidewaystable}[!htb]\n\\centering\n', ...
        textsizes{tabletextsize}, '\n\\begin{tabular}']; 
elseif strcmp('Could not fit',layoutOK); % replace textsizes{tabletextsize} with a resizebox when the table does not fit
    matrix = [matrix, '\n\\begin{table}[!htb]\n\\centering\n', ...
        '\\resizebox{\\textwidth}{!}{', '\n\\begin{tabular}'];
else
    matrix = [matrix, '\n\\begin{table}[!htb]\n\\centering\n', ...
        textsizes{tabletextsize}, '\n\\begin{tabular}']; 
end
% Produce columns.
matrix = [matrix, '{|', ...
    strjoin(repmat({'c'}, 1, numCols + ~isempty(rowNames)), '|'), ...
    '|}\n\\hline\n']; 

% Write header column names.
if ~isempty(rowNames)
    if length(colNames) == numCols && ~isempty(rowNames)
        colNames = [{'~'}, colNames];
    end
    matrix = [matrix, strjoin(colNames, ' & '), '\\\\ \\hline \n'];
end

% Write data.
for i = 1:numRows
    if isempty(rowNames)
        matrix = [matrix, strjoin(cellMatrix(i, :), ' & '), ...
            '\\\\ \\hline \n']
    else
        matrix = [matrix, ...
            strjoin([rowNames(i), cellMatrix(i, :)], ' & '), ...
            '\\\\ \\hline \n'];
    end
end

% Add caption and end table.
if strcmp('Could not fit',layoutOK);
    matrix = [matrix, '\\end{tabular}}\n\\caption{', caption, '}\n']; % End of resizebox
else
    matrix = [matrix, '\\end{tabular}\n\\caption{', caption, '}\n'];
end
matrix = [matrix, '\\label{', label, '}\n'];
if strcmp('sideways', layout);
    matrix = [matrix, '\\end{sidewaystable}'];
else
    matrix = [matrix, '\\end{table}\n'];
end
end
