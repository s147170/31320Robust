function [sys,symbolic,analytic,vargout] = sa_match(sys, varargin)
%SA_MATCH Perform matching on a structural analysis system.
%   SYS=SA_MATCH(SYS) performs a matching of the unknown variables to the
%   constraints of the system SYS using the default matching algorithm. It
%   returns a new copy of the original system structure in SYS holding the
%   obtained matchings and a binary parity equation matrix that indicates
%   which constraints are used in which parity equations.
%
%   The matchings are returned in the two-dimensional matrix SYS.matchings
%   where each field xN = (i,j) is the unknown variable that is matched to
%   the j-th constraint of the i-th matching. If xN is zero, then no
%   variable is matched to the j-th constraint and the constraint can be
%   used to obtain a parity equation. If xN is minus one (-1), it indicates
%   that the j-th constraint was not used to obtain the i-th matching and
%   must not be used to find parity equations.
%   Note that xN indicates the index of the unknown variable starting with
%   one. To get the column index within the incidence matrix the number of
%   inputs and measurements has to be added to xN.
%   The matrix has as many rows as matchings were found and as many columns
%   as there are constraints in the system.
%
%   [SYS,SYMBOLIC]=SA_MATCH(SYS) further returns the symbolic expressions
%   obtained from the backtracking algorithm. See sa_backtrack() for more
%   information.
%
%   [SYS,SYMBOLIC,ANALYSIS]=SA_MATCH(SYS) further returns the analytic
%   expressions obtained from the backtracking algorithm. See
%   sa_backtrack() for more information.
%
%   [SYS,...]=SA_MATCH(SYS,OPTIONS...) further allows to specify several
%   further options. At the moment the following options are supported.
%
%   'algorithm':
%       'rank' (default algorithm):
%           Ranking algorithm to obtain one complete ranking from [2].
%
%       'mso':
%           Efficient algorithm for finding all matchings based on minimal
%           overdetermined subsystems as described in [1].
%
%       'full':
%           Algorithm to find all complete matchings on all unknown
%           variables as described in [3].
%
%       'custom':
%           A custom anonymous function is called. The function is
%           specified in the field 'function_handle'.
%  
%           The custom function takes a structure of the system to analyze
%           and must return a system structure with the possible matchings
%           or an empty set. The backtracking algorithm is then used to
%           obtain the parity equations. Further output variables can be
%           returned if necessary. The custom function has to have the 
%           following structure
%               [SYS, VARARGOUT] = CUSTOM_FUNCTION(SYS, varargin).
%           where SYS is the structure of the system and VARARGIN the
%           arguments supplied to this method.
%
%   'backtrack': 
%           Specify whether the matchings are to be backtracked
%           (default: true).
%
%   'ignore_causalities':
%           Specify whether invalid matchings are to kept
%           (default: false).
%
%   'function_handle:':
%           Specify the function handle of an anonymous function to call
%           instead of internal sub-algorithms to perform the matching. The
%           parameter is only used if 'algorithm' is 'custom'.
%
%   Example:
%       sys = sa_rand();
%       sys = sa_match(sys);
%       sa_disp(sys, 't');
%
%   [1] M. Krysander, J. �slund, and M Nyberg. An Efficient Algorithm for Finding
%       Minimal Overconstrained Subsystems for Model-Based Diagnosis.
%       IEEE TRANSACTIONS ON SYSTEMS, MAN, AND CYBERNETICS�PART A: SYSTEMS
%       AND HUMANS, Vol. 38, No. 1, January 2008
%
%   [2] M. Blanke, M. Kinnaert, J. Schr�der, and J. Lunze. Diagnosis and
%       fault-tolerant control. Springer, second edition, 2006
%
%   [3] D. D�stegor, V. Cocquempot and M. Staroswiecki, Structural analysis
%       for fault detection and isolation: An algorithmic study.,
%       Proceedings of the 2nd International IFAC symposium on system, 2004
%
%   See also: sa_backtrack

%   Copyright 2002-2013 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 17/10-2012
%   Based on matching algorithms by Torsten Lorentzen, Dilek D�stegor and
%   Mattias Krysander.
%   Last modified - 27/01-2012

% Check input arguments
[valid,msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Parse input parameters
p = inputParser;
p.KeepUnmatched = true;
p.addOptional('algorithm', 'rank', ...
    @(x) any(validatestring(x, {'rank', 'mso', 'full', 'custom'})));
p.addOptional('backtrack', true, @(x) islogical(x));
p.addOptional('ignore_causalities', false, @(x) islogical(x));
p.addOptional('function_handle', @(x,y) [], @(x) isa(x, 'function_handle'));

% Parse input arguments
p.parse(varargin{:})

% Reset matchings and parity equations of input system
sys.matchings = [];
sys.parityEqs = {};

% Predefine output variables
symbolic = {};
analytic = {};
vargout = {};

% Call appropriate sub-algorithm or invoke custom function.
switch lower(p.Results.algorithm)
    case 'rank'
        sys = ranking_algorithm_adapter(sys);
    
    case 'full'
        sys = full_match_adapter(sys);

    case 'mso'
        sys = mso_match_adapter(sys);
        
    case 'custom'
        [sys, vargout] = p.Results.function_handle(sys, varargin);
        [valid, msg] = sa_check(sys);
        if (~valid)
            error(['Custom algorithm does not return a valid system.', ...
                '\n%s'], msg);
        end
end
    
% In case the used algorithm only returns matchings, the backtracking
% algorithm is used to obtain parity equations from them.
if (~isempty(sys.matchings) && isempty(sys.parityEqs))
    % Remove matchings that contain invalid causalities.
    if ~p.Results.ignore_causalities
        validMatchings = find_non_causal_matchings(sys);
        sys.matchings = sys.matchings(validMatchings, :);
    end
    % Perform backtracking.
    if (p.Results.backtrack)
        if (nargout <= 1)
            sys = sa_backtrack(sys);
        elseif (nargout <= 2)
            [sys, symbolic] = sa_backtrack(sys);
        else
            [sys, symbolic, analytic] = sa_backtrack(sys);
        end
    end
end

end

%% Causality filter
% Find matchings with causalities.
function validMatchings = find_non_causal_matchings(sys, index)
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    if (nargin == 1 || ~isvector(index))
        index = 1:size(sys.matchings, 1);
    end
    % Return indices of unknown variables with causalities and their
    % respective constraints.
    [consInd, unknownInd] = find(sys.incMatrix(:,numKnowns+1:end)==-1);
    % Check if causalities exist.
    if (~isempty(unknownInd))
        % Build compare matrix from constraints that contain causalities.
        compMatrix = repmat(unknownInd', length(index), 1);
        % Check equality of the matched unknown variables of the selected
        % constraints from all matchings with the unknown variables
        % containing causalities. If any equality is found for a row, then
        % that matching is invalid.
        validMatchings = index(~any(sys.matchings(index, consInd) == ...
            compMatrix, 2));
    else
        validMatchings = index;
    end
end

%% Ranking algorithm
% Adapter to use old ranking match algorithm with the new data structure.
function [sys] = ranking_algorithm_adapter(sys)
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    % Perform canonical decomposition
    [uc, uu, jc, ju, oc, ou] = sa_decomp(sys);
    % Check whether a matching exists.
    if ~isempty(oc) || ~isempty(jc)
        % Mark only enabled constraints as usable.
        sys.matchings(1, 1:size(sys.incMatrix, 1)) = -1;
        sys.matchings(1, sys.enabledCons) = 0;
    end
    % Check for a over-determined sub-part.
    if ~isempty(oc)
        % Convert "new" incidence matrix where a -1 marks a one-sided
        % connection into the old representation where the mark was _inf_.
        incM = [sys.incMatrix(oc, 1:numKnowns), ...
            sys.incMatrix(oc, ou + numKnowns)];
        incM(incM==-1) = inf;
        % Define inputs
        numUnknowns = length(ou);
        numCons = length(oc);
        % Call old ranking algorithm.
        [matchedCons, matchedUnknowns, ~, ~, ~] = ...
            ranking_algorithm(incM, numKnowns, numUnknowns, numCons, [], []);
        % Adapt results.
        if (~isempty(matchedCons) && ...
                (length(matchedCons) == length(matchedUnknowns)));
            % Save matchings.
            sys.matchings(1, oc(matchedCons)) = ou(matchedUnknowns);
        end
    end
    % Check for just-determined sub-part.
    if ~isempty(jc)
        sys.matchings(1, jc) = ju;
    end
    if ~isempty(sys.matchings)
        % Mark under-determined constraints as not used (-1).
        sys.matchings(1, uc) = -1;
    end
end

% Original ranking algorithm
function [CON_matched,UNK_matched,CON_rank,VAR_rank,UNM_vars] = ...
    ranking_algorithm(Origin_matrix,num_of_known,num_of_unknown,...
    num_of_const,matched_unknown,matched_cons)
% The idea of matching is: The matching starts with the constraints, called KF, that are
% connected to the known variables. The unknown variables that can be 
% matched to these by one-to-one (injective) basis are chosen. These new variables
% are now "Known". The corresponding connected contraints are found (surjective mapping)
% and these are now assumed to be the new KF. The process then continues iteratively. 
% The program ends when no new KF is found.
%
% Input:
%   Origin_matrix   : Boolean incidence matrix mapping constraints to variables
%                     columns must be sorted with known variables first then unknown
%   num_of_known    : number of known variables in Origin_matrix
%   num_of_unknown  : number of unknown variables in Origin_matrix
%   num_of_const    : number of constraints in Origin_matrix
%   matched_unknown : indices of previously matched unknown variables (eq part of identified loop)
%   matched_cons    : indices of previously matched constraints (eq part of identified loop)
%
% Output
%   CON_matched     : vector containing index of matched constraints
%   UNK_matched     : vector containing index of matched unknown variables
%   CON_rank        : vector containing the rank of all constraints
%   VAR_rank        : vector containing the rank of all variables (known variables are ranked 0)
%   UNM_vars        : vector of unmatched variables (non empty if underdetermined subsystem exist)
%
% The function restructures the incidence matrix of a system's structural
% model to an incidence matrix of a corresponding di-graph representation of 
% the same model form the following form
%
%                      K | U 
%                  f [ - | - ]
%                  f [ - | - ]
%
% to  the form given bellow
%
%                    K | f | U
%               K  [ 0 | - | 0]
%               f  [ - | 0 | -]
%               U  [ 0 | - | 0]
% The program checks for the analytic redundancy possibilities when unknown
% variables are involved (i.e. when F_X exists). Analytic redundancy
% involving only known variables are not of interest (can be easily built
% up). 
%
% Originally developed by Roozbeh Izadi-Zamanabadi
%
% Modified by Torsten Lorentzen (transformed from script to function and 
%                           identification of multiple matched variables
%                           modifications are marked with TL)
% modified 13/1-2003 by Torsten Lorentzen
% The function now handles multiple matched variables in a prober manner
% modified 19/2-2003 by Torsten Lorentzen
% Modified to rank the constraints as in algorithm 5.1 in FTCBook 
%-------------------------------------------------------------
% Check for the size correction at the entry
%-------------------------------------------------------------
[l_matrix, h_matrix]=size(Origin_matrix);

if num_of_const ~= l_matrix,
  disp(' ');
  disp('The number of constraints are wrong,');
  disp(' ');
  return
elseif (num_of_known + num_of_unknown)~= h_matrix,
  disp(' ');
  disp('The number of variables are wrong,');
  disp(' ');
  return
end


%-------------------------------------------------------------
% Buildup the part matrices
%-------------------------------------------------------------
U_matrix = Origin_matrix(:,num_of_known+1:num_of_known+num_of_unknown);
Known_matrix = Origin_matrix(:,1:num_of_known);

%test for Causality

[I,J]=find(isinf(U_matrix));

%% Dealing with causality
Unknown_matrix =  U_matrix;
Unknown_matrix1 =  U_matrix;

if ~isempty(I),
  for i=1:length(I), 
    % Connection from constraints to the unknown vars
    Unknown_matrix(I(i),J(i))=1;
    % Connection from unknown vars to the constraints 
    Unknown_matrix1(I(i),J(i))=0;
  end;
end;


%-------------------------------------------------------------
% Condensating the matrix
%-------------------------------------------------------------
condenced_known=sum(Origin_matrix(:,1:num_of_known),2);
known_vect_indices = find(condenced_known);
known_vect = condenced_known;
known_vect(known_vect_indices)=1;


condenced_matrix = [known_vect Unknown_matrix];

KnownVector  = known_vect;
KnownVectorT = known_vect';
FtoUnknown = Unknown_matrix;
UnknowntoF =  Unknown_matrix1';

% -------------------------------------------------------------
% New matrix for the di-graph representation
% -------------------------------------------------------------
I_md = [ 0  KnownVectorT zeros(1,num_of_unknown)
  KnownVector zeros(num_of_const,num_of_const) FtoUnknown
  zeros(num_of_unknown,1) UnknowntoF  zeros(num_of_unknown,num_of_unknown)];


global TotalPars  % the size of the matrix (TotalPars x TotalPars)
TotalPars = num_of_unknown + num_of_const + 1;


% -------------------------------------------------------------
% Matching program begins
% -------------------------------------------------------------

% initialization

KF  = KnownVector;
A   = FtoUnknown ;
A_s = UnknowntoF ; 

CON_matched = []; 			%List of matched constraints 
UNK_matched = []; 			%List of matched unknowns
ITER_matched = [];      % List of the iteration where the matching occur

CON_rank = zeros(1,num_of_const); % List of ranking of the constraints
UNK_rank = zeros(1,num_of_unknown);  % List of ranking of the unknown variables
VAR_rank = zeros(1,num_of_known+num_of_unknown); % List of ranking of the variables
RED_vec = [];
%-------------------------------------------------------------
% Added by TL
%-------------------------------------------------------------
doublematched = [];
doublematchedconstraints = [];
%-------------------------------------------------------------

%% ============================================================================
%% The loop for recursive matching of the constraints and the unknown variables
%% ============================================================================

flag = 1;
iter = 1;
while flag == 1
  
  %***********************************************************************
  %Projections of KF on A_s to find out about all unknowns that are 
  %matched to the contraints
  %***********************************************************************
  projectKFtoA_s = A_s & (ones(num_of_unknown,1)*KF');
  
  projectKFtoA = A' & (ones(num_of_unknown,1)*KF');
  %***********************************************************************
  % Find out if any unmatched constraints exists that are not connected to 
  % any unknown variables. If so they are redundant and therefore marked (added by TL)
  %***********************************************************************
  index = find(sum(A) == 0);
  for i = 1:length(index)
    if isempty(find(CON_matched==index(i)))
      if isempty(find(RED_vec==index(i)))
        RED_vec = [RED_vec index(i)];
        CON_rank(index(i)) = iter-1;
      end
    end
  end
  
  %***********************************************************************
  %find out how many unknowns are related to each constraint
  %***********************************************************************
  %  index = find(sum(projectKFtoA_s) > 1);
  index = find(sum(projectKFtoA) > 1);
  if ~isempty(index),
    projectKFtoA_s(:,index)=0;	
    % Disconnect those constraints that are
    % connected to more than one unknown var. By this way we make sure tha
    % any matched unknown var is reachable/ observable.
  end
  
  %***********************************************************************
  %Jp is the Index of the matched constraints
  %Ip is the Index of the matched unknown variables
  %***********************************************************************
  projectKFtoA_s;
  [Ip,Jp] = find(projectKFtoA_s);
  
  %-------------------------------------------------------------
  % Detect if several constraints are used to match the same variable.
  % Added by TL
  %-------------------------------------------------------------
  multimatched = find(sum(projectKFtoA_s')>1);
  doublematched = [doublematched find(sum(projectKFtoA_s')>1)];
  [row,col]=find(projectKFtoA_s(doublematched,:));
  %  make sure that col is a column vector for some reason col is sometimes a row !!!
  [r1,c1] = size(col);
  if r1 < 2
    col = col';
  end
  if ~isempty(col)
    doublematchedconstraints = [doublematchedconstraints col'];
  end  
  % Always select the first (lowest numbered) constraint if several 
  % are matching the same variable 
  for i = 1:length(multimatched)
    multi_index = find(Ip==multimatched(i));
    for j = 2:length(multi_index)
      
      Jp = [Jp(1:multi_index-1); Jp(multi_index+1:end)];
      Ip = [Ip(1:multi_index-1); Ip(multi_index+1:end)];
    end
  end
  %-------------------------------------------------------------
  
  CON_matched = [CON_matched, Jp']; 	% Matched constraints , and
  UNK_matched = [UNK_matched, Ip']; 	% Their corresponding matched unknown variables
  ITER_matched = [ITER_matched  ones(1,length(Ip))*iter];
  
  CON_rank(Jp) = iter;
  UNK_rank(Ip) = iter;
  %***********************************************************************
  %If no new matching is found the program tests for the Over/just/under-determined
  %part of the system.
  %***********************************************************************
  if isempty(Jp),  
    %Stop the iteration
    flag = 0;
    XK = zeros(1,num_of_unknown);
    XK(UNK_matched) =1;
    F  = zeros(1,num_of_const);
    F(CON_matched) = 1;
    
    %% find all constraints that are connected to the matched unknowns.
    F_related_to_XK_mat = A_s & (XK' * ones(1,num_of_const));
    F_related_to_XK_index = find(sum(F_related_to_XK_mat));
    F_related_to_XK_vec = zeros(1,num_of_const);
    F_related_to_XK_vec(F_related_to_XK_index) = 1;
    
    F_redundant_vec = xor(F_related_to_XK_vec, F);
    F_redundant_index = find(F_redundant_vec);
    %     if isempty(F_redundant_index),
    %       disp('No redundant information is found, i.e No Over-determined part exists'),
    %     else
    %       disp(' Over-determined part exists')
    %       disp(' The following unmatched constrainst are found')
    %       disp (find(~F_redundant_vec))
    %     end
    UNM_vars = find(~XK);
    
    %     if ~isempty(UNM_vars)
    %       unmatched_str = sprintf('x%i ',UNM_vars);
    %       msgbox(sprintf('Underdetermined subsystem exists containing the variables:\n %s',unmatched_str),'Warning','Warn','modal')
    %       %      disp('Underdetermined subsystem exists')
    %       %      disp(' Un-Matched variables')
    %       %      disp(unmatched_variables)
    %     end
    
  else  
    
    %***********************************************************************
    %get rid af all matched unknowns 
    %***********************************************************************
    X_known = zeros(num_of_unknown,1);
    X_known(UNK_matched) = 1; 		% All the matched unknowns  (assumed 
    % to be known)
    X_unknown = ~X_known; 		% All the unknowns not yet matched
    
    %***********************************************************************
    %-----------      get rid of all matched unknowns in A_s -------------
    %***********************************************************************
    index = find(X_known);
    
    A_s(index,:)=0;
    
    %***********************************************************************
    %get rid af all matched constraints in A and A_s
    %***********************************************************************
    A(Jp,:)=0;
    %    A_s(:,Jp)=0;
    %***********************************************************************
    %%%%% HERE WE FIND THE MATCHED NEW KF (KF_new) THAT CORRESPONDS
    %%%%% TO THE NEW KNOWN VARIABLES IN X_known
    %***********************************************************************
    
    projectX_knowntoA = A & (ones(num_of_const,1)*X_known');
    A(:,index) = 0;    
    %***********************************************************************
    %% find the related constraints to the "X_known" variables
    %***********************************************************************
    ind=find(sum(projectX_knowntoA'));
    %***********************************************************************
    %The remaining constraints that can/shall be matched again 
    %***********************************************************************
    
    KF = zeros(num_of_const,1);
    KF(ind) = 1;
    
    if sum(KF) >0, 			% If there is still some constraints 
      %    left then repeat the procedure.
      flag =1;
    end
  end
  iter = iter+1;
end  
%% ============================================================================
%% The maching loop ends here.
%% ============================================================================
VAR_rank = [zeros(1,num_of_known) UNK_rank];
end

%% Dilec's full matching algorithm
function sys = full_match_adapter(sys)
    % Perform canonical decomposition.
    [uc, uu, jc, ju, oc, ou] = sa_decomp(sys);
    % Calculate internal variables.
    numCons = size(sys.incMatrix, 1);
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    numUnknowns = size(sys.incMatrix, 2) - numKnowns;
    % Check whether a matching exists
    if length(uu) < numUnknowns
        % Get over-determined subpart and do matching if it exists.
        over = sys.incMatrix(oc, numKnowns+ou);
        if (~isempty(over))
            matchings = dilek_match(over, 1, zeros(1, size(over, 2)), ...
                zeros(1, size(over, 1)), []);
            % Initialize matchings with not used (-1) and mark all enable
            % constraints as for use (0).
            sys.matchings(1:size(matchings, 1), 1:numCons) = -1;
            sys.matchings(:, sys.enabledCons) = 0;
            % Assign over determined matchings.
            for i = 1:size(matchings, 1)
                sys.matchings(i, oc(matchings(i, :))) = ou;
            end
        else
            % Initialize matchings with not used (-1).
            sys.matchings(1, 1:numCons) = -1;
        end
        % Assign matching of the just-determined subpart if it exists.
        if (~isempty(jc))
            sys.matchings(:, jc) = repmat(ju, size(sys.matchings, 1), 1);
        end
        % Set under-determined subpart to not used (-1).
        if (~isempty(uc))
            sys.matchings(:, uc) = -1;
        end
    end
end

function [All_Match]= dilek_match(over,i,check_var,check_eq,All_Match)
%[All_Match]=dilec_match(over,i,check_var,check_eq)
%
%INPUT:
%over        ==> the over determined system from which all possible matching are derived
%i           ==> a counter that tells which variable is to be matched next
%check_var   ==> a vector of size |number of variables| that tells which variable has been matched
%check_eq    ==> a vector of size |number of equations| that tells which equation has been matched
%
%INITIALIZATION:
%i=1;
%check_var=zeros(1,size(over,2));
%check_eq=zeros(1,size(over,1));
%
%OUTPUT:
%All_Match   ==> a matrix of size |all possible matching|*|number of variables|
%Matchings are relative to the input matrix over
%
%Copyright (c) 2003-2006, All rights reserved. Dilek Dustegor

if i==size(over,2)+1,
    All_Match=check_var;
    return;
end;   
stack=[find(over(:,i))];  
stack=[stack;-1];  %% -1 stands for the end of the stack
top=stack(1,1);        

while top~=-1,
    while ((top~=-1) && (check_eq(top)~=0)),
        stack=stack(2:size(stack,1),1);
        top=stack(1,1);  
        if top==-1,
            break;   
        end;
    end;
    
    if (top~=-1),
        temp1=check_var;
        temp2=check_eq;
        temp1(1,i)=top;
        temp2(1,top)=i;
        [temp]=dilek_match(over,i+1,temp1,temp2,[]);
        All_Match=[All_Match;temp];
        stack=stack(2:size(stack,1),1);
    end;     
    top=stack(1,1);    
end;

check_var=zeros(1,size(over,2));
check_eq=zeros(1,size(over,1));
end

%% Matchings based on minimal overdetermined sets (Krysander's algorithm)
function sys = mso_match_adapter(sys) 
    numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);
    numCons = size(sys.incMatrix, 1);
    % Retrieve minimal overdetermined sets.
    S = FindMSO(abs(sys.incMatrix(sys.enabledCons, numKnowns+1:end)));
    % Preallocate memory for matchings.
    sys.matchings(1:length(S.MSOs), 1:numCons) = -1;
    for i = 1:length(S.MSOs)
        cons = sys.enabledCons(S.MSOs{i});
        if length(cons) > 1
            for j = 1:length(cons)
                [~, ~, jc, ju, ~, ~] = sa_decomp(sys, ...
                    [cons(1:j-1), cons(j+1:end)]);
                sys.matchings(i, jc) = ju;
                sys.matchings(i, cons(j)) = 0;
                % Check sub-matching for causality and if sub-matching
                % is valid, break and proceed with next matching.
                if ~isempty(find_non_causal_matchings(sys, i))
                    break;
                end
            end
        end
    end
end

function S = FindMSO(sm,p)

% FindMSO  Performs structural analysis to obtain all MSOs and optionally
% also all PSOs.
%
%    S = FindMSO(sm,p)
%
%  Inputs:
%      - sm   A structural model of the unnknown variables represented with
%             its biadjacency matrix.
%
%      - p    If p is true all PSO sets are also stored in the output
%             S.PSOs. The default value is false.
%
%  Outputs:
%    S.MSOs      - The family of all MSO sets in sm. 
%
%    S.PSOs      - The family of all PSO sets in sm.
%
%  Example:
%    >> sm = [1 0;1 0;1 1;0 1;0 1];
%    >> p = false;
%    >> S = FindMSO(sm,p)
%
%    S = 
%
%        MSOs: {[4 5]  [3 2 5]  [3 2 4]  [1 3 5]  [1 3 4]  [1 2]}
%
% Author:   Mattias Krysander Revision: 0.1,  
% Date: 2008/01/16 Copyright (C) 2008 Mattias Krysander
%
% FindMSO is free software; you can redistribute it and/or modify it under
% the terms of the GNU General Public License as published by the Free
% Software Foundation; either version 2 of the License, or (at your option)
% any later version.
%
% FindMSO is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
% more details.
%
% You should have received a copy of the GNU General Public License along
% with FindMSO; if not, write to the Free Software Foundation, Inc.,
% 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
global withPSO
if nargin<2
    withPSO = 0;
else
    withPSO = p;
end

[row_over,col_over]=Mp(sm); %Computes the overdetermined part.
sr = length(row_over)-length(col_over);
S.MSOs = {};
if withPSO
        S.PSOs = {};
end
if sr > 0
    sm = sm(row_over,col_over);
    for i=1:length(row_over)
        M{i} = row_over(i);
    end
    delM = 1;
    no_classes = length(M);
    S.MSOs = {};
    
    S = sub(sm,M,sr,delM,no_classes);
end
end

function S = sub(sm,M,sr,delM,no_classes)
% A subroutine to FindMSO
global withPSO

if withPSO 
    S.PSOs = {[M{:}]};
end
if sr==1
    S.MSOs = {[M{:}]};
else
    S.MSOs = {};
    Mesleft = no_classes - delM + 1;

    while sr-1<=Mesleft;
        idxM = [1:delM-1 delM+1:no_classes];
        [row_just,row_over,col_over]=GetJustOver(sm(idxM,:));

        merge = length(row_just)>0;

        if merge
            no_rows_before = sum(row_just < delM);
            Mesleft = Mesleft - (length(row_just) - no_rows_before);

            if sr-1<=Mesleft
                mergeclasses = [idxM(row_just) delM];
                delM = delM - no_rows_before;
                sm =[sm(idxM(row_over(1:delM-1)),col_over);...
                    any(sm(mergeclasses,col_over));...
                    sm(idxM(row_over(delM:end)),col_over)];
                M = [M(idxM(row_over(1:delM-1))) {[M{mergeclasses}]}...
                    M(idxM(row_over(delM:end)))];
                no_classes = no_classes - length(row_just);
                if no_rows_before==0
                    idxM = [1:delM-1 delM+1:no_classes];
                    Sn=sub(sm(idxM,:),M(idxM),sr-1,delM,no_classes-1);
                    S.MSOs=[S.MSOs Sn.MSOs];
                    if withPSO 
                        S.PSOs =[S.PSOs Sn.PSOs];
                    end
                end
                delM=delM+1;
                Mesleft = no_classes - delM + 1;
            else
                break;
            end
        else
            Sn=sub(sm(idxM,:),M(idxM),sr-1,delM,no_classes-1);
            S.MSOs=[S.MSOs Sn.MSOs];
            if withPSO 
                S.PSOs =[S.PSOs Sn.PSOs];
            end
            delM=delM+1;
            Mesleft = no_classes - delM + 1;
        end
    end
end
end

function [row_just,row_over,col_over]=GetJustOver(sm)
%   Computes the just and over determined part of a structural
%              matrix. 
%
%    [row_just,row_over,col_over]=GetJustOver(sm)
%
%  Inputs:
%    sm  - A biadjacency matrix with a non-empty structurally
%                 overdetermined part and an empty structurally underdetermined part. 
%
%  Output:
%    row_just   - The row indices of the just-determind part.
%    row_over   - The row indices of the overdetermined part. 
%    col_over   - The column indices of the overdetermined part.
if ~size(sm,2)
    col_over = [];
    row_just = [];
    row_over = 1:size(sm,1);
else
    [p,q,r,s]=dmperm(sm);

    %last component
    rtmp=[r(end-1):r(end)-1];
    k=[s(end-1):s(end)-1];
    row_over = sort(p(rtmp)); % beh�vs detta
    col_over = q(k);

    %all but the last component
    r=[1:r(end-1)-1];
    row_just = p(r);
end
end

function [row_over,col_over]=Mp(sm)
%   Computes the overdetermined part of a structural
%              matrix. 
%
%    [row_over,col_over]=Mp(sm)
%
%  Inputs:
%    sm  - A biadjacency matrix. 
%
%  Output:
%    row_over   - The row indices of the overdetermined part. 
%    col_over   - The column indices of the overdetermined part.
if ~size(sm,2)
    col_over = [];
    row_over = 1:size(sm,1);
else
    [p,q,r,s]=dmperm(sm);
    rtmp=[r(end-1):r(end)-1];
    k=[s(end-1):s(end)-1];
    if length(rtmp) > length(k) % if overdetermined part exist
        row_over = sort(p(rtmp)); % beh�vs detta
        col_over = q(k);
    else
        row_over = [];
        col_over = [];
    end
end
end