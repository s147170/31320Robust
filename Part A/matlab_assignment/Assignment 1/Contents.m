% STRUCTURAL ANALYSIS TOOLBOX
% Version 2.0200 (R2011a) 06-Feb-2017
%
% TABLE OF CONTENTS (TOC)
% -----------------------
% System data creation and import
%   sa_create              - Create structural analysis system.
%   sa_load                - Load structural analysis system.
%   sa_rand                - Create random structural analysis system.
%
% System manipulation
%   sa_disable             - Disable variables and constraints.
%   sa_match               - Perform matching on a structural analysis system.
%   sa_backtrack           - Backtrack matchings to obtain parity equations.
%   sa_incidencematrix     - Generate an incidence matrix from system specification
%
% System analysis
%   sa_controllability     - Examine controllability of a structural analysis system.
%   sa_detectability       - Examine detectability of faults in the constraints.
%   sa_isolability         - Examine isolability of faults in the constraints.
%   sa_reachability        - Examine reachability of a structural analysis system.
%
% System serialization
%   sa_save                - Save structural analysis system.
%   sa_disp                - Display information about a structural analysis system.
%   sa_report              - Generate a report of a structural system.
%
% Optimize isolability
%   sa_msoselect           - Select a set of residual generators among MSO sets
%                            to obtain desired structural isolability
%
% Helper function
%   sa_adj                 - Retrieve adjacency matrix of a structural analysis system.
%   sa_check               - Check validity of structural analysis system.
%   sa_decomp              - Perform canonical decomposition of a structural analysis system.
%   sa_names               - Returns the names of the constraints and variables of a system.
%   sa_solve               - Calls the Matlab symbolic toolbox to obtain
%                            analytical expressions for parity relations
%
%   strjoin                - Join cell array elements with a string.
%
% Graphical User Interface (GUI)
%   sa_gui                 - Open GUI to create/edit a structural analysis system.
