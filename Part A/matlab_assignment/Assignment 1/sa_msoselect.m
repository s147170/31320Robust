%%%%%%% Selection of Residual Generators Using a greedy approach %%%%%%%
% See Svärd, C., Nyberg, M., Frisk, E., 2011. A greedy approach for selection of residual generators, in: Proceedings of the 22nd International Workshop on Principles of Diagnosis (DX-11).
% for details

% The inputs are:
% sys = the system
% Sg = parity equations in matrix form
%  ie.   Sg=cell2mat(sys.parityEqs);
% F = Isolation requirement
% ie. Matrix indicating which faults should be isolable from each other
% 1  => the fault is present in the residual
% -1 => the fault is not present in the residual
% 0  => it does not matter whether the fault is present.
% Example:
%           3 possible faults should all be isolable from each other
%           
%       F = [1 -1 0;
%            1 0 -1;
%            0 1 -1;
%            0 0 1 ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function R_idx = sa_selectResGenGreedy(sys,Sg,F)
    
    %%Find Isolation Classes
    clear I
    I{size(F,1)} = [];
    for i=1:size(Sg,1)
        for j=1:size(F,1)
            if (sum(Sg(i,:).*F(j,:)) == 1)
                I{j} = [I{j},i];
            end
        end
    end
    
    
    clear R_idx R
    cnt = 1;
%     cnt2 = length(Sg);
    %%Greedy Selection
    while ~isempty(cell2mat(I)) % Stop when all isolation requirements are fulfilled
        if ~isempty(Sg) || max(max(Sg)) > 0 % Or there are no more MSO sets
            [a,b]=hist(cell2mat(I),unique(cell2mat(I))); % Determine the number of MSO sets contained in each isolation class
            idxSG = find(a==max(a)); % Find the MSO sets contained in the most
            H = Sg(b(idxSG),:);
            [~,idxS] = min(sum(H,2)); % Find the MSO set with lowest cardinality
            if ~isempty(b(idxSG(idxS))) 
                [res,r] = sa_solve(sys,b(idxSG(idxS))); % solve for the residual
                b(idxSG(idxS));
                if ~isempty(res) && ~isempty(r) && isempty(findstr(char(r{1}), '*i')) % Is it realizable and real?
                    R_idx(cnt) = b(idxSG(idxS)) % Save the index to the solution
                    cnt = cnt+1;
%                     cnt2 = cnt2 -1
                    for j=1:size(F,1) % Remove the corresponding isolation classes
                        if (sum(Sg(b(idxSG(idxS)),:).*F(j,:)) == 1)
                            j;
                            I{j} = [];
                        end
                    end
                else
                    Sg(b(idxSG(idxS)),:) = 0; % If the residual is not realizable then remove it
%                     cnt2 = cnt2 -1
                    for i = 1:length(I)
                        I{i} = I{i}(I{i}~= b(idxSG(idxS)));
                    end
                    
                end
            end

        else
            break
        end
    end

end