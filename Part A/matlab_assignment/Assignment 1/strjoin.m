function str = strjoin(fields, glue)
%STRJOIN Join cell array elements with a string.
%   STR = STRJOIN(FIELDS, GLUE) joins all elements of the cell string array
%   FIELDS with the string GLUE and returns it.
%
%   Example:
%      fields = {'abc', 'def', 'ghi'};
%      glue = ', ';
%      str = strjoin(fields, glue);
%      % str is 'abc, def, ghi'.

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 06/11-2012
%   Last modified - 06/11-2012

% Check arguments.
if (~iscellstr(fields) || ~ischar(glue))
    error('Invalid arguments.');
end

% Get number of elements.
n = numel(fields);
if (n == 0)
    str = '';
else
    % Generate cell array with space for the fields and the glue inbetween.
    ss = cell(1, 2*n-1);
    % Assign fields and glue.
    ss(1:2:end) = fields;
    [ss{2:2:end}] = deal(glue);
    % Return cell array as string.
    str = [ss{:}];
end

end

