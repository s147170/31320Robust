function [isolable,notIsolable,notFail] = sa_isolability(sys, index, ...
    overall)
%SA_ISOLABILITY Examine isolability of faults in the constraints.
%   [ISOLABLE,NOTISOLABLE,NOTFAIL]=SA_ISOLABILITY(SYS) determines for all
%   parity equations found through matching of the system SYS the isolable,
%   non-isolable and not-failing constraints and returns their indices in 
%   ISOLABLE, NOTISOLABLE and NOTFAIL, respectively. Disabled constraints
%   are not considered.
%
%   Since a matching can find N different sets of parity equations and each
%   set may have different isolability properties, this algorithm
%   determines the isolability of each set separately. The return variables
%   ISOLABLE and NOTISOLABLE are thus cell row vectors of size N holding
%   the isolability properties of each set in one cell. The non-failable
%   contraints are identical for each set and hence returned once in a
%   vector.
%
%   [ISOLABLE,NOTISOLABLE,NOTFAIL]=SA_ISOLABILITY(SYS, INDEX) returns the
%   isolable, non-isolable and not-failing constraint only for the parity
%   equations of the matchings defined in INDEX. Use an empty vector to
%   analyze all matchings.
%
%   [ISOLABLE,NOTISOLABLE,NOTFAIL]=SA_ISOLABILITY(SYS, INDEX, 1) returns
%   the isolability over all specified parity equations. The two variables
%   ISOLABLE and NOTISOLABLE return in this case two vectors (instead of
%   cell vectors).
%
%   Isolable constraints are all those that have a unique column signature
%   that is different from zero can fail by definition.
%
%   Example:
%       % Examine isolability of random system.
%       sys = sa_rand();
%       sys = sa_match(sys);
%       [isolable,notIsolable] = sa_isolability(sys);
%       for i = 1:size(isolable, 1)
%           fprintf('Isolable faults for matching %d:\n', i);
%           disp(isolable{i});
%       end
%
%       % More elegant is to use sa_disp().
%       sa_disp(sys, 'i');
%
%   See also: sa_disp sa_detectability sa_match.

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 15/10-2012
%   Based on isolable.m by Torsten Lorentzen - 02/12-2003
%   Last modified - 02/12-2012

% Check system
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Check arguments
if (nargin == 1 || ~isvector(index) || ~isnumeric(index))
    index = 1:size(sys.parityEqs, 1);
else
    % Filter valid indices from index vector.
    index = intersect(index, 1:size(sys.parityEqs, 1));
end

if (nargin < 3)
    overall = false;
end

% The notFail vector is independent of the sets of parity equations and
% thus always the same.
notFail = setdiff(sys.enabledCons, sys.canFail);
% Get vector of enabled constraints that can also fail.
enabledAndCanFailCons = intersect(sys.enabledCons, sys.canFail);

% Check if parity equations exist
if isempty(sys.parityEqs)
    % If no parity equations exist, the isolable set is empty, the
    % non-isolable set consists of all contraints that are enabled and can
    % fail and the notFail part is the part of the enable constraints that
    % are not non-isolable.
    isolable = {[]};
    notIsolable = {enabledAndCanFailCons};
else
    if (~overall)
        % Preallocate memory for isolable and non-isolable part. Since the
        % parts can differ for each set of parity equations, cell arrays
        % are necessary.
        isolable = cell(length(index), 1);
        notIsolable = cell(length(index), 1);
        % For each set of parity equations return the isolable and non-isolable
        % part.
        for i=1:length(index)
            [isolable{i},notIsolable{i}] = get_isolability(...
                sys.parityEqs{index(i)}, enabledAndCanFailCons);
        end
    else
        [isolable,notIsolable] = get_isolability(...
                cell2mat(sys.parityEqs(index)), enabledAndCanFailCons);
    end
end

end

function [isolable,notIsolable] = get_isolability(parityEqs, ...
    enabledAndCanFailCons)
    % Consider only the columns with constraints that are enabled and that
    % can fail. Further transpose matrix since the set functions only work
    % on rows but the vector of interest are the columns.
    M = parityEqs(:, enabledAndCanFailCons)';
    % Find unique columns
    [~, m, ~] = unique(M, 'rows');
    % Find set of indices of duplicated rows: m returns the indices of the
    % unique columns and dup the inverse, hence rows that are not unique.
    [l,o] = size(M);
    dup = setxor(1:l, m);
    % Now remove all rows from M that look like M(dup,:) which are the rows
    % that are not unique including zero rows and return the indices of the
    % rows that are not in both sets and hence only existing once.
    [~, ia, ~] = setxor(M, [M(dup,:); zeros(1, o)], 'rows');
    % In ia' the indices of the unique elements are stored in arbitrary
    % order. The isolable constraints are those that are found in the set
    % of enabled and failable constraints. Also sort the result for
    % convenience.
    isolable = sort(enabledAndCanFailCons(ia));
    % The non-isolable part consists out of all enabled constraints minus
    % the constraints in the isolable part.
    notIsolable = setdiff(enabledAndCanFailCons, isolable);
end

% Another way to calculate the isolability by reducing the matrix to a
% vector first by calculating the decimal number of each binary column and
% then applying the same method as before.
% function [isolable,notIsolable] = get_isolability2(sys, index, ...
%     enabledAndCanFailCons)
%     % Consider only the columns with constraints that are enabled and that
%     % can fail. Further transpose matrix since the set functions only work
%     % on rows but the vector of interest are the columns.
%     B = sys.parityEqs(:, enabledAndCanFailCons, index)';
%     % Convert binary matrix rows into decimals
%     D = bi2de(B);
%     % Get indices of unique elements (that are elements that have different
%     % values).
%     [~, m, ~] = unique(D);
%     % Find set of indices of duplicated rows: m returns the indices of the
%     % unique columns and dup the inverse, hence rows that are not unique.
%     l = length(D);
%     dup = setxor(1:l, m);
%     % Now remove all rows from D that look like B(dup) which are the
%     % elements that are not unique including zero rows and return the
%     % indices of those that are not in both sets and hence only existing
%     % once.
%     [~, ia, ~] = setxor(D, [D(dup); 0]);
%     % In ia' the indices of the unique elements are stored in arbitrary
%     % order. The isolable constraints are those that are found in the set
%     % of enabled and failable constraints. Also sort the result for
%     % convenience.
%     isolable = intersect(enabledAndCanFailCons, ia);
%     % The non-isolable part consists out of all enabled constraints minus
%     % the constraints in the isolable part.
%     notIsolable = setdiff(enabledAndCanFailCons, isolable);
% end