function [controllable,notControllable] = sa_controllability(sys)
%SA_CONTROLLABILITY Examine controllability of a structural analysis system.
%	[CONTROLLABLE,NOTCONTROLLABLE]=SA_CONTROLLABILITY(SYS) examines if the
%   system SYS is structurally controllable. CONTROLLABLE and
%   NOTCONTROLLABLE return a vector of indices of the unknown variables
%   that are controllable and not controllable, respectively. Disabled
%   constraints and variables are not included in this analysis.
%
%   This function equals SA_REACHABILITY(SYS, 1). See the help for that
%   function for more information.
%
%   Example:
%       % Examine controllability of random system.
%       sys = sa_rand();
%       [contr,nonContr] = sa_controllability(sys);
%       [~,~,~,unknowns] = sa_names(sys);
%       disp('Controllable unknown variables:');
%       disp(unknowns(contr));
%
%       % More elegant is to use sa_disp().
%       sa_disp(sys, 'c');
%
%   See also: sa_disp sa_reachability.

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 13/10-2012
%   Last modified - 02/12-2012

[controllable,notControllable] = sa_reachability(sys, 1);

end

