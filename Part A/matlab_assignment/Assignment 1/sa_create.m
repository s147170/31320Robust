function sys = sa_create(m, varargin)
%SA_CREATE Create structural analysis system.
%   SYS=SA_CREATE(M,NI,NM) creates a structural analysis system structure
%   from the incidence matrix M with NI inputs and NM measurements.
%   Constraints, inputs, measurements and variables are given default
%   names. The columns of the incidence matrix M are to be orded in inputs,
%   measurements and unknown variables from left to right.
%
%   SYS=SA_CREATE(M,C,U,Y,X) creates a structural analysis system structure
%   from the incidence matrix M with the names of the constraints, inputs,
%   measurements and unknown variables given in C, U, Y and X,
%   respectively. The length of C and U+Y+X must be equal to the number of
%   rows and columns of M, respectively.
%
%   SYS=SA_CREATE(M,C,U,Y,X,D) further allows to specify the domain D
%   of each constraint. D is a vector of numbers with the same length as C.
%   A domain can be used to tag subparts of a system. By default the domain
%   of all constraints is one.
%
%   SYS=SA_CREATE(M,C,U,Y,X,D,F) further allows to specify which of the
%   constraints can fail. F is a vector of constraint indices that can
%   fail. By default all constraints can fail, thus F is a vector
%   [1:length(C)].
%
%   SYS=SA_CREATE(M,C,U,Y,X,D,F,P) further allows to specify which of the
%   Parameters of the system. These are not used in the analysis and only
%   exist for reporting purposes. It is thus possible to used LaTex
%   expressions for the parameters
%
%   The incidence matrix M should be in the format
%        u1 u2 uN  y1 y2 yN x1 x2 xN
%   M = [        |         |        ] c1
%       |   1    |         |        | c2
%       |        |         |   -1   | c3
%       |        |    1    |        | c4
%       [        |         |        ] cN
%   where c1, c2, ..., cN are the constraints, u1, c2, ..., uN the inputs,
%   y1, y2, ..., yN the measurements and x1, x2, ..., xN the unknowns of
%   the system, respectively. A cell M(i,j) contains a one (1), if the
%   variable Z(j) is part of the constraint C(i) and is undirected. A cell
%   M(i,j) contains a minus one (-1), if the variable Z(j) is part of the
%   constraint C(i) but Z(j) is directed and thus C(i) cannot be used to
%   express Z(j). M(i,j) is zero (0), if the variable Z(j) is not part of
%   the constraint C(i).
%
%   Example:
%       % Create system from incidence matrix with one input and two
%       % measurements with default names.
%       M = [1, 0, 0, 1, 0, 0, 0, 0, 0;
%            0, 0, 0, 1, 1, 0, 0, 0, 0;
%            0, 1, 0, 0, 1, 0, 0, 0, 0;
%            0, 0, 1, 0, 0, 1, 0, 0, 0;
%            0, 0, 0, 0, 0, 1, 1, 1, 0];
%       numInputs = 1;
%       numMeas = 2;
%       % Create system and display it.
%       sys = sa_create(M, numInputs, numMeas);
%       sa_disp(sys, 'm');
%
%       % Create same system from above but with custom naming.
%       cons = {'eq1', 'eq2', 'eq3', 'eq4', 'eq5'};
%       input = {'u'};
%       meas = {'y', 'z'};
%       unknowns = {'x1', 'x2', 'x3', 'x4', 'x5', 'x6'};
%       % Create system with custom naming and display it.
%       sys2 = sa_create(M, cons, input, meas, unknowns);
%       sa_disp(sys2, 'm');
%
%       % Create same system from above and further specify domains and
%       % constraints that can fail.
%       domains = [1, 1, 1, 2, 3];
%       canFail = [1:4]; % Constraint 5 cannot fail.
%       sys3 = sa_create(M, cons, input, meas, unknowns, domains, canFail);
%       sa_disp(sys3, 'm');
%
%   See also: sa_disp sa_load

%   Copyright 2002-2012 
%   Automation, DTU-Elektro, Technical University of Denmark
%   Created by Hans-Peter Wolf - 11/10-2012
%   Last modified - 13/10-2012

% Check and store number of function parameters
if (nargin == 3 || nargin == 5 || nargin == 6 || nargin == 7 || nargin == 8) % Added par so nargin can be == 8
    [numCons,numVars] = size(m);
    if (nargin == 3)    
        numInputs = varargin{1};
        numMeas = varargin{2};
        numUnknowns = numVars - numInputs - numMeas;
    else
        numInputs = size(varargin{2}, 2);
        numMeas = size(varargin{3}, 2);
        numUnknowns = size(varargin{4}, 2);
    end
else
    error('Invalid number of arguments.');
end

% Set default parameters
doms = ones(1, numCons); % Every domain is one
canFail = 1:numCons; % All constraints can fail
enabledCons = 1:numCons; % All constraints are enabled
%enabledVars = 1:numVars; % All variables are enabled

% Setup data but do not check type, since this is done later with
% function sa_check.
if (nargin == 3)
    % Create default names and latex representation
    cons = strcat('c', cellstr(num2str((1:numCons)', '%-1.0f'))');
    cons_latex = ...
        strcat('c_{', cellstr(num2str((1:numCons)', '%-1.0f'))', '}');
    cons = [cons; cons_latex];
    inputs = [];
    if (numInputs > 0)
        inputs = strcat('u', cellstr(num2str((1:numInputs)', '%-1.0f'))');
        inputs_latex = ...
            strcat('u_{', cellstr(num2str((1:numInputs)', '%-1.0f'))', '}');
        inputs = [inputs; inputs_latex];
    end
    meas = [];
    if (numMeas > 0 )
        meas = strcat('y', cellstr(num2str((1:numMeas)', '%-1.0f'))');
        meas_latex = ...
            strcat('y_{', cellstr(num2str((1:numMeas)', '%-1.0f'))', '}');
        meas = [meas; meas_latex];
    end
    unknowns = [];
    if (numUnknowns > 0)
        unknowns = strcat('x', cellstr(num2str((1:numUnknowns)', '%-1.0f'))');
        unknowns_latex = ...
            strcat('x_{', cellstr(num2str((1:numUnknowns)', '%-1.0f'))', '}');
        unknowns = [unknowns; unknowns_latex];
    end
    parameters = [];
else
    % Copy input parameters and remove unecessary spaces
    cons = varargin{1};
    inputs = varargin{2};
    meas = varargin{3};
    unknowns = varargin{4};
    parameters = [];
    if (iscellstr(cons))
        cons = strtrim(cons);
        [numRows, numCols] = size(cons);
        if (numRows < 2)
            cons(2, :) = repmat({''}, 1, numCols);
        end
        if (numRows < 3)
            cons(3, :) = repmat({''}, 1, numCols);
        end
    end
    if (iscellstr(inputs))
        inputs = strtrim(inputs);
        [numRows, numCols] = size(inputs);
        if (numRows < 2)
            inputs(2, :) = repmat({''}, 1, numCols);
        end
    end
    if (iscellstr(meas))
        meas = strtrim(meas);
        [numRows, numCols] = size(meas);
        if (numRows < 2)
            meas(2, :) = repmat({''}, 1, numCols);
        end
    end
    if (iscellstr(unknowns))
        unknowns = strtrim(unknowns);
        [numRows, numCols] = size(unknowns);
        if (numRows < 2)
            unknowns(2, :) = repmat({''}, 1, numCols);
        end
    end

    if (nargin >= 6)
        doms = varargin{5};
        % Use the same domain for all constraints in case domain is not an
        % array but just a single number.        
        if (length(doms) == 1)
            doms = repmat(doms, 1, numCons);
        end
    end
    if (nargin >= 7)
        canFail = varargin{6};
    end
    
    if (nargin >= 8)
        parameters = varargin{7};
        if (iscellstr(parameters))
            parameters = strtrim(parameters);
            [numRows, numCols] = size(parameters);
            if (numRows < 2)
                parameters(2, :) = repmat({''}, 1, numCols);
            end
        end
    end
end

% Create structure
sys = struct('incMatrix', m, 'cons', {cons}, 'inputs', {inputs}, ...
    'meas', {meas}, 'unknowns', {unknowns},'parameters', {parameters}, 'domains', doms, ...
    'canFail', canFail, 'enabledCons', enabledCons, ...
    ...'enabledVars', enabledVars, ...
    'matchings', [], 'parityEqs', []);

% Check for validity
[valid,msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Normalize m
sys.incMatrix(sys.incMatrix>0)=1;
sys.incMatrix(sys.incMatrix<0)=-1;

end