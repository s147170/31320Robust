function [uc, uu, jc, ju, oc, ou] = sa_decomp(sys, cons)
%SA_DECOMP Perform canonical decomposition of a structural analysis system.
%   [UC,UU,JC,JU,OC,OU] = SA_DECOMP(SYS) performs a canonical decompostion
%   and returns the indices of the constraints and unknown variables
%       - under-determined subpart in UC and UU,
%       - just-determined subpart in JC and JU and
%       - over-determined subpart in OC and OC,
%   respectively. Disabled constraints are not included.
%
%   [UC,UU,JC,JU,OC,OU] = SA_DECOMP(SYS,CONS) performs a canonical
%   decomposition only of the constraints specified in CONS. Constraints
%   that are disabled are again not included in this analysis.
%
%   This function internally uses the Dulmage-Mendelsohn decomposition
%   provided by dmperm and simply extends its usage for structural analysis
%   systems for convenience.
%
%   Example:
%       % Retrieve just-determined subpart of a system.
%       [~,~,jc,ju,~,~] = sa_decomp(sys);
%       [cons,~,~,unknowns] = sa_names(sys);
%       disp('Just-determined constraints:');
%       disp(cons(jc));
%       disp('Just-determined unknowns:');
%       disp(unknowns(ju));
%
%   See also dmperm.

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 02/11-2012
%   Last modified - 01/12-2012

% Check system.
[valid, msg] = sa_check(sys);
if (~valid)
    error(msg);
end

% Define enabled constraints and variables.
enabledCons = sys.enabledCons;
if (nargin > 1)
    enabledCons = intersect(cons, enabledCons);
end

% Calculate number of known variables.
numKnowns = size(sys.inputs, 2) + size(sys.meas, 2);

% Perform canonical decomposition of the enabled subpart.
[p,q,r,s] = dmperm(sys.incMatrix(enabledCons, numKnowns+1:end));

% The under-determined part is always rectangular with more variables than
% constraints, hence s(...) > r(...).
i = 1;
if (r(i+1) - r(i)) < (s(i+1) - s(i))
    uc = enabledCons(p(r(i):r(i+1)-1));
    uu = q(s(i):s(i+1)-1);
    % Increment the fine decomposition index for the left border of the
    % just-determined part.
    i = i + 1;
else
    uc = [];
    uu = [];
end

% The over-determined part is always rectangular with more constraints than
% variables. Hence, if the following inequality does not hold, there is no
% over-determined part.
b = length(r) - 1;
if (r(b+1) - r(b)) > (s(b+1) - s(b))
    oc = enabledCons(p(r(b):r(b+1)-1));
    ou = q(s(b):s(b+1)-1);
else
    oc = [];
    ou = [];
    % Increment the fine decomposition index for the right border of the
    % just-determined part.
    b = b + 1;
end

% The just-determined part is always square with the same constraints as
% variables. Hence if the following equality does not hold, there is no
% just-determined part.
if ((r(b) - r(i)) == (s(b) - s(i))) && ((r(b) - r(i)) > 0)
    jc = enabledCons(p(r(i):r(b)-1));
    ju = q(s(i):s(b)-1);    
else
    jc = [];
    ju = [];
end	

end
