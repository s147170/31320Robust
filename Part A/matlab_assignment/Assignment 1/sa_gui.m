function varargout = sa_gui(varargin)
%SA_GUI Open GUI to create/edit a structural analysis system.
%   SYS = SA_GUI(SYS) opens a graphical user interface (GUI) editor to
%   create and/or edit the properties of the structural analysis system SYS
%   and returns the modified system. If the system SYS is empty, the editor
%   starts with a simple system.
%
%   SA_GUI(SYS, 'standalone') opens a GUI in standalone mode with the data
%   of SYS. That means that the user control directly returns with the
%   opening of the GUI. In this mode however no output is returned.
%
%   See also: sa_create

%   Copyright 2002-2012 Automation, �rsted.DTU Technical University of
%   Denmark
%   Created by Hans-Peter Wolf - 10/01-2013
%   Last modified - 10/01-2013

% Last Modified by GUIDE v2.5 18-Jun-2013 14:33:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @editor_OpeningFcn, ...
                   'gui_OutputFcn',  @editor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before editor is made visible.
function editor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to editor (see VARARGIN)

if (~isempty(varargin) && sa_check(varargin{1}))
    handles.system = varargin{1};
else
    handles.system = [];
end

% Initialize variables
handles.selectedView = 0;
handles.variableSelection = [0, 0];
handles.constraintSelection = [0, 0];
handles.parameterSelection = [0, 0];
handles.changed = false;
handles.fileName = '';
handles.standalone = length(varargin) >= 2 && ...
    strcmp(varargin{2}, 'standalone');

% The docking controls are disabled by default. If standalone mode is
% actived, show them.
if (handles.standalone)
    set(hObject, 'WindowStyle', 'docked');
end

% Update handles structure
guidata(hObject, handles);

% Update GUI view.
selectView(handles, 1);
if (~isempty(handles.system))
    updateGUIfromSYS(handles, handles.system);
else
    internalClose(handles);
end

% UIWAIT makes editor wait for user response (see UIRESUME).
if (~handles.standalone)
    uiwait(handles.frmMain);
end


% --- Outputs from this function are returned to the command line.
function varargout = editor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if (sa_check(handles.system) && ~handles.standalone)
    varargout{1} = handles.system;
else
    varargout{1} = [];
end

if (~handles.standalone)
    delete(handles.frmMain);
end


% --- Executes when user attempts to close frmMain.
function frmMain_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to frmMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

canClose = internalClose(handles, true);
if canClose
    if isequal(get(hObject, 'waitstatus'), 'waiting')
        % The GUI is still in UIWAIT, us UIRESUME.
        uiresume(hObject);
    else
        % The GUI is no longer waiting, just close it.
        delete(hObject);
    end
end

% --- Executes when selected cell(s) is changed in tVariables.
function tVariables_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tVariables (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% Store selection in global user data structure.
handles.variableSelection = eventdata.Indices;
if (isempty(handles.variableSelection))
    handles.variableSelection = [0, 0];
end
guidata(hObject, handles);
internalEnableEditButtons(handles);


% --- Executes when selected cell(s) is changed in tConstraints.
function tConstraints_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tConstraints (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% Store selection in global user data structure.
handles.constraintSelection = eventdata.Indices;
if (isempty(handles.constraintSelection))
    handles.constraintSelection = [0, 0];
end
guidata(hObject, handles);
internalEnableEditButtons(handles);

% --- Executes when selected cell(s) is changed in tConstraints.
function tParameters_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tConstraints (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)

% Store selection in global user data structure.
handles.parameterSelection = eventdata.Indices;
if (isempty(handles.parameterSelection))
    handles.parameterSelection = [0, 0];
end
guidata(hObject, handles);
internalEnableEditButtons(handles);

    

% --- Executes when entered data in editable cell(s) in tConstraints.
function tConstraints_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tConstraints (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
if (~isequal(eventdata.PreviousData, eventdata.NewData))
    % Ignore changes of enabled flag of a constraint.
    if (handles.selectedView ~= 1 || eventdata.Indices(2) ~= 5)
        internalSetChanged(handles, true);
    end
end


% --- Executes when frmMain is resized.
function frmMain_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to frmMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
window = get(hObject, 'Position');
position = [0, 0, window(3), window(4)];
set(handles.tConstraints, 'Position', position);
set(handles.tVariables, 'Position', position);
set(handles.tMatrix, 'Position', position);
set(handles.tParameters, 'Position', position);
set(handles.axGraph, 'Position', position);

% --------------------------------------------------------------------
function mnuFileNew_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
canClose = internalClose(handles, true);
if (canClose)
    prompt = {'Enter number of constraints:', ...
        'Enter number of inputs:', ...
        'Enter number of measurements:', ...
        'Enter number of unknowns:'};
    dlgTitle = 'Input System Properties';
    numLines = 1;
    def = {'1', '1', '1', '0'};
    answer = inputdlg(prompt, dlgTitle, numLines, def);
    if (~isempty(answer))
        numVar = str2double(answer);
        % Check for invalid number.
        if ~any(isnan(numVar)) && numVar(1) > 0 && numVar(3) > 0
            system = sa_create(zeros(numVar(1), sum(numVar(2:end))), ...
                numVar(2), numVar(3));
            [valid, error] = sa_check(system);
            if valid
                userData = guidata(handles.frmMain);
                handles.system = system;
                guidata(handles.frmMain, userData);
                updateGUIfromSYS(handles, system);
                internalSetChanged(handles, true);
            else
                msgbox(error, 'System Error', 'error');
            end
            
        else
            msgbox('Invalid parameters.', 'System Error', 'error');
        end
    end
end

% --------------------------------------------------------------------
function mnuFileOpen_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileOpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileName, pathName] = uigetfile({'*.xml';'*.mat'});
if (ischar(fileName) && (exist([pathName, fileName], 'file') == 2))
    internalOpen(handles, [pathName, fileName]);
end


% --------------------------------------------------------------------
function mnuFileClose_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
internalClose(handles);

% --------------------------------------------------------------------
function mnuFileSave_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
internalSave(handles);


% --------------------------------------------------------------------
function mnuFileSaveAs_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileSaveAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileName, pathName] = uiputfile({'*.xml';'*.mat'});
if (ischar(fileName))
    internalSave(handles, [pathName, fileName]);
end


% --------------------------------------------------------------------
function mnuFileQuit_Callback(hObject, eventdata, handles)
% hObject    handle to mnuFileQuit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.frmMain);


% --------------------------------------------------------------------
function mnuViewConstraints_Callback(hObject, eventdata, handles)
% hObject    handle to mnuViewConstraints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectView(handles, 1);

% --------------------------------------------------------------------
function mnuViewVariables_Callback(hObject, eventdata, handles)
% hObject    handle to mnuViewVariables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectView(handles, 2);

% --------------------------------------------------------------------
function mnuViewMatrix_Callback(hObject, eventdata, handles)
% hObject    handle to mnuViewMatrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectView(handles, 3);

% --------------------------------------------------------------------
function mnuViewParameters_Callback(hObject, eventdata, handles)
% hObject    handle to mnuViewMatrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectView(handles, 4);


% --------------------------------------------------------------------
function btnInsert_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to btnInsert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
operation = get(hObject, 'UserData');
incMatrix = get(handles.tMatrix, 'Data');
success = false;
switch handles.selectedView
    % Constraints...
    case 1
        data = get(handles.tConstraints, 'Data');
        rowNr = handles.constraintSelection(1);
        % Create data for new row.
        newRow = [];
        switch (lower(operation))
            case 'insert'
                newRow = {'', '', '', data{rowNr, 4}, ...
                    data{rowNr, 5}, data{rowNr, 6}};

            case 'append'
                lastRow = size(data, 1);
                if (lastRow > 0)
                    newRow = {'', '', '', data{lastRow, 4}, ...
                        data{lastRow, 5}, data{lastRow, 6}};
                else
                    newRow = {'', '', '', 1, true, true};
                end
        end
        % Insert new row into existing data.
        [data, incMatrix, success] = manipulateData(data, ...
            operation, rowNr, newRow, incMatrix, true);
        % Set new data to view.
        set(handles.tConstraints, 'Data', data);

    % Variables...
    case 2
        data = get(handles.tVariables, 'Data');
        rowNr = handles.variableSelection(1);
        % Create data for new row.
        newRow = [];
        switch (lower(operation))
            case 'insert'
                newRow = {'', '', data{rowNr, 3}};

            case 'append'
                lastRow = size(data, 1);
                if (lastRow > 0)
                    newRow = {'', '', data{lastRow, 3}};
                else
                    newRow = {'', '', 'input'};
                end
        end
        % Insert new row into existing data.
        [data, incMatrix, success] = manipulateData(data, ...
            operation, rowNr, newRow, incMatrix, false);
        % Set new data to view.
        set(handles.tVariables, 'Data', data);
        
    % Parameters...
    case 4
        data2 = get(handles.tParameters, 'Data');
        rowNr = handles.parameterSelection(1);
        % Create data for new row.
        newRow = [];
        switch (lower(operation))
            case 'insert'
                newRow = {'', ''};

            case 'append'
                lastRow = size(data2, 1);
                if (lastRow > 0)
                    newRow = {'', ''};
                else
                    newRow = {'', ''};
                end
        end
        % Insert new row into existing data.
        [data2, ~, success2] = manipulateData(data2, ...
            operation, rowNr, newRow, incMatrix, false);
        % Set new data to view.
        set(handles.tParameters, 'Data', data2);
end
if (success)
    internalSetChanged(handles, true);
    set(handles.tMatrix, 'Data', incMatrix);
end


% --------------------------------------------------------------------
function btnAnalyzeSimple_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to btnAnalyzeSimple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (internalClose(handles, true))
    choices = {'Ranking Algorithm', 'Krysander''s Algorithm', ...
        'Brute-Force Algorithm'};
    algorithms = {'rank', 'mso', 'full'};
    dlgTitle = 'Report Generator';
    choice = questdlg('Which matching algorithm would you like to use?', ...
        dlgTitle, choices{:}, choices{1});
    if (~isempty(choice))
        [fileName, pathName] = uiputfile({'*.txt'; '*.pdf'});
        if (ischar(fileName))
            choice = strcmp(choice, choices);
            userData = guidata(handles.frmMain);
            sys = sa_match(userData.system, algorithms{choice});
            [~,~,fileExt] = fileparts(fileName);
            if(strcmp(fileExt,'.txt'))
                sa_report(sys, [pathName,'/',fileName]);
            elseif(strcmp(fileExt,'.pdf'))
                sa_report(sys, [pathName,'/', fileName],'pdf','analytic_expressions',true);
            end
            %system([pathName, fileName]);
        end
    end
end

%% Helper functions
function canClose = internalClose(handles, keepSystem)
if (nargin < 2)
    keepSystem = false;
end

userData = guidata(handles.frmMain);
canClose = ~userData.changed;
if (~canClose)
    % Construct a questdlg with three options
    choice = questdlg(['The current system has been changed. ', ...
        'Would you like to save it before proceeding?'], ...
        'Close Menu', 'Yes', 'No', 'Cancel', 'Yes');
    % Handle response
    switch choice
        case 'Yes'
            canClose = internalSave(handles);
        case 'No'
            canClose = true;
    end
end
% Disconnect system from external file.
if (canClose && ~keepSystem)
        userData.fileName = '';
        userData.system = sa_create([0, 0], 1, 1);
    guidata(handles.frmMain, userData);
    internalSetChanged(handles, true);
    updateGUIfromSYS(handles, userData.system);
end

function success = internalSave(handles, fileName)
system = updateSYSfromGUI(handles);
[valid, error] = sa_check(system);
success = valid;
if (~valid)
    msgbox(error, 'System Error', 'error');
else
    userData = guidata(handles.frmMain);
    userData.system = system;
    if (nargin >= 2)
        userData.fileName = fileName;
    end
    success = ~userData.standalone;
    % In case of standalone mode, it is essential that the results are
    % saved to an actual file since otherwise they are lost because no
    % output is returned.
    if (userData.standalone)
        if (isempty(userData.fileName))
            [fileName, pathName] = uiputfile({'*.xml';'*.mat'});
            if (ischar(fileName))
                userData.fileName = [pathName, fileName];
                success = true;
            end
        end
    end
    % Save changes to file.    
    if (~isempty(userData.fileName))
        sa_save(system, userData.fileName);
    end
    guidata(handles.frmMain, userData);
    if (success)
        internalSetChanged(handles, false);
    end
end    

function internalOpen(handles, fileName)
% Close file internally first.
canClose = internalClose(handles);
if (canClose)
    system = sa_load(fileName);
    if (~isempty(system))
        userData = guidata(handles.frmMain);
        userData.system = system;
        userData.fileName = fileName;
        guidata(handles.frmMain, userData);
        updateGUIfromSYS(handles, system);
        internalSetChanged(handles, false);
    end    
end

function internalSetChanged(handles, changed)
userData = guidata(handles.frmMain);
userData.changed = changed;
caption = 'Structural Analysis System Editor';
if (~isempty(userData.fileName))
    [~, name, ext] = fileparts(userData.fileName) ;
    caption = [caption, ' (', name, ext, ')'];
end
if (changed)
    set(handles.frmMain, 'Name', [caption, ' *']);
else
    set(handles.frmMain, 'Name', caption);        
end
% Update global user data.
guidata(handles.frmMain, userData);

function updateGUIfromSYS(handles, sys)
[numCons, numVars] = size(sys.incMatrix);

% Update constraints table.
enabledCons = false(1, numCons);
enabledCons(sys.enabledCons) = true;
canFail = false(1, numCons);
canFail(sys.canFail) = true;
% Build latex expression cell.
numRows = size(sys.cons, 1);
if (numRows >= 2)
    latex = sys.cons(2, :);
else
    latex = repmat({''}, 1, numCons);
end
% Build analytic expresssion cell.
if (numRows >= 3)
    analytic = sys.cons(3, :);
else
    analytic = repmat({''}, 1, numCons);
end
% Build data matrix.
data = [sys.cons(1, :)', latex', analytic', num2cell(sys.domains'), ...
    num2cell(enabledCons'), num2cell(canFail')];
% Update constraints view.
set(handles.tConstraints, 'Data', data);

% Update variables table.
% Build latex expression cell.
latex = [];
if (size(sys.inputs, 1) >= 2)
    latex = sys.inputs(2, :);
end
if (size(sys.meas, 1) >= 2)
    latex = [latex, sys.meas(2, :)];
end
if (size(sys.unknowns, 1) >= 2)
    latex = [latex, sys.unknowns(2, :)];
end
if (isempty(latex))
    latex = repmat({''}, 1, numVars);
end
% Build variable type cell.
type = [repmat({'input'}, 1, size(sys.inputs, 2)), ...
    repmat({'measurement'}, 1, size(sys.meas, 2)), ...
    repmat({'unknown'}, 1, size(sys.unknowns, 2))];
% Build data matrix.
%[rownames, columnNames] = sa_names(sys, 1);
[rownames, input_names, meas_name, unknown_name, param_name] = sa_names(sys);
columnNames = [input_names, meas_name, unknown_name];

data = [columnNames', latex', type'];
% Update variable view.
set(handles.tVariables, 'Data', data);


if isempty(param_name)
    param_name = {''};
end
latex = {''};
if (size(sys.parameters, 1) >= 2)
    latex = sys.parameters(2, :);
end
% Update parameter view.
set(handles.tParameters, 'Data', [param_name;latex]');

% Update incidence matrix.
columnFormat = repmat({'numeric'}, 1, numVars);
columnEditable =  true(1, numVars);
columnWidth = repmat({30}, 1, numVars);
% Update matrix view.
set(handles.tMatrix, ...
    'Data', sys.incMatrix, ...
    'ColumnName', columnNames, ...
    'ColumnFormat', columnFormat, ...
    'ColumnEditable', columnEditable, ...
    'ColumnWidth', columnWidth, ...
    'RowName', rownames);
% Redraw stuff to visualize changes.
% TODO: Find out why this sometimes not works correctly.
drawnow;

function [vars, rows] = getVariablesOfType(variables, type)
numVars = size(variables, 1);
rows = false(1, numVars);
for i = 1:numVars
    if strcmp(variables{i, 3}, type)
        rows(i) = true;
    end
end
vars = variables(rows, 1:2)';

function sys = updateSYSfromGUI(handles)
% Retrieve data from GUI tables.
matrix = get(handles.tMatrix, 'Data');
constraints = get(handles.tConstraints, 'Data');
variables = get(handles.tVariables, 'Data');
parameters = get(handles.tParameters, 'Data');
% Parse constraints...
cons = constraints(:, 1:3)';
doms = cell2mat(constraints(:, 4))';
enabledCons = find(cell2mat(constraints(:, 5))');
canFail = find(cell2mat(constraints(:, 6))');
% Parse variables...
[inputs, inputInd] = getVariablesOfType(variables, 'input');
[meas, measInd] = getVariablesOfType(variables, 'measurement');
[unknowns, unknownInd] = getVariablesOfType(variables, 'unknown');

% Build incidence matrix...
m = [matrix(:, inputInd), matrix(:, measInd), matrix(:, unknownInd)];

% Create structure
sys = struct('incMatrix', m, 'cons', {cons}, 'inputs', {inputs}, ...
    'meas', {meas}, 'unknowns', {unknowns}, 'parameters', {parameters'}, ...
    'domains', doms, 'canFail', canFail, 'enabledCons', enabledCons, ...
    ...'enabledVars', enabledVars, ...
    'matchings', [], 'parityEqs', []);

function selectView(handles, view)
userData = guidata(handles.frmMain);
if (view ~= userData.selectedView && (view >= 1) && (view <= 4))
    % Store view and update global gui structure.
    userData.selectedView = view;
    guidata(handles.frmMain, userData);
    % Reset visibility of tables.
    set(handles.tConstraints, 'Visible', 'off');
    set(handles.tVariables, 'Visible', 'off');
    set(handles.tMatrix, 'Visible', 'off');
    set(handles.tParameters, 'Visible', 'off');
    % Reset menu state.
    set(handles.mnuViewConstraints, 'Checked', 'off');
    set(handles.mnuViewVariables, 'Checked', 'off');
    set(handles.mnuViewMatrix, 'Checked', 'off');
    set(handles.mnuViewParameters, 'Checked', 'off');
    set(handles.mnuEdit, 'Visible', boolstr(view == 1 || view == 2));
    % Reset toolbar state.
    set(handles.btnViewConstraints, 'State', 'off');
    set(handles.btnViewVariables, 'State', 'off');
    set(handles.btnViewMatrix, 'State', 'off');
    set(handles.btnViewParameters, 'State', 'off');
    % Reset edit buttons
    if (view == 1 || view == 2 || view == 4)
        visible = 'on';
    else
        visible = 'off';
    end
    set(handles.btnInsert, 'Visible', visible);
    set(handles.btnAppend, 'Visible', visible);
    set(handles.btnRemove, 'Visible', visible);
    set(handles.btnMoveUp, 'Visible', visible);
    set(handles.btnMoveDown, 'Visible', visible);
    
    switch view
        case 1
            set(handles.tConstraints, 'Visible', 'on');
            set(handles.mnuViewConstraints, 'Checked', 'on');
            set(handles.btnViewConstraints, 'State', 'on');
            
        case 2
            set(handles.tVariables, 'Visible', 'on');
            set(handles.mnuViewVariables, 'Checked', 'on');
            set(handles.btnViewVariables, 'State', 'on');
            
        case 3
            % Update column and row names.
            constraintsData = get(handles.tConstraints, 'Data');
            variablesData = get(handles.tVariables, 'Data');
            if (~isempty(constraintsData) && ~isempty(variablesData))
                set(handles.tMatrix, ...
                    'RowName', constraintsData(:, 1), ...
                    'ColumnName', variablesData(:, 1));
                set(handles.tMatrix, 'Visible', 'on');
            end
            % Set other properties.
            set(handles.mnuViewMatrix, 'Checked', 'on');
            set(handles.btnViewMatrix, 'State', 'on');
        case 4
            set(handles.tParameters, 'Visible', 'on');
            set(handles.mnuViewParameters, 'Checked', 'on');
            set(handles.btnViewParameters, 'State', 'on');
    end
    % Set enable option of edit buttons.
    internalEnableEditButtons(handles);
end

function [data, incMatrix, success] = manipulateData(data, operation, ...
    rowNr, newRow, incMatrix, isRowOperation)
success = false;
if (~isRowOperation)
    incMatrix = incMatrix';
end
numCols = size(incMatrix, 2);
switch lower(operation)
    case 'append'
        data = [data; newRow];
        incMatrix = [incMatrix; zeros(1, numCols)];
        success = true;
    
    case 'insert'
        data = [data(1:rowNr-1, :); newRow; data(rowNr:end, :)];
        incMatrix = [incMatrix(1:rowNr-1, :); zeros(1, numCols); ...
            incMatrix(rowNr:end, :)];
        success = true;
        
    case 'remove'
        if (size(data, 1) > 1)
            data(rowNr, :) = [];
            if rowNr <= size(incMatrix,1)
                incMatrix(rowNr, :) = [];
            end
            success = true;
        end
        
    case 'up'
        if (rowNr > 1)
            temp = data(rowNr-1, :);
            data(rowNr-1, :) = data(rowNr, :);
            data(rowNr, :) = temp;
            temp = incMatrix(rowNr-1, :);
            incMatrix(rowNr-1, :) = incMatrix(rowNr, :);
            incMatrix(rowNr, :) = temp;
            success = true;
        end
        
    case 'down'
        if (rowNr < size(data, 1))
            temp = data(rowNr+1, :);
            data(rowNr+1, :) = data(rowNr, :);
            data(rowNr, :) = temp;
            temp = incMatrix(rowNr+1, :);
            incMatrix(rowNr+1, :) = incMatrix(rowNr, :);
            incMatrix(rowNr, :) = temp;
            success = true;
        end
        
    otherwise
        error('Invalid operation %s.', operation); 
end
if (~isRowOperation)
    incMatrix = incMatrix';
end

function str = boolstr(bool)
if (bool)
    str = 'on';
else
    str = 'off';
end

function internalEnableEditButtons(handles)
userData = guidata(handles.frmMain);
fieldnames(userData);
switch userData.selectedView
    case 1
        enable1 = userData.constraintSelection(1) > 0;
        data = get(handles.tConstraints, 'Data');
        numRows = size(data, 1);
        enable2 = enable1 && (numRows > 1);
        enable3 = enable2 && (userData.constraintSelection(1) > 1);
        enable4 = enable2 && (userData.constraintSelection(1) < numRows);
        
    case 2
        enable1 = userData.variableSelection(1) > 0;
        data = get(handles.tVariables, 'Data');
        numRows = size(data, 1);
        enable2 = enable1 && (numRows > 1);
        enable3 = enable2 && (userData.variableSelection(1) > 1);
        enable4 = enable2 && (userData.variableSelection(1) < numRows);
        
    case 4
        enable1 = userData.parameterSelection(1) > 0;
        data = get(handles.tParameters, 'Data');
        numRows = size(data, 1);
        enable2 = enable1 && (numRows > 1);
        enable3 = enable2 && (userData.parameterSelection(1) > 1);
        enable4 = enable2 && (userData.parameterSelection(1) < numRows);
        
    otherwise
        enable1 = false;
        enable2 = false;
        enable3 = false;
        enable4 = false;
end
% Set enable option.
set(handles.btnInsert, 'Enable', boolstr(enable1));
set(handles.mnuEditInsert, 'Enable', boolstr(enable1));
set(handles.btnRemove, 'Enable', boolstr(enable2));
set(handles.mnuEditRemove, 'Enable', boolstr(enable2));
set(handles.btnMoveUp, 'Enable', boolstr(enable3));
set(handles.mnuEditMoveUp, 'Enable', boolstr(enable3));
set(handles.btnMoveDown, 'Enable', boolstr(enable4));
set(handles.mnuEditMoveDown, 'Enable', boolstr(enable4));       


% --- Executes when entered data in editable cell(s) in tParameters.
function tParameters_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tParameters (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
