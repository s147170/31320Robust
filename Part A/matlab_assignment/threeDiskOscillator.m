% 31320 Mandatory assignment 2020 starting script
% Dimitrios Papageorgiou (dimpa@elektro.dtu.dk)

clear all;
close all;
clc;
warning ('off','all');

load('ECP_values.mat');
% Physical system parameters
J_1 = ECP_values(1);            % Disk 1 inertia kgm^2
J_2 = ECP_values(2);            % Disk 2 inertia kgm^2
J_3 = ECP_values(3);            % Disk 3 inertia kgm^2
k_1 = ECP_values(4);            % Shaft 1-2 stiffness Nm/rad
k_2 = ECP_values(5);            % Shaft 2-3 stiffness Nm/rad
b_1 = mean(ECP_values([6 7]));  % Disk 1 damping and friction Nms/rad
b_2 = mean(ECP_values([8 9]));  % Disk 2 damping and friction Nms/rad
b_3 = mean(ECP_values([10 11]));% Disk 3 damping and friction Nms/rad
T_Cp = ECP_values(12);          % Disk 1 Coulomb friction in positive direction
T_Cm = ECP_values(13);          % Disk 1 Coulomb friction in negative direction
atan_scale = 100;               % Sign approximation factor
w_th = 0.75;                    % Threshold angular velocity rad/s

% The system states are [theta_1;omega_1;theta_2;omega_2;theta_3;omega_3]
x_0 = [0;0;0;0;0;0];            % Initial conditions

T_s = 0.004;                    % Sampling period
sigma_meas = deg2rad(0.0056)*[1;1;1]; % Measurements noise


%% State space representation
A = [[        0,        1,                0,        0,        0,        0];
[ -k_1/J_1, -b_1/J_1,          k_1/J_1,        0,        0,        0];
[        0,        0,                0,        1,        0,        0];
[  k_1/J_2,        0, -(k_1 + k_2)/J_2, -b_2/J_2,  k_2/J_2,        0];
[        0,        0,                0,        0,        0,        1];
[        0,        0,          k_2/J_3,        0, -k_2/J_3, -b_3/J_3]];
B = [ 0;
 1/J_1;
     0;
     0;
     0;
     0];
C = [[ 1, 0, 0, 0, 0, 0];
[ 0, 0, 1, 0, 0, 0];
[ 0, 0, 0, 0, 1, 0]];
D = [0;
    0;
    0];
E_x = [0;
 -1/J_1;
      0;
      0;
      0;
      0];
E_y = [ 0;
     0;
     0;];

% Discrete time
sys_d = c2d(ss(A,B,C,D),T_s);
F_d = sys_d.A;
G_d = sys_d.B;

% Luenberger observer with integral action design
F_aug = [F_d G_d;zeros(1,6) 1];
G_aug = [G_d;0];
C_aug = [C zeros(3,1)];
L_aug = dlqe(F_aug,eye(7),C_aug,1e-3*eye(7),deg2rad(0.0056)*eye(3));
L_o = L_aug(1:6,:);
L_d = L_aug(7,:);

% State-feedback LQR design
Q_c = diag([2 0 2 0 2.5 0.0024]);
R_c = 10;
[K_c,S_c,CLP] = dlqr(F_d,G_d,Q_c,R_c);

% Scaling of reference
C_3 = [0 0 0 0 1 0];
C_ref = pinv(C_3*(eye(6) - F_d + G_d*K_c)^-1*G_d*K_c);
theta_ref = pi/2;

%% Residuals and filters design

% This is directly inserted in simulink model.

%% Design of the nullspace-based residuals

syms s
H_yu = C*((eye(size(A,1))*s-A)^-1)*B+D;
H_yd = C*((eye(size(A,1))*s-A)^-1)*E_x+E_y;
H_yu = simplify(H_yu);
H_yd = simplify(H_yd);

H = [H_yu H_yd;
     1 0];

F = simplify(null(H')');

omega_1 = 5;
omega_2 = 5;
zeta_1 = 1.707;
zeta_2 = 1.707;

filters = [omega_1/(s^2 + zeta_1*s + omega_1) 0 ;
          0 omega_2/(s^2 + zeta_2*s + omega_2)];

     
F_filter = filters*F;
[num,den] = numden(F_filter);

for i = 1:size(num,1)
    for j = 1:size(num,2)
        F_tf(i,j) = [tf(sym2poly(num(i,j)),sym2poly(den(i,j)))];
        F_tf_d(i,j) = c2d(F_tf(i,j),T_s,'tustin');
    end
end

%% CUSUM

T_s_noise = 0.004;
sigma_1 = 10e-2;
sigma_2 = 10e-2;
sigma_3 = 10e-2;
sigmas = [sigma_1 sigma_2 sigma_3];
%%-- Build residual generator given in problem6 --%%
N_y1 = 1.1004e4;
N_y2 = -(7.2199*s^2 + 0.6478*s + 2.1008e4);
N_y3 = 1.0003e4;

D_s = s^2 + 87.9646*s + 3947.8;
r_s = [N_y1/D_s N_y2/D_s N_y3/D_s];


[r_s_num, r_s_den] = numden(r_s);
for i = 1:size(r_s_num,1)
    for j = 1:size(r_s_num,2)
        r_s_tf(i,j) = [tf(sym2poly(r_s_num(i,j)),sym2poly(r_s_den(i,j)))];
        r_s_tf_d(i,j) = c2d(r_s_tf(i,j),T_s_noise,'tustin');
    end
end
r_s_d = c2d(ss(r_s_tf),T_s_noise,'tustin');

sigma_in = diag(sigmas);
sigma_out = sqrt(covar(r_s_tf_d(i,j),sigma_2));


mu0 = 0;
mu1 = 0.05; % this is what we are looking for
sigma_r_min = sigma_out(1,1);
sigma_r_max = sigma_out(1,1);
h_min = 0;
h_max = 17;
ARL_design(mu1,mu0,sigma_r_min,sigma_r_max,h_min,h_max)
h = 10;

%% GLR
f_m = [0;-0.025;0]; % Sensor fault vector (added to [y1;y2;y3])
P_F = 0.01; %(or lower) false alarm probability
P_M = 0.01; % probability of false detection 
mu_0 = 0;
mu_1 = -0.025;

for i = 1:0.0001:50
    P_F = 1 - chi2cdf(2*i,1);
    if(P_F<=0.01)
        h_glr = i;
        P_F;
        break;
    end
end

for i = 1:1000
    lambda = i*((mu_1 - mu_0)/sigma_2)^2;
    P_D = 1 - ncx2cdf(2*h_glr,1,lambda);
    P_M = 1- P_D;
    if(P_M <= 0.01)
        M = i;
        P_M;
        break;
    end
end

GLR_design(M,mu_0,mu_1,sigma_2);



%% Virtual sensor
F = sys_d.A;
G = sys_d.B;
C_f = [C(1,:);C(3,:)];

if (rank(C_f) == rank([C;C_f]))
    disp('Perfect static matching for sensor fault');
else
    disp('Imperfect static matching for sensor fault');
end
% Check observability of the faulty system
if (rank(obsv(F,C_f)) == size(A,1))
    disp('Faulty system is observable');
else
    disp('Faulty system is not observable');
end
% discretized virtual sensor

vs_eig = eig(F-G*K_c);
L_V_d = place(F',C_f',vs_eig)'; 
F_V = F-L_V_d*C_f;
G_V = sys_d.B;
P_V_d = C*pinv(C_f);
C_V_d = C-P_V_d*C_f;

%% Simulation
simTime = 25;       % Simulation duration in seconds
f_m_time = 8.5;     % Sensor fault occurence time

tau_1 = 1;
tau_2 = 1;
tau_3 = 1;
tau_4 = 1;

C_ref = C_ref;
K_c = K_c;

useTestSignals = 0; %If 1, use the results acquired from the test rig.

load('y_values.mat')

TimeVector = [0:T_s:T_s*(length(y)-1)]'; 
sim('threeDiskOscillatorModel');

%% Plots
set(0,'DefaultTextInterpreter','latex');
set(0,'DefaultAxesFontSize',20);
set(0,'DefaultLineLineWidth', 2);
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
%%
%Plotting Residuals (question 2 and 4)

figure
plot(Residuals_Laplace, 'linewidth',3)
grid on
grid minor
hold on
yyaxis right
plot(Ref_signal, 'linewidth',3)
legend('Residual 1', 'Residual 2', 'Reference signal')
title('Residual generator in laplace')

figure
plot(Residuals_LeftNullspace, 'linewidth',3)
grid on
grid minor
hold on
yyaxis right
plot(Ref_signal, 'linewidth',3)
legend('Residual 1', 'Residual 2', 'Reference signal')
title('Left Nullspace residual generator')
%%
%Plotting CUSUM
figure
subplot(2,1,1)
plot(g_CUSUM_out(:,1), 'linewidth',3)
grid on
hold on
yyaxis right
plot(H_CUSUM_out(:,1), 'linewidth',3)
legend('CUSUM output','Fault detection','Interpreter','latex')
title('CUSUM residual 1 and fault detection','Interpreter','latex')

subplot(2,1,2)
plot(g_CUSUM_out(:,2), 'linewidth',3)
grid on
hold on
yyaxis right
plot(H_CUSUM_out(:,2), 'linewidth',3)
ylim([0 1])
legend('CUSUM output','Fault detection')
title('CUSUM residual 2 and fault detection')

%%
%Plotting GLR
figure
plot(g_GLR_out, 'linewidth',3)
grid on
hold on
yyaxis right
plot(H_GLR_out, 'linewidth',3)
legend('GLR output','Fault detection')
title('GLR residual and fault detection')


%% Plot theta_3 for DLQR
figure
plot(theta_3_out,'linewidth',3)
set(gca,'ytick',[0:pi/4:pi/2]) % where to set the tick marks
set(gca,'yticklabels',{'0','$\frac{pi}{4}$','$\frac{pi}{2}$'}) % give them user-defined labels

ylabel('$\theta_3$','Interpreter','latex')
xlabel('time','Interpreter','latex')
yyaxis right
plot(H_GLR_out, 'linewidth',1)
set(gca,'ytick',[0:1:1]) % where to set the tick marks
ylabel('$H_{GLR}$','Interpreter','latex')
grid on
grid minor
title('$\theta_3$ with DLQR and virtual sensor','Interpreter','latex')
